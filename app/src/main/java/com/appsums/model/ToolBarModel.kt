package com.appsums.model

data class ToolBarModel(
    var backBtn: Boolean = false,
    var sideNav: Boolean = false,
    var bgToolBar: Int = 0,
    var toolBarShow: Boolean = false,
    var toolBarTtlShow: Boolean = false,
    var toolBarProfileShow: Boolean = false,
    var notiIconShow: Boolean = false,
    var imgGrpUserShow: Boolean = false,
    var imgAudiohow: Boolean = false,
    var imgVideoShow: Boolean = false,
    var toolBarTtl: String = ""
)

data class GenericModel1(
    var responseMessage: String,
    var userData: ArrayList<HomeDataDocsModel>,
    var result: ArrayList<ViewDoctorDetail>
)

data class GenericModel(
    var responseMessage: String,
    var twilioToken: String,
    var roomName: String,
    var userData: UserData,
    var result: UserData,
    var slotData: AvailabilityModle,
    var homeData: ArrayList<HomeDataModel>,
    var appointmentData: ArrayList<AppointmentModel>,
    var appointmentDetails: AppointmentModel,
    var labData: ArrayList<HomeDataDocsModel>,
    var laboratory: LaboratoryModel,
    var documentList: ArrayList<SubscriptionModel>,
    var notificationData: ArrayList<NotificationModel1>,
    var userList: ArrayList<FavouriteListModel>,
    var walletData: WalletData,
    val laboratoryDetails: LaboratoryDetailModel,
    val packageList: List<Any>

)

data class AvailabilityModle(
    var day: String = "",
    var _id: String = "",
    var startTime: String = "",
    var endTime: String = "",
    var interval: String = "",
    var slots: ArrayList<SloGetData> = ArrayList<SloGetData>(),
)

data class SloGetData(var slotTime: String, var _id: String)

data class LaboratoryModel(
    var docs: ArrayList<HomeDataDocsModel>
)

data class HomeDataModel(
    var docs: ArrayList<HomeDataDocsModel>

)

data class AppointmentModel(
    var amount: Int,
    var status: String,
    var appointmentEndTime: String,
    var appointmentStartTime: String,
    var isFavourite: Boolean,
    var document: String,
    var reason: String,
    var _id: String,
    var serviceProviderId: AppointmentDocModel
)

data class AppointmentDocModel(
    var serviceProviderType: ArrayList<ServiceProviderType>,
    var _id: String,
    var experience_in_months: Int,
    var fullName: String,
    var secondry_specialty: ArrayList<SecondarySpcuaility>,
    var image: String,
    var serviceType: String,
    var businessLogo: String,
    var name: String,
    var profilePic: String,
    var isFavourite: Boolean,
    var rating: Int,
    var location: LocationModel,
    var experience_in_years: Int,
    var serviceCharge: Int,
)

data class HomeDataDocsModel(
    var serviceProviderType: ArrayList<ServiceProviderType>,
    var _id: String,
    var experience_in_months: Int,
    var fullName: String,
    var image: String,
    var serviceType: String,
    var businessLogo: String,
    var name: String,
    var profilePic: String,
    var isFavourite: Boolean,
    var rating: Int,
    var location: LocationModel,
    var experience_in_years: Int,
    var serviceCharge: Int,
)

data class SecondarySpcuaility(var _id: String, var secondry_specialty: String)

data class ServiceProviderType(
    var _id: String,
    var primary_specialty: String,
    var description: String,
    var image: String
)

data class SubscriptionModel(
    var couponType: String = "",
    var code: String = "",
    var secondry_specialty: String = "",
    var status: String = "",
    var _id: String = "",
    var primary_specialtyId: String = "",
    var name: String = "",
    var applicability: String = "",
    var value: Int = 0,
    var price: Int = 0,
    var primary_specialty: String = "",
    var description: String = "",
    var image: String = "",
    var createdAt: String = "",
    var isFeatured: String = "",
    var updatedAt: String = "",
    var numberOfSecondrySpecialty: Int = 0,
    var numberOfLeads: Int = 0,
    var numberOfMessages: Int = 0,
    var days_duration: Int = 0,
    var session_numbers: Int = 0,
    var minutesOfAudioCalls: Int = 0,
    var minutesOfVideoCalls: Int = 0
)

data class UserData(
    var url: String,
    var gender: String,
    var lastName: String,
    var firstName: String,
    var mobileNumber: String,
    var bloodGroup: String,
    var marital_status: String,
    var diabetesDetails: String,
    var height: String,
    var medicalFile: String,
    var profilePic: String,
    var allergyDeatils: String,
    var weight: String,
    var familyNotes: String,
    var bio: String,
    var date_of_birth: String,
    var email: String,
    var token: String, var _id: String,
    var isPersonal_profile_complete: Boolean,
    var iSDiabetes: Boolean,
    var location: LocationModel,
    var isSubscribe: Boolean,
    var isMedical_profile_complete: Boolean,
    var isProfessional_profile_complete: Boolean,
    var isVerified: Boolean
)

data class LocationModel(var address: String, var coordinates: ArrayList<Double>)

data class ParamModel(var id: String = "", var isSelect: Boolean = false)

data class ResendOtpModel(
    var userId: String
)

data class OtpModel(
    var userId: String,
    var otp: String
)

data class FavouriteModal(
    var userId: String,
    var favourite: Boolean
)

data class BookModel(
    var serviceProviderId: String,
    var slotTime: String,
    var dayId: String,
    var slotId: String,
    var bookingDate: String,
    var appointmentStartTime: String,
    var appointmentEndTime: String,
    var reason: String,
    var document: String? = "",
)

data class LatLngSprciltyModel(
    var serviceProviderTypeId: String? = null,
    var startRating: Int? = null,
    var endRating: Int? = null,
    var costFrom: Int? = null,
    var costTo: Int? = null,
    var distance: Int? = null,
    var specialtyId: String? = null,
    var lat: Double,
    var long: Double
)

data class LatLngModel(
    var lat: Double,
    var long: Double
)

data class ForgotPasswordNoModel(
    var mobileNumber: String
)

data class ForgotPasswordModel(
    var email: String
)

data class LoginModel(
    var deviceType: String,
    var deviceToken: String,
    var email: String,
    var password: String,
    var offset: Int
)

data class ResetPasswordModel(
    var userId: String,
    var password: String
)

data class LoginPhnModel(
    var deviceType: String,
    var deviceToken: String,
    var mobileNumber: String,
    var password: String,
    var offset: Int
)

data class MedicalModel(
    var height: String,
    var weight: String,
    var bloodGroup: String,
    var diabetesDetails: String,
    var allergyDeatils: String,
    var familyNotes: String,
    var medicalFile: String,
    var iSDiabetes: Boolean?
)

data class BuySubscriptionModel(
    var couponId: String? = null,
    var subscriptionId: String,
    var cardType: String,
    var cardNumber: String,
    var exp_month: String,
    var exp_year: String,
    var cvc: String,
    var price: Int,
    var cardSave: Boolean
)

data class AvailabilityDoctorModle(
    var day: String = "",
    var startTime: String = "",
    var endTime: String = "",
    var interval: String = "",
    var slots: ArrayList<SloData> = ArrayList<SloData>(),
)

data class SloData(var slotTime: String)

data class ProfessionalDoctorModel(
    var businessLogo: String,
    var primary_specialtyId: String,
    var secondry_specialty: ArrayList<String>,
    var licenceNumber: String,
    var experience_in_months: Int,
    var experience_in_years: Int,
    var serviceCharge: Int,
    var availability: ArrayList<AvailabilityDoctorModle>
)

data class ProfileDoctorModel(
    var fullName: String,
    var gender: String,
    var description: String,
    var address: String,
    var profilePic: String,
    var lat: Double,
    var long: Double
)

data class ProfileModel(
    var firstName: String,
    var lastName: String,
    var gender: String,
    var date_of_birth: String,
    var marital_status: String,
    var bio: String,
    var address: String,
    var profilePic: String,
    var lat: Double,
    var long: Double
)

data class EditProfileModel(
    var firstName: String,
    var lastName: String,
    var gender: String,
    var date_of_birth: String,
    var marital_status: String,
    var bio: String,
    var address: String,
    var profilePic: String,
    var lat: Double,
    var long: Double,
    var height: String,
    var weight: String,
    var bloodGroup: String,
    var diabetesDetails: String,
    var allergyDeatils: String,
    var familyNotes: String,
    var medicalFile: String,
    var iSDiabetes: Boolean?

)

data class SignupModel(
    var deviceType: String,
    var deviceToken: String,
    var email: String,
    var countryCode: String,
    var mobileNumber: String,
    var password: String
)

data class TwillioModel(
    var appointmentId: String? = "",
    var receiverId: String? = "",
    var pushType: String? = ""
)

data class NotificationModel1(
    var _id: String,
    var userId: String,
    var title: String,
    var body: String,
    var notificationType: String,
    var isRead: Boolean,
    var isAdminInvolved: Boolean,
    var senderId: String = "",
    var updatedAt: String = "",
    var createdAt: String = "",
    var __v: Int,
)

data class FavouriteListModel(
    var _id: String = "",
    var profilePic: String = "",
    var rating: Int = 0,
    var fullName: String = "",
    var experience_in_months: Int,
    var experience_in_years: Int,
    var serviceCharge: Int,
    var isFavourite: Boolean
)

data class WalletData(
    val expiryDate: String,
    val createdAt: String,
    val sessions: Int,
    val totalBalance: Int,
    val __v: Int,
    val _id: String,
    val userType: String,
    val numberOfMessages: Int,
    val subscriptionId: String,
    val isFeatured: Boolean,
    val userId: String,
    val updatedAt: String
)

data class ViewDoctorDetail(
    var _id: String = "",
    var profilePic: String = "",
    var rating: Int = 0,
    var businessLogo: String = "",
    var fullName: String = "",
    var experience_in_months: Int,
    var experience_in_years: Int,
    var primary_specialtyId: String,
    var serviceCharge: Int,
    var isFavourite: Boolean
)

data class ServiceProviderTypeItem(
    var image: String,
    var createdAt: String,
    var V: Int,
    var primary_specialty: String,
    var description: String,
    var _id: String,
    var status: String,
    var updatedAt: String
)

data class Location(
    var address: String,
    var coordinates: List<Double>,
    var type: String
)

data class SpecialtiesItem(
    var image: String,
    var createdAt: String,
    var primary_specialtyId: String,
    var __v: Int,
    var _id: String,
    var secondry_specialty: String,
    var status: String,
    var updatedAt: String
)

data class LaboratoryDetailModel(
    val serviceType: String,
    val image: String,
    val city: String,
    val postalCode: String,
    val availability: List<AvailabilityModle>,
    val createdAt: String,
    val __v: Int,
    val name: String,
    val location: Location,
    val _id: String,
    val state: String,
    val status: String,
    val updatedAt: String
)



