package com.appsums.application

import androidx.multidex.MultiDexApplication
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.PhoneAuthProvider

class GymApplication : MultiDexApplication() {
    companion object {
        @get:Synchronized
        lateinit var instance: GymApplication

        var resendToken: PhoneAuthProvider.ForceResendingToken? = null
    }

    // 15, 20, 22, 27, 28, 64
    // 39, 40, 41,

    // 36, 44, 53, 55

    override fun onCreate() {
        super.onCreate()
        instance = this


      //  if (BuildConfig.DEBUG) {
        //    CrashReporter.initialize(this)
        //}

        FirebaseApp.initializeApp(this)
//        FirebaseApp.initializeApp(this)

    }
}