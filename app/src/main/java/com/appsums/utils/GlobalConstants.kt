package com.appsums.utils

import com.appsums.R
import com.appsums.application.GymApplication

object GlobalConstants {

    @JvmStatic
    val BASE_URL = "http://3.19.69.119/api/"

    @JvmStatic
    val BASE_URL_IMG = "http://3.19.69.119/public/"

    @JvmStatic
    val SHARED_PREF = GymApplication.instance.resources.getString(R.string.shared_pref)

    @JvmStatic
    val ACCESS_TOKEN = "access_token"

    @JvmStatic
    var DEVICE_ID = "device_id"

    var UNIQUE_DEVICE_ID = "unique_device_id"

    @JvmStatic
    var Device_Type = "Android"

    @JvmStatic
    var APP_VERSION = "1.0"

    @JvmStatic
    val USERID = "user_id"

    @JvmStatic
    val EMAIL = "EMAIL"

    const val ACTION = "action"
    const val CALLING = "calling"
    const val PUSH_TYPE = "push_type"
    const val VIDEO_CALL = "video_call"

    const val DIAL_CALL = "dial_call"
    const val INCOMING_CALL = "incoming_call"

    const val AUDIO = "audio"
    const val VIDEO = "video"
    const val CALL_REJECT = "call_reject"

    //choose login
    const val LAWYER = "lawyer"
    const val CLIENT = "client"
    const val OTHER = "other"
    const val FROM = "from"
    const val PROFILE_PIC = "profile_pic"
    const val FIRST_NAME = "first_name"
    const val LAST_NAME = "last_name"
    const val TWILLIO_TOKEN = ""
    const val ROOM_ID = "room_id"
    const val SALUTATION = "salutation"
    const val SENDER_ID = "sender_id"
    const val RECEIVER_ID = "receiver_id"
    const val IS_CALL_ACCEPT = "is_call_accept"
    const val NAME = "name"
    const val RELATION = "relation"
    const val FEE = "fee"
    const val DOC_LIST = "doc_list"
    const val FOLLOW_UP_ACCEPT = "follow_up_accept"

    /*   @JvmStatic
    val TWILLIO_TOKEN = "TWILLIO_TOKEN"

    @JvmStatic
    val CALLING = "CALLING"

    const val DIAL_CALL = "dial_call"
    const val INCOMING_CALL = "incoming_call"

    @JvmStatic
    val FIRST_NAME = "FIRST_NAME"

    @JvmStatic
    val FROM = "FROM"

    @JvmStatic
    val ROOM_ID = "ROOM_ID"

    @JvmStatic
    val VIDEO = "video"
    @JvmStatic
    val AUDIO = "AUDIO"
    @JvmStatic
    val SENDER_ID = "SENDER_ID"
    @JvmStatic
    val RECEIVER_ID = "RECEIVER_ID"
    @JvmStatic
    val PROFILE_PIC = "PROFILE_PIC"
    @JvmStatic
    val PUSH_TYPE = "PUSH_TYPE"
    @JvmStatic
    val LAST_NAME = "LAST_NAME"*/

    @JvmStatic
    val USER_PROFILE = "userProfileM"

    @JvmStatic
    val PIC = "PIC"


    @JvmStatic
    val LATTITUDE = "LATTITUDE"

    @JvmStatic
    val PROFILE_STATUS = "PROFILE_STATUS"

   @JvmStatic
    val MEDICAL_STATUS = "MEDICAL_STATUS"

    @JvmStatic
    val PROFESSIONAL_STATUS = "PROFESSIONAL_STATUS"

     @JvmStatic
    val IS_VERIFIED = "IS_VERIFIED"

     @JvmStatic
    val IS_SUBSCRIBED = "IS_SUBSCRIBED"

    @JvmStatic
    val LONGITUDE = "LONGITUDE"

    val requestKey = "requestKey"

    @JvmStatic
    val FILTER_OBJECT = "FILTER_OBJECT"

    @JvmStatic
    val LOCNAME = "LOCNAME"


    const val EMAIL_PATTERN =
        "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
}




