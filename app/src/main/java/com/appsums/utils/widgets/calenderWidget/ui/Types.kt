package com.appsums.utils.widgets.calenderWidget.ui

import android.view.View
import com.appsums.utils.widgets.calenderWidget.model.CalendarDay
import com.appsums.utils.widgets.calenderWidget.model.CalendarMonth

open class ViewContainer(val view: View)

interface DayBinder<T : ViewContainer> {
    fun create(view: View): T
    fun bind(container: T, day: CalendarDay)
}

interface MonthHeaderFooterBinder<T : ViewContainer> {
    fun create(view: View): T
    fun bind(container: T, month: CalendarMonth)
}

typealias MonthScrollListener = (CalendarMonth) -> Unit
