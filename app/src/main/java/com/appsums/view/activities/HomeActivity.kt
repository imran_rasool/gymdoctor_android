package com.appsums.view.activities

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import com.appsums.AppointmentFragment
import com.appsums.DasboardFragment
import com.appsums.R
import com.appsums.SettingsFragment
import com.appsums.application.GymApplication
import com.appsums.databinding.ActivityHomeBinding
import com.appsums.model.ParamModel
import com.appsums.model.ToolBarModel
import com.appsums.utils.GPSTracker
import com.appsums.utils.GlobalConstants
import com.appsums.utils.SharedPrefClass
import com.appsums.view.base.BaseActivity
import com.appsums.view.base.CustomDialog
import com.appsums.view.fragment.*
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener


class HomeActivity : BaseActivity<ActivityHomeBinding>(ActivityHomeBinding::inflate) {


    var locationname: MutableLiveData<String>? = null

    override fun setContainerLayout(): Int {
        return R.id.container
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        getCurrentLocationName()

//        if (!SharedPrefClass().getPrefValue(
//                this,
//                GlobalConstants.PROFESSIONAL_STATUS
//            ).toString().toBoolean()
//        ){
//            displayItNoStack(ClientInfoFragment(0))
//        }else if (!SharedPrefClass().getPrefValue(
//                this,
//                GlobalConstants.PROFILE_STATUS
//            ).toString().toBoolean()
//        ){
//            displayItNoStack(CreateProfileFragment(0))
//        }
//        else

//        Log.d("Dddd",SharedPrefClass().getPrefValue(
//            this,
//            GlobalConstants.ACCESS_TOKEN
//        ).toString())

        //  val mAuthToken =
        //    SharedPrefClass().getPrefValue(GymApplication.instance, GlobalConstants.ACCESS_TOKEN)
        //      .toString()
        //Log.d("mAuthToken", mAuthToken)

        if (SharedPrefClass().getPrefValue(this, GlobalConstants.ACCESS_TOKEN)
            != null && SharedPrefClass().getPrefValue(this, GlobalConstants.MEDICAL_STATUS)
                .toString().toBoolean()
        ) {
            displayItNoStack(DasboardFragment())
            updateNavHeader()
        } else
            displayItNoStack(InitialFragment())

        callNavigationItems()

        viewDataBinding?.apply {

            imgProfile.setOnClickListener {
                displayItAddStack(ProfileFragment())
            }

            imgMsg.setOnClickListener {
                displayItAddStack(MessageFragment())
            }

            imgNoti.setOnClickListener {
                displayItAddStack(NotificationFragment())
            }

            imgDrawer.setOnClickListener {
                drawerLayout.openDrawer(GravityCompat.START)
            }

            imgBackBtn.setOnClickListener {
                popBack()
            }

        }
    }

    public fun updateNavHeader() {
        viewDataBinding?.navLayout?.apply {
            tvEmailNav.setText(
                SharedPrefClass().getPrefValue(
                    this@HomeActivity,
                    GlobalConstants.EMAIL
                ).toString()
            )
//            tvNameNav.setText(
//                SharedPrefClass().getPrefValue(
//                    this@HomeActivity,
//                    GlobalConstants.NAME
//                ).toString()
//            )
            setImageWithUrl(
                SharedPrefClass().getPrefValue(
                    this@HomeActivity,
                    GlobalConstants.PIC
                ).toString(), imgnav
            )
        }
    }

    fun setToolbarText(toolBarModel: ToolBarModel) {

        viewDataBinding?.apply {

            tvTtl.text = toolBarModel.toolBarTtl ?: ""
            if (toolBarModel.backBtn) {
                imgBackBtn.visibility = View.VISIBLE
            } else {
                imgBackBtn.visibility = View.GONE
            }

            if (toolBarModel.sideNav) {
                imgDrawer.visibility = View.VISIBLE
            } else {
                imgDrawer.visibility = View.GONE
            }

            if (toolBarModel.toolBarShow) {
                llToolBar.visibility = View.VISIBLE
            } else {
                llToolBar.visibility = View.GONE
            }

            if (toolBarModel.notiIconShow) {
                imgNoti.visibility = View.VISIBLE
            } else {
                imgNoti.visibility = View.GONE
            }

            if (getCurPatientPackage()) {
                imgMsg.visibility = View.GONE
                if (toolBarModel.toolBarProfileShow) {
                    imgProfile.visibility = View.VISIBLE
                } else {
                    imgProfile.visibility = View.GONE
                }
            } else {
                imgProfile.visibility = View.GONE
                if (toolBarModel.toolBarProfileShow) {
                    imgMsg.visibility = View.VISIBLE
                } else {
                    imgMsg.visibility = View.GONE
                }
            }


            if (toolBarModel.imgAudiohow) {
                imgAudioCall.visibility = View.VISIBLE
            } else {
                imgAudioCall.visibility = View.GONE
            }

            if (toolBarModel.imgVideoShow) {
                imgVideoCall.visibility = View.VISIBLE
            } else {
                imgVideoCall.visibility = View.GONE
            }


            if (toolBarModel.bgToolBar != 0) {
                llToolBar.setBackgroundColor(
                    ContextCompat.getColor(
                        this@HomeActivity,
                        R.color.white
                    )
                )
                imgBackBtn.setImageDrawable(
                    ContextCompat.getDrawable(
                        this@HomeActivity,
                        R.drawable.ic_baseline_arrow_back_ios_red
                    )
                )
            } else {
                llToolBar.setBackgroundColor(
                    ContextCompat.getColor(
                        this@HomeActivity,
                        R.color.colorRed
                    )
                )
                imgBackBtn.setImageDrawable(
                    ContextCompat.getDrawable(
                        this@HomeActivity,
                        R.drawable.ic_baseline_arrow_back_ios_24
                    )
                )
            }
        }
    }

    fun callNavigationItems() {

        viewDataBinding?.navLayout?.apply {
            if (getCurPatientPackage()) {
                navAbout.visibility = View.GONE
                navNoti.visibility = View.GONE
                navTermCondition.visibility = View.GONE
                navPrivacy.visibility = View.GONE
                navSchedule.visibility = View.GONE
                navMediHistory.visibility = View.VISIBLE
                navMsg.visibility = View.VISIBLE
                navFav.visibility = View.VISIBLE
                navHelp.visibility = View.VISIBLE
                navReport.visibility = View.VISIBLE
                navReq.visibility = View.VISIBLE
            } else {
                navAbout.visibility = View.VISIBLE
                navNoti.visibility = View.VISIBLE
                navTermCondition.visibility = View.VISIBLE
                navPrivacy.visibility = View.VISIBLE
                navSchedule.visibility = View.VISIBLE
                navMediHistory.visibility = View.GONE
                navMsg.visibility = View.GONE
                navFav.visibility = View.GONE
                navHelp.visibility = View.GONE
                navReport.visibility = View.GONE
                navReq.visibility = View.GONE
            }
            navSchedule.setOnClickListener {
                callNavFragment(ScheduleActivityFragment())
            }
            navAppointment.setOnClickListener {
                callNavFragment(AppointmentFragment())
            }
            navTermCondition.setOnClickListener {
                callNavFragment(WebViewFragment(getString(R.string.Terms_Conditions)))
            }
            navPrivacy.setOnClickListener {
                callNavFragment(WebViewFragment(getString(R.string.Privacy_Policy)))
            }
            navAbout.setOnClickListener {
                callNavFragment(WebViewFragment(getString(R.string.About)))
            }
            navMsg.setOnClickListener {
                callNavFragment(MessageFragment())
            }
            navNoti.setOnClickListener {
                callNavFragment(NotificationFragment())
            }
            navFav.setOnClickListener {
                callNavFragment(FavSpecialistFragment())
            }
            navReq.setOnClickListener {
                callNavFragment(RequestFragment())
            }
            navReport.setOnClickListener {
                callNavFragment(ReportFragment())
            }
            navMediHistory.setOnClickListener {
                callNavFragment(MedicalHistoryFragment())
            }
            navHelp.setOnClickListener {
                callNavFragment(HelpFragment())
            }
            navReview.setOnClickListener {
                callNavFragment(RatingReviewFragment())
            }
            navWallet.setOnClickListener {
                callNavFragment(WalletFragment())
            }
            navSubs.setOnClickListener {
                callNavFragment(SubscriptionFragment(1))
            }
            navSettings.setOnClickListener {
                callNavFragment(SettingsFragment())
            }
            navLogout.setOnClickListener {
                logoutDialog()
            }
            navBilling.setOnClickListener {
                callNavFragment(BillingFragment())
            }
        }
    }

    fun callNavFragment(fragment: Fragment) {
        viewDataBinding?.drawerLayout?.closeDrawer(GravityCompat.START)
        displayItAddStack(fragment)
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            //  var fragment = supportFragmentManager.findFragmentById(R.id.frameContainer)
            supportFragmentManager.popBackStack()
        } else {
            finishAffinity()

        }
    }


    fun logoutDialog() {
        CustomDialog(this).openLogoutDialog(object :
            CustomDialog.DialogListener {
            override fun positiveBtn() {
                logout()
            }

            override fun positiveBtn(paramModel: ParamModel) {

            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val fragment = supportFragmentManager.findFragmentById(R.id.container)
        fragment!!.onActivityResult(requestCode, resultCode, data)

    }


    fun getCurrentLocation(): Boolean {
        var chk = false
        Dexter.withContext(this)
            .withPermissions(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
//                        val locationManager =
//                            getSystemService(Context.LOCATION_SERVICE) as LocationManager
//
//                        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
//                            OnGPS();
//                        } else {
//                            getLocation(locationManager);
//                        }

                        var gps = GPSTracker(this@HomeActivity)
                        // Check if GPS enabled
                        //       GlobalScope.async(Dispatchers.IO) {
                        var latitude = gps.latitude
                        var longitude = gps.longitude

                        //   withContext(Dispatchers.Main) {

                        if (gps.canGetLocation()) {
                            if (gps.location != null) {
                                chk = true
                                latitude = gps.latitude
                                longitude = gps.longitude

                            } else {

                            }
                        } else {
                            // Can't get location.
                            // GPS or network is not enabled.
                            chk = false
                            gps.showSettingsAlert()

                        }
                        //    }

                        //     }
//                        getCurLatLong()
//                        getCurrentLatlng()
                    }
                    if (report.isAnyPermissionPermanentlyDenied) {
                        showSettingsDialog(this@HomeActivity)
                    }

                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest?>?,
                    token: PermissionToken?
                ) {
                    chk = false
                    token?.continuePermissionRequest()
                }
            }).check()
        return chk
    }


}