package com.appsums.view.activities


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.appsums.application.GymApplication
import com.appsums.databinding.ActivitySplashBinding
import com.appsums.utils.GlobalConstants
import com.appsums.utils.SharedPrefClass
import kotlinx.coroutines.*


class SpalshActivity() : AppCompatActivity() {


    private val activityScope = CoroutineScope(Dispatchers.Main)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(ActivitySplashBinding.inflate(layoutInflater).root)

        //classConstructor(applicationContext)


        //val mAuthToken =
          //  SharedPrefClass().getPrefValue(GymApplication.instance, GlobalConstants.ACCESS_TOKEN).toString()
        //Log.d("mAuthToken", mAuthToken)


        activityScope.launch {
            delay(2000)
            startActivity(Intent(this@SpalshActivity, HomeActivity::class.java))

        }

    }


    fun classConstructor(context: Context) {
        val packageName: String = context.getPackageName()
        Toast.makeText(
            this@SpalshActivity,
            "" + packageName,
            Toast.LENGTH_LONG
        ).show()

    }

    override fun onPause() {
        super.onPause()
        // activityScope.cancel()
    }


}