package com.appsums.view.fragment

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.View
import com.appsums.R
import com.appsums.databinding.FragmentSelectAppDateBinding
import com.appsums.model.*
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.appsums.utils.CheckPermission
import com.appsums.utils.TakePictureUtils
import com.appsums.utils.cropimage.CropImage
import com.appsums.view.base.BaseFragment
import retrofit2.Response
import java.io.*


class SelectAppointmentFragment2(var model: HomeDataDocsModel, var bookModel: BookModel) :
    BaseFragment<FragmentSelectAppDateBinding>(FragmentSelectAppDateBinding::inflate) {

    private var binding: FragmentSelectAppDateBinding? = null
    private val tempImage = "tempImage"
    private var file: File? = null
    private var path: String? = null
    private var url: String = ""

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.toolBarTtl = getString(R.string.SELECT_APPOINTMENT_DATE)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = viewDataBinding
        callBookMark()
        viewDataBinding?.apply {
            ll3.visibility = View.VISIBLE


            imgFav.setOnClickListener {
                callAPi(
                    FavouriteModal(model._id, !model.isFavourite)
                )
            }

            setImageWithUrl(model.profilePic, img)
            tvName.text = model.fullName
            ratingStars.rating = model.rating.toFloat()
            when {
                model.experience_in_months == null -> {
                    tvExp.text =
                        "" + model.experience_in_years + "yrs " + "of Experience"
                }
                model.experience_in_years == null -> {
                    tvExp.text =
                        "" + model.experience_in_months + "mo " + "of Experience"
                }
                else -> {
                    tvExp.text =
                        "" + model.experience_in_months + "mo " + model.experience_in_years + "yrs " + "of Experience"
                }
            }

            underlineText(txtUploadDoc)

            cardViewImg.setOnClickListener {
                checkPermission()
            }

            btnCall.setOnClickListener {
                bookModel.reason = etReason.text.toString()
                bookModel.document = url
                callAPi(bookModel)
            }
        }
    }


    fun callAPi(body: String) {
        homeActivity?.let {
            ApiCall.uploadImg(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    url = mResponse.body()?.result?.url ?: ""
                    viewDataBinding?.txtUploadDoc?.setText(url)
                }

                override fun onError(msg: String) {
                    showToast(msg)
                    Log.d("Dddd==", msg.toString())
                }
            })
        }
    }


    fun callAPi(body: BookModel) {
        homeActivity?.let {
            ApiCall.bookAPi(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    popBack()
                    popBack()
                    popBack()
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    private fun checkPermission() {
        if (CheckPermission.checkIsMarshMallowVersion()) {
            if (CheckPermission.checkCameraStoragePermission(requireContext())) {
                showImageDailoge(requireActivity(), tempImage, false)
            } else {
                CheckPermission.requestCameraStoragePermission(requireActivity())
            }
        } else {
            showImageDailoge(requireActivity(), tempImage, false)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            CheckPermission.REQUEST_CODE_CAMERA_STORAGE_PERMISSION -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showImageDailoge(requireActivity(), tempImage, false)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (resultCode == Activity.RESULT_OK) when (requestCode) {
            TakePictureUtils.TAKE_PICTURE -> TakePictureUtils.startCropImage(
                context as Activity?,
                tempImage + ".jpg",
                500,
                500
            )
            TakePictureUtils.PICK_GALLERY -> {
                try {
                    val inputStream: InputStream? =
                        homeActivity!!.getContentResolver().openInputStream(
                            data!!.data!!
                        )
                    val fileOutputStream: FileOutputStream =
                        FileOutputStream(
                            File(
                                requireContext().getExternalFilesDir("temp"),
                                tempImage + ".jpg"
                            )
                        )
                    TakePictureUtils.copyStream(inputStream, fileOutputStream)
                    fileOutputStream.close()
                    inputStream?.close()
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                TakePictureUtils.startCropImage(context as Activity?, tempImage + ".jpg", 500, 500)
            }
            TakePictureUtils.CROP_FROM_CAMERA -> {
                path = null
                if (data != null) {
                    path = data.getStringExtra(CropImage.IMAGE_PATH)
                    if (path != null) file = File(path)
                    //    viewModel.callApiFileToUrl(context, file)
                    if (path == null) {
                        return
                    }
                    callAPi(path!!)
                    //  setImageWithUri(file, viewDataBinding?.imgProfileAvatar)
//                    Glide.with(context!!)
//                        .load(file)
//                        .placeholder(R.drawable.dummy)
//                        .into<Target<Drawable>>(viewDataBinding)
                }
            }
            TakePictureUtils.PICK_DOCUMENT -> {

                data?.data?.also { uri ->

                    // Perform ope
                }

            }
        }
    }

    fun callBookMark() {
        viewDataBinding?.apply {
            if (model.isFavourite) {
                setImgTint(imgFav, R.color.colorRed)
            } else {
                setImgTint(imgFav, R.color.colorGray4)
            }
        }
    }

    fun callAPi(body: FavouriteModal) {
        homeActivity?.let {
            ApiCall.favAPi(it, body, object :
                ResponseListener<GenericModel1> {
                override fun onSuccess(mResponse: Response<GenericModel1>) {
                    model.isFavourite = !model.isFavourite
                    callBookMark()

                }

                override fun onError(msg: String) {

                }
            })
        }
    }

}


