package com.appsums.view.fragment

import android.os.Bundle
import android.view.View
import com.appsums.R
import com.appsums.databinding.FragmentInvoiceDetailBinding
import com.appsums.model.ToolBarModel
import com.appsums.view.base.BaseFragment


class InvoiceDetailFragment :
    BaseFragment<FragmentInvoiceDetailBinding>(FragmentInvoiceDetailBinding::inflate) {

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.toolBarTtl = getString(R.string.INVOICE_DETAILS)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {


        }

    }


}