package com.appsums.view.fragment

import android.os.Bundle
import android.view.View
import com.appsums.R
import com.appsums.databinding.FragmentBankDetailBinding
import com.appsums.model.ToolBarModel
import com.appsums.view.base.BaseFragment


class BankDetailsFragment :
    BaseFragment<FragmentBankDetailBinding>(FragmentBankDetailBinding::inflate) {

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.toolBarTtl = getString(R.string.BANK_DETAILS)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {


            btn2.setOnClickListener {
                displayItAddStack(AddBankFragment())
            }


        }

    }


}