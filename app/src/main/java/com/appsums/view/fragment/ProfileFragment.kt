package com.appsums.view.fragment

import android.os.Bundle
import android.util.Log
import android.view.View
import com.appsums.CreateProfileFragment
import com.appsums.R
import com.appsums.databinding.FragmentProfileBinding
import com.appsums.model.GenericModel
import com.appsums.model.ToolBarModel
import com.appsums.model.UserData
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.appsums.utils.GlobalConstants
import com.appsums.utils.SharedPrefClass
import com.appsums.view.base.BaseFragment
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import retrofit2.Response

private const val TAG = "ProfileFragment"
class ProfileFragment :
    BaseFragment<FragmentProfileBinding>(FragmentProfileBinding::inflate) {

    private var model: UserData? = null

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        callAPi()

        viewDataBinding?.apply {

            imgBackBtn.setOnClickListener {
                popBack()
            }

            btnCall.setOnClickListener {
                if (selectPosition == 1) {
                    displayItAddStack(CreateMedicalFragment(1, model))
                } else {
                    displayItAddStack(CreateProfileFragment(1, model))
                }

            }

            if (tabLayout.tabCount <= 0) {
                tabLayout.addTab(tabLayout.newTab().setText(R.string.Personal_Info))
                tabLayout.addTab(tabLayout.newTab().setText(R.string.Medical_Info))
            }


            tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab?) {
                    selectPosition = tab?.position ?: 0

                    if (selectPosition == 1) {
                        llMedical.visibility = View.VISIBLE
                        llPersonal.visibility = View.GONE
                    } else {
                        llMedical.visibility = View.GONE
                        llPersonal.visibility = View.VISIBLE
                    }

                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {

                }

                override fun onTabReselected(tab: TabLayout.Tab?) {

                }
            })
        }
    }


    fun callAPi() {
        homeActivity?.let {
            ApiCall.profileApi(it, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    Log.d(TAG, mResponse.body()?.homeData.toString())

                    model = mResponse.body()?.userData

                    val model1 = Gson().toJson(model)
                    SharedPrefClass().putObject(it, GlobalConstants.USER_PROFILE, model1)

                    viewDataBinding?.apply {
                        setImageWithUrl(model?.profilePic, imgProfileAvatar)
                        tvFirstName.text = model?.firstName
                        tvLastName.text = model?.lastName
                        tvAddress.text = model?.location?.address
                        tvBlodgrp.text = model?.bloodGroup
                        tvDiaDetail.text = model?.diabetesDetails
                        tvDiabetes.text = model?.iSDiabetes.toString()
                        tvDob.text = model?.date_of_birth
                        tvGender.text = model?.gender
                        tvHt.text = model?.height
                        tvLifeStyle.text = model?.bio
                        tvMartialStatus.text = model?.marital_status
                        tvNotes.text = model?.familyNotes
                        tvwt.text = model?.weight
                        tvallergy.text = model?.allergyDeatils

                    }


                    SharedPrefClass().putObject(
                        it,
                        GlobalConstants.EMAIL,
                        model?.email.toString()
                    )
                    SharedPrefClass().putObject(
                        it,
                        GlobalConstants.NAME,
                        model?.firstName.toString()
                    )
                    SharedPrefClass().putObject(
                        it,
                        GlobalConstants.PIC,
                        model?.profilePic.toString()
                    )

                    homeActivity?.updateNavHeader()
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

}