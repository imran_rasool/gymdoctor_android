package com.appsums.view.fragment

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.appsums.R
import com.appsums.databinding.FragmentMedHisBinding
import com.appsums.databinding.ItemMedHistoryBinding
import com.appsums.model.ToolBarModel
import com.appsums.view.adapters.RecyclerCallback
import com.appsums.view.adapters.RecyclerViewGenricAdapter
import com.appsums.view.base.BaseFragment


class MedicalHistoryFragment : BaseFragment<FragmentMedHisBinding>(FragmentMedHisBinding::inflate) {

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.notiIconShow = true
        toolBarModel.toolBarProfileShow = true
        toolBarModel.toolBarTtl = getString(R.string.Medical_History)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            recyclerView.layoutManager = LinearLayoutManager(requireContext())
            setAdapter1()

        }

    }

    fun setAdapter1() {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        val rvAdapProgress = RecyclerViewGenricAdapter<String, ItemMedHistoryBinding>(
            list,
            R.layout.item_med_history, object :
                RecyclerCallback<ItemMedHistoryBinding, String> {
                override fun bindData(
                    binder: ItemMedHistoryBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {
                        btnCall1.setOnClickListener {
                            displayItAddStack(PrescriptionFragment())
                        }

                        btnCall2.setOnClickListener {
                            displayItAddStack(MedicalRecordFragment())
                        }

                        btnCall3.setOnClickListener {
                            displayItAddStack(BillingFragment())
                        }

                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }

}