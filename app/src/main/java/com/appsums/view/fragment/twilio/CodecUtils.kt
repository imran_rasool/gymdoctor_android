@file:JvmName("CodecUtils")
package com.appsums.view.fragment.twilio

import tvi.webrtc.HardwareVideoDecoderFactory
import tvi.webrtc.HardwareVideoEncoderFactory

/**
 *<h1>com.appsums.view.fragment.twilio</h1>

 *<p>This class used to  in the application</p>


 *@since 27/01/22
 *@updated 27/01/22
 *@version 1.0
 **/
public fun isH264Supported(): Boolean {
    val hardwareVideoEncoderFactory =
        HardwareVideoEncoderFactory(null, true, true)
    val hardwareVideoDecoderFactory =
        HardwareVideoDecoderFactory(null)

    val h264EncoderSupported = hardwareVideoEncoderFactory.supportedCodecs.any {
        it.name.equals("h264", ignoreCase = true)
    }
    val h264DecoderSupported = hardwareVideoDecoderFactory.supportedCodecs.any {
        it.name.equals("h264", ignoreCase = true)
    }

    return h264EncoderSupported && h264DecoderSupported
}