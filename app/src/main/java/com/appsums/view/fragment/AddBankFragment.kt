package com.appsums.view.fragment

import android.os.Bundle
import android.view.View
import com.appsums.R
import com.appsums.databinding.FragmentAddBankBinding
import com.appsums.model.ToolBarModel
import com.appsums.view.base.BaseFragment


class AddBankFragment :
    BaseFragment<FragmentAddBankBinding>(FragmentAddBankBinding::inflate) {

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.toolBarTtl = getString(R.string.ADD_BANK_DETAILS)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            btnCall.setOnClickListener { displayItAddStack(BankDetailsFragment()) }


        }

    }


}