package com.appsums.view.fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import com.appsums.AppointmentViewDetailFragment
import com.appsums.R
import com.appsums.databinding.FragmentAppointmentDetailBinding
import com.appsums.model.*
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.appsums.utils.GlobalConstants
import com.appsums.view.base.BaseFragment
import com.appsums.view.fragment.twilio.VideoActivity
import retrofit2.Response
import java.text.DateFormat
import java.text.SimpleDateFormat


class AppointmentDetailFragment(val selectTab: Int? = null, var _id: String? = "") :
    BaseFragment<FragmentAppointmentDetailBinding>(FragmentAppointmentDetailBinding::inflate) {

    private var model1: AppointmentModel? = null

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.toolBarTtl = getString(R.string.Appointment_Details)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            if (selectTab == 1) {
                  llBtn.visibility = View.GONE
                cardPdf.visibility = View.GONE
                llBtn2.visibility = View.VISIBLE
                btnAppDetail.visibility = View.VISIBLE
            } else {
                llBtn.visibility = View.VISIBLE
                cardPdf.visibility = View.VISIBLE
                llBtn2.visibility = View.GONE
                btnAppDetail.visibility = View.GONE
            }

            btnAppDetail.setOnClickListener {
                displayItAddStack(AppointmentViewDetailFragment())
            }

            btnVideoCall.setOnClickListener {
                callAPi(TwillioModel(_id, model1?.serviceProviderId?._id, "video"))
            }

//            btnAudioCall.setOnClickListener {
//                callAPi(TwillioModel(_id, model1?.serviceProviderId?._id, "audio"))
//            }

            btnReshedule.setOnClickListener {
                displayItAddStack(InvoiceDetailFragment())
            }

            btnAddRating.setOnClickListener {
                displayItAddStack(AddRatingFragment())
            }
        }
        callAPi(_id ?: return)
    }

    fun callAPi(id: String) {
        homeActivity?.let {
            ApiCall.viewAppointment(it, id, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    model1 = mResponse.body()?.appointmentDetails

                    viewDataBinding?.apply {
                        if (model1?.serviceProviderId != null) {
                            var model = model1?.serviceProviderId
                            when {
                                model?.experience_in_months == null -> {
                                    tvExp.text =
                                        "" + model?.experience_in_years + "yrs " + "of Experience"
                                }
                                model?.experience_in_years == null -> {
                                    tvExp.text =
                                        "" + model?.experience_in_months + "mo " + "of Experience"
                                }
                                else -> {
                                    tvExp.text =
                                        "" + model?.experience_in_months + "mo " + model?.experience_in_years + "yrs " + "of Experience"
                                }
                            }

                            setImageWithUrl(model?.profilePic, img)
                            tvName.text = model?.fullName
                            ratingStars.rating = (model?.rating ?: 0).toFloat()
                            if (!model?.secondry_specialty.isNullOrEmpty())
                                tvType.text = model?.secondry_specialty?.get(0)?.secondry_specialty
                            tvFees.text = "$" + model1?.amount + " Consultation Fee"
                        }
                        callBookMark()
                        imgFav.setOnClickListener {
                            callAPi(
                                FavouriteModal(model1?._id ?: "", !(model1?.isFavourite ?: true))
                            )
                        }

                        tvReason.setText(model1?.reason)
                        val formatter1: DateFormat =
                            SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss.SSS'Z'")
                        val formatterDate: DateFormat = SimpleDateFormat("dd MMM yyyy")
                        val formatterTime: DateFormat = SimpleDateFormat("hh:mm a")
                        var dateFormat = ""
                        var timeFormat = ""
                        var endtimeFormat = ""
                        try {
//                            dateFormat=formatterDate.format(formatter1.parse("2022-01-16T12:23:34.000Z"))
                            dateFormat =
                                formatterDate.format(formatter1.parse(model1?.appointmentStartTime))
                            timeFormat =
                                formatterTime.format(formatter1.parse(model1?.appointmentStartTime))
                            endtimeFormat =
                                formatterTime.format(formatter1.parse(model1?.appointmentEndTime))
                        } catch (e: Exception) {
                            Log.d("Dddd", e.toString())
                        }
                        tvDate.setText(dateFormat)
                        tvTime.setText(timeFormat + "-" + endtimeFormat)
                    }
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    fun callBookMark() {
        viewDataBinding?.apply {
            if (model1?.isFavourite == true) {
                setImgTint(imgFav, R.color.colorRed)
            } else {
                setImgTint(imgFav, R.color.colorGray4)
            }
        }
    }

    fun callAPi(body: FavouriteModal) {
        homeActivity?.let {
            ApiCall.favAPi(it, body, object :
                ResponseListener<GenericModel1> {
                override fun onSuccess(mResponse: Response<GenericModel1>) {
                    model1?.isFavourite = !(model1?.isFavourite ?: true)
                    callBookMark()
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    fun callAPi(body: TwillioModel) {
        homeActivity?.let {
            ApiCall.getTwillioTokenApi(it, body, object :
                ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    val model = mResponse.body()
                    if (body.pushType==GlobalConstants.AUDIO) {
                        val intent = Intent(context, VideoAudioActivity::class.java)
                        intent.putExtra(GlobalConstants.TWILLIO_TOKEN, model?.twilioToken)
                        intent.putExtra(GlobalConstants.ROOM_ID, model?.roomName)
                        intent.putExtra(GlobalConstants.SENDER_ID, "senderId")
                        intent.putExtra(GlobalConstants.FIRST_NAME, "firstName")
                        intent.putExtra(GlobalConstants.LAST_NAME, "lastName")
                        intent.putExtra(GlobalConstants.RECEIVER_ID, "receiverId")
                        intent.putExtra(GlobalConstants.CALLING, GlobalConstants.DIAL_CALL)
                        intent.putExtra(GlobalConstants.PUSH_TYPE, body.pushType)
                        startActivity(intent)
                    }else {
                        val intent = Intent(context, VideoActivity::class.java)
                        intent.putExtra(GlobalConstants.TWILLIO_TOKEN, model?.twilioToken)
                        intent.putExtra(GlobalConstants.ROOM_ID, model?.roomName)
                        intent.putExtra(GlobalConstants.SENDER_ID, "senderId")
                        intent.putExtra(GlobalConstants.FIRST_NAME, "firstName")
                        intent.putExtra(GlobalConstants.LAST_NAME, "lastName")
                        intent.putExtra(GlobalConstants.RECEIVER_ID, "receiverId")
                        intent.putExtra(GlobalConstants.CALLING, GlobalConstants.DIAL_CALL)
                        intent.putExtra(GlobalConstants.PUSH_TYPE, body.pushType)
                        startActivity(intent)
                    }
                }

                override fun onError(msg: String) {
                }

            })
        }
    }

}