package com.appsums.view.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.appsums.R
import com.appsums.databinding.*
import com.appsums.model.GenericModel
import com.appsums.model.ToolBarModel
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.appsums.view.adapters.RecyclerCallback
import com.appsums.view.adapters.RecyclerViewGenricAdapter
import com.appsums.view.base.BaseFragment
import com.google.gson.Gson
import retrofit2.Response

private const val TAG = "WalletFragment"

class WalletFragment : BaseFragment<FragmentWalletBinding>(FragmentWalletBinding::inflate) {

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.notiIconShow = true
        toolBarModel.toolBarProfileShow = true
        toolBarModel.toolBarTtl = getString(R.string.Wallet)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            underlineText(tvUpdateSubs)

            if (getCurPatientPackage()) {
                tvAvailableBal.visibility = View.VISIBLE
                btn1.visibility = View.GONE
            } else {
                tvAvailableBal.visibility = View.GONE
                btn1.visibility = View.VISIBLE
            }

            tvUpdateSubs.setOnClickListener {
                //   displayItAddStack(SubscriptionFragment(1))
            }

            btn1.setOnClickListener {
                displayItAddStack(WithdrawAmountFragment())
            }

            //recyclerView.layoutManager = LinearLayoutManager(requireContext())
            // setAdapter1()
        }

        callAPi()
    }

    fun callAPi() {
        homeActivity?.let {
            ApiCall.myWalletAmount(it, object : ResponseListener<GenericModel> {
                @SuppressLint("SetTextI18n")
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    Log.d(TAG, "onSuccess: ${Gson().toJson(mResponse.body())}")
                    val model = mResponse.body()?.walletData

                    viewDataBinding.apply {
                        this!!.tvWalletAmount.text =
                            "$" + model?.totalBalance
                        tvNumberOfSession.text = "Number of Sessions: " + model?.sessions
                        tvNumberOfMessage.text = "Number of Messages: " + model?.numberOfMessages

                    }
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    fun setAdapter1() {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        val rvAdapProgress = RecyclerViewGenricAdapter<String, ItemTransactionBinding>(
            list,
            R.layout.item_transaction, object :
                RecyclerCallback<ItemTransactionBinding, String> {
                override fun bindData(
                    binder: ItemTransactionBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }

}