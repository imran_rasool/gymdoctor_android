package com.appsums.view.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.MotionEvent
import android.view.View
import com.appsums.R
import com.appsums.databinding.FragmentPhysicanBinding
import com.appsums.databinding.ItemPhysicanBinding
import com.appsums.model.*
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.appsums.utils.GlobalConstants.FILTER_OBJECT
import com.appsums.utils.GlobalConstants.requestKey
import com.appsums.utils.setFragmentResultListener
import com.appsums.view.adapters.RecyclerCallback
import com.appsums.view.adapters.RecyclerViewGenricAdapter
import com.appsums.view.base.BaseFragment
import com.google.gson.Gson
import retrofit2.Response

private const val TAG = "PhysicanFragment"

class PhysicanFragment(val _id: String) :
    BaseFragment<FragmentPhysicanBinding>(FragmentPhysicanBinding::inflate) {

    private var latLngSprciltyModel: LatLngSprciltyModel? = null
    val list = ArrayList<HomeDataDocsModel>()
    private var rvAdapProgress: RecyclerViewGenricAdapter<HomeDataDocsModel, ItemPhysicanBinding>? =
        null

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.toolBarTtl = getString(R.string.FIND_PHYSICIANS)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            //recyclerView.layoutManager = LinearLayoutManager(requireContext())
            setAdapter1()

            etSearch.setOnTouchListener(View.OnTouchListener { _, event ->
                val DRAWABLE_LEFT = 0
                val DRAWABLE_TOP = 1
                val DRAWABLE_RIGHT = 2
                val DRAWABLE_BOTTOM = 3
                if (event.action == MotionEvent.ACTION_UP) {
                    if (event.rawX >= etSearch.right - etSearch.compoundDrawables[DRAWABLE_RIGHT].bounds.width()
                    ) {
                        displayItAddStack(FilterFragment(_id))

                        return@OnTouchListener true
                    }
                }
                false
            })

            etSearch.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    if (s != null && s.isNotEmpty()) {
                        rvAdapProgress?.filter?.filter(s.toString())
                        recyclerView.visibility = View.VISIBLE
                    } else {
                        rvAdapProgress?.filter?.filter("")
                    }
                }

                override fun afterTextChanged(s: Editable) {
                }
            })


        }

        if (latLngSprciltyModel == null && list.isNullOrEmpty()) {
            latLngSprciltyModel =
                LatLngSprciltyModel(serviceProviderTypeId = _id, lat = lat, long = long)

            callAPi(latLngSprciltyModel!!)
        }

        setFragmentResultListener(
            requestKey
        ) { _, result ->
            result.getString(FILTER_OBJECT)?.let { note ->
                val gson = Gson()
                latLngSprciltyModel =
                    gson.fromJson(note, LatLngSprciltyModel::class.java)
            }
            callAPi(
                latLngSprciltyModel!!
            )
            Log.d("Dddd222", latLngSprciltyModel.toString())
        }


    }


    fun setAdapter1() {

        rvAdapProgress = RecyclerViewGenricAdapter<HomeDataDocsModel, ItemPhysicanBinding>(
            list,
            R.layout.item_physican, object :
                RecyclerCallback<ItemPhysicanBinding, HomeDataDocsModel> {
                @SuppressLint("SetTextI18n")
                override fun bindData(
                    binder: ItemPhysicanBinding,
                    model: HomeDataDocsModel,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {


                        setImageWithUrl(model.profilePic, img)
                        tvName.text = model.fullName
                        ratingStars.rating = model.rating.toFloat()
                        when (model.experience_in_years) {
                            else -> {
                                tvExp.text =
                                    "" + model.experience_in_months + "mo " + model.experience_in_years + "yrs " + "of Experience"
                            }
                        }
                        tvFees.text = "$" + model.serviceCharge + " Consultation Fee"

                        btnBookApp.setOnClickListener {

                            displayItAddStack(SelectAppointmentFragment(model))
                        }

                        llDetail.setOnClickListener {
                            displayItAddStack(PhysicanDetailFragment())
                        }

                        if (model.isFavourite) {
                            setImgTint(imgFav, R.color.colorRed)
                        } else {
                            setImgTint(imgFav, R.color.colorGray4)
                        }

                        imgFav.setOnClickListener {

                            callAPi(
                                FavouriteModal(list[position]._id, !list[position].isFavourite),
                                position
                            )
                        }

                    }
                }

                override fun getFilterChar(row: HomeDataDocsModel, s: String): Boolean {
                    if ((row.fullName?.toLowerCase()).contains(s)) {
                        return true
                    }
                    return false
                }
            }
        )
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }


    fun callAPi(body: LatLngSprciltyModel) {
        homeActivity?.let {
            ApiCall.nearByServiceProviderAPi(it, body, object :
                ResponseListener<GenericModel1> {
                override fun onSuccess(mResponse: Response<GenericModel1>) {
                    val model = mResponse.body()
                    Log.d(TAG, "onSuccess: ${Gson().toJson(model)}")
                    list.clear()
                    list.addAll(model?.userData!!)
                    rvAdapProgress?.notifyDataSetChanged()
                }

                override fun onError(msg: String) {

                }
            })
        }


    }

    fun callAPi(body: FavouriteModal, position: Int) {
        homeActivity?.let {
            ApiCall.favAPi(it, body, object :
                ResponseListener<GenericModel1> {
                override fun onSuccess(mResponse: Response<GenericModel1>) {
                    list[position].isFavourite = !list[position].isFavourite
                    rvAdapProgress?.notifyDataSetChanged()
                }

                override fun onError(msg: String) {

                }
            })
        }
    }


}