package com.appsums.view.fragment

import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.widget.TextView
import androidx.core.view.children
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.appsums.R
import com.appsums.databinding.*
import com.appsums.model.*
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.appsums.utils.widgets.calenderWidget.daysOfWeekFromLocale
import com.appsums.utils.widgets.calenderWidget.model.CalendarDay
import com.appsums.utils.widgets.calenderWidget.model.CalendarMonth
import com.appsums.utils.widgets.calenderWidget.model.DayOwner
import com.appsums.utils.widgets.calenderWidget.setTextColorRes
import com.appsums.utils.widgets.calenderWidget.ui.DayBinder
import com.appsums.utils.widgets.calenderWidget.ui.MonthHeaderFooterBinder
import com.appsums.utils.widgets.calenderWidget.ui.ViewContainer
import com.appsums.utils.widgets.calenderWidget.utils.next
import com.appsums.utils.widgets.calenderWidget.utils.previous
import com.appsums.view.adapters.RecyclerCallback
import com.appsums.view.adapters.RecyclerViewGenricAdapter
import com.appsums.view.base.BaseFragment
import retrofit2.Response
import java.io.*
import java.time.LocalDate
import java.time.YearMonth
import java.time.format.DateTimeFormatter
import java.time.format.TextStyle
import java.util.*


class SelectAppointmentFragment(var model: HomeDataDocsModel) :
    BaseFragment<FragmentSelectAppDateBinding>(FragmentSelectAppDateBinding::inflate) {

    private val list= ArrayList<SloGetData>()
    private var rvAdapProgress: RecyclerViewGenricAdapter<SloGetData, ItemSlotsBinding>?=null
    private var selectedDate: LocalDate? = null
    private val monthTitleFormatter = DateTimeFormatter.ofPattern("MMMM")
    private var binding: FragmentSelectAppDateBinding? = null
    private val today = LocalDate.now()
    var selectposition=-1

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.toolBarTtl = getString(R.string.SELECT_APPOINTMENT_DATE)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = viewDataBinding
        selectedDate=today
        setCalenderData(binding!!)
        callBookMark()
        viewDataBinding?.apply {
            ll1.visibility = View.VISIBLE

            tvSelectDate.setText(selectedDate.toString())
            tvSelectDate1.setText(selectedDate.toString())

            imgFav.setOnClickListener {
                callAPi(
                    FavouriteModal(model._id, !model.isFavourite)
                )
            }


            setImageWithUrl(model.profilePic, img)
            tvName.text = model.fullName
            ratingStars.rating = model.rating.toFloat()
            when {
                model.experience_in_months == null -> {
                    tvExp.text =
                        "" + model.experience_in_years + "yrs " + "of Experience"
                }
                model.experience_in_years == null -> {
                    tvExp.text =
                        "" + model.experience_in_months + "mo " + "of Experience"
                }
                else -> {
                    tvExp.text =
                        "" + model.experience_in_months + "mo " + model.experience_in_years + "yrs " + "of Experience"
                }
            }
            //  tvFees.text = "$" + model.serviceCharge + " Consultation Fee"


            recyclerView.layoutManager = GridLayoutManager(context, 2)
            setAdapter1(recyclerView)
            underlineText(txtUploadDoc)


            btnCall.setOnClickListener {
                if (selectedDate==null){
                    showToast("Please select date")
                }else {
                    displayItAddStack(SelectAppointmentFragment1(model,selectedDate!!))
                }
           }
        }
    }

    fun setAdapter1(recyclerView: RecyclerView) {

        rvAdapProgress = RecyclerViewGenricAdapter<SloGetData, ItemSlotsBinding>(
            list,
            R.layout.item_slots, object :
                RecyclerCallback<ItemSlotsBinding, SloGetData> {
                override fun bindData(
                    binder: ItemSlotsBinding,
                    model: SloGetData,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                        tvTime.text = model.slotTime
                        if (selectposition==position){
//                            llMain.background =
//                                ContextCompat.getDrawable(requireContext(), R.drawable.edit_bg_red)
                            tvTime.setTextColor(setColor(R.color.colorRed))
                        }else{
//                            llMain.background =
//                                ContextCompat.getDrawable(requireContext(), R.drawable.edit_bg_red)
                            tvTime.setTextColor(setColor(R.color.black))
                        }

                        llMain.setOnClickListener {
                            selectposition=position
                            rvAdapProgress?.notifyDataSetChanged()

                        }

                    }
                }
            })
        recyclerView.adapter = rvAdapProgress
        recyclerView.isNestedScrollingEnabled = false
    }


    fun setCalenderData(binding: FragmentSelectAppDateBinding) {
        val daysOfWeek = daysOfWeekFromLocale()
        var currentMonth = YearMonth.now()
        binding.exFiveCalendar.setup(
            currentMonth.minusMonths(10),
            currentMonth.plusMonths(10),
            daysOfWeek.first()
        )
        binding.exFiveCalendar.scrollToMonth(currentMonth)

        class DayViewContainer(view: View) : ViewContainer(view) {
            lateinit var day: CalendarDay // Will be set when this container is bound.
            val binding = CalendarDayBinding.bind(view)

            init {
                view.setOnClickListener {
                    if (day.owner == DayOwner.THIS_MONTH) {
                        if (selectedDate != day.date) {
                            val oldDate = selectedDate
                            selectedDate = day.date
                            val binding = this@SelectAppointmentFragment.binding!!
                            binding.exFiveCalendar.notifyDateChanged(day.date)
                            oldDate?.let { binding.exFiveCalendar.notifyDateChanged(it) }
                            updateAdapterForDate(day.date)
                        }
                    }
                }
            }
        }
        binding.exFiveCalendar.dayBinder = object : DayBinder<DayViewContainer> {
            override fun create(view: View) = DayViewContainer(view)
            override fun bind(container: DayViewContainer, day: CalendarDay) {
                container.day = day
                val textView = container.binding.exFiveDayText
                val layout = container.binding.exFiveDayLayout
                textView.text = day.date.dayOfMonth.toString()

                val flightTopView = container.binding.exFiveDayFlightTop
                val flightBottomView = container.binding.exFiveDayFlightBottom
                flightTopView.background = null
                flightBottomView.background = null

//                if (today == day.date) {
//                  //  selectedDate=today
//                    textView.setTextColorRes(R.color.colorRed)
//                    layout.setBackgroundResource(R.color.colorRed)
//                } else
                    if (day.owner == DayOwner.THIS_MONTH) {
                    textView.setTextColorRes(R.color.colorRed)
                    layout.setBackgroundResource(if (selectedDate == day.date) R.color.colorRed else 0)

                    binding.tvSelectDate.setText(selectedDate.toString())
                    binding.tvSelectDate1.setText(selectedDate.toString())

//                    val flights = flights[day.date]
//                    if (flights != null) {
//                        if (flights.count() == 1) {
//                            flightBottomView.setBackgroundColor(view?.context.getColorCompat(flights[0].color))
//                        } else {
//                            flightTopView.setBackgroundColor(view?.context.getColorCompat(flights[0].color))
//                            flightBottomView.setBackgroundColor(view?.context.getColorCompat(flights[1].color))
//                        }
//                    }
                } else {
                    textView.setTextColorRes(R.color.colorGray)
                    layout.background = null
                }
            }
        }

        class MonthViewContainer(view: View) : ViewContainer(view) {
            val legendLayout = CalendarHeaderBinding.bind(view).legendLayout.root
        }
        binding.exFiveCalendar.monthHeaderBinder = object :
            MonthHeaderFooterBinder<MonthViewContainer> {
            override fun create(view: View) = MonthViewContainer(view)
            override fun bind(container: MonthViewContainer, month: CalendarMonth) {
                // Setup each header day text if we have not done that already.
                if (container.legendLayout.tag == null) {
                    container.legendLayout.tag = month.yearMonth
                    container.legendLayout.children.map { it as TextView }
                        .forEachIndexed { index, tv ->
                            tv.text =
                                daysOfWeek[index].getDisplayName(TextStyle.SHORT, Locale.ENGLISH)
                                    .toUpperCase(Locale.ENGLISH)
                            tv.setTextColorRes(R.color.colorRed)
                            tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f)
                        }
                    month.yearMonth
                }
            }
        }

        binding.exFiveCalendar.monthScrollListener = { month ->
            val title = "${monthTitleFormatter.format(month.yearMonth)} ${month.yearMonth.year}"
            binding.exFiveMonthYearText.text = title

            selectedDate?.let {
                // Clear selection if we scroll to a new month.
                selectedDate = null
                binding.exFiveCalendar.notifyDateChanged(it)
                updateAdapterForDate(null)
            }
        }

        binding.exFiveNextMonthImage.setOnClickListener {
            binding.exFiveCalendar.findFirstVisibleMonth()?.let {
                binding.exFiveCalendar.smoothScrollToMonth(it.yearMonth.next)
            }
        }

        binding.exFivePreviousMonthImage.setOnClickListener {
            binding.exFiveCalendar.findFirstVisibleMonth()?.let {
                binding.exFiveCalendar.smoothScrollToMonth(it.yearMonth.previous)
            }
        }
    }

    private fun updateAdapterForDate(date: LocalDate?) {
        Log.d("dddd", "dsdsdsdsds")
//        flightsAdapter.flights.clear()
//        flightsAdapter.flights.addAll(flights[date].orEmpty())
//        flightsAdapter.notifyDataSetChanged()
    }


    fun callBookMark() {
        viewDataBinding?.apply {
            if (model.isFavourite) {
                setImgTint(imgFav, R.color.colorRed)
            } else {
                setImgTint(imgFav, R.color.colorGray4)
            }
        }
    }

    fun callAPi(body: FavouriteModal) {
        homeActivity?.let {
            ApiCall.favAPi(it, body, object :
                ResponseListener<GenericModel1> {
                override fun onSuccess(mResponse: Response<GenericModel1>) {
                    model.isFavourite = !model.isFavourite
                    callBookMark()

                }

                override fun onError(msg: String) {

                }
            })
        }
    }

}