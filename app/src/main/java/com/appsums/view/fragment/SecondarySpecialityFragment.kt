package com.appsums.view.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.appsums.R
import com.appsums.databinding.FragmentSecondrySpecialityBinding
import com.appsums.databinding.ItemSpecialityBinding
import com.appsums.model.GenericModel
import com.appsums.model.SubscriptionModel
import com.appsums.model.ToolBarModel
import com.appsums.network.ApiCallDoctor
import com.appsums.network.ResponseListener
import com.appsums.view.adapters.RecyclerCallback
import com.appsums.view.adapters.RecyclerViewGenricAdapter
import com.appsums.view.base.BaseFragment
import retrofit2.Response


class SecondarySpecialityFragment(val from: Int) :
    BaseFragment<FragmentSecondrySpecialityBinding>(FragmentSecondrySpecialityBinding::inflate) {

    private var rvAdapProgress: RecyclerViewGenricAdapter<SubscriptionModel, ItemSpecialityBinding>? =
        null
    val list = ArrayList<SubscriptionModel>()

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {
            if (from == 1) {
                tvTtl.setText(getString(R.string.Select_Secondary_Speciality))
            } else {
                tvTtl.setText(getString(R.string.Select_Primary_Speciality))
            }
            recyclerView.layoutManager = LinearLayoutManager(requireContext())
            setAdapter1(recyclerView)

            btnClose.setOnClickListener {
                popBack()
            }

            etSearch.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    if (s != null && s.isNotEmpty()) {
                        rvAdapProgress?.filter?.filter(s.toString())
                        recyclerView.visibility = View.VISIBLE
                    } else {
                        rvAdapProgress?.filter?.filter("")
                    }
                }

                override fun afterTextChanged(s: Editable) {
                }
            })
        }

        if (from == 1) {
            callAPi(ClientInfoFragment.primaryId)
        } else {
            callAPi()
        }
    }

    fun setAdapter1(recyclerView: RecyclerView) {
        rvAdapProgress = RecyclerViewGenricAdapter<SubscriptionModel, ItemSpecialityBinding>(
            list,
            R.layout.item_speciality, object :
                RecyclerCallback<ItemSpecialityBinding, SubscriptionModel> {
                override fun bindData(
                    binder: ItemSpecialityBinding,
                    model: SubscriptionModel,
                    position: Int,
                    itemView: View
                ) {
                    binder.apply {

                        if (from == 1)
                            tvName.text = model.secondry_specialty
                        else {
                            tvName.text = model.primary_specialty
                        }

                        llRoot.setOnClickListener {
                            if (from == 1) {
                                ClientInfoFragment.secondaryId = model._id
                                ClientInfoFragment.secondaryName = model.secondry_specialty
                            } else {
                                ClientInfoFragment.primaryId = model._id
                                ClientInfoFragment.primaryName = model.primary_specialty
                            }

                            popBack()
                        }

                    }
                }

                override fun getFilterChar(row: SubscriptionModel, s: String): Boolean {
                    if ((row.primary_specialty?.toLowerCase()).contains(s)) {
                        return true
                    }
                    return false
                }
            })
        recyclerView.adapter = rvAdapProgress
        recyclerView.isNestedScrollingEnabled = false
    }

    fun callAPi(body: String) {
        homeActivity?.let {
            ApiCallDoctor.secondrySpecialtyList(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    list.clear()
                    mResponse.body()?.documentList?.let { it1 -> list.addAll(it1) }
                    rvAdapProgress?.notifyDataSetChanged()
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    fun callAPi() {
        homeActivity?.let {
            ApiCallDoctor.primarySpecialtyListApi(it, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {

                    list.clear()
                    mResponse.body()?.documentList?.let { it1 -> list.addAll(it1) }
                    rvAdapProgress?.notifyDataSetChanged()

                }

                override fun onError(msg: String) {

                }
            })
        }
    }

}