package com.appsums.view.fragment

import android.os.Bundle
import android.view.View
import com.appsums.R
import com.appsums.databinding.FragmentAddCardBinding
import com.appsums.databinding.FragmentChangePassBinding
import com.appsums.databinding.FragmentSetttingsBinding
import com.appsums.model.ToolBarModel
import com.appsums.view.base.BaseFragment


class ChangePasswordFragment :
    BaseFragment<FragmentChangePassBinding>(FragmentChangePassBinding::inflate) {

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.toolBarTtl = getString(R.string.Change_Password)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {


        }

    }


}