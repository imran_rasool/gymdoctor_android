package com.appsums.view.fragment

import android.os.Bundle
import android.view.View
import com.appsums.R
import com.appsums.databinding.FragmentInviteFrdBinding
import com.appsums.model.ToolBarModel
import com.appsums.view.base.BaseFragment


class InviteFrdFragment :
    BaseFragment<FragmentInviteFrdBinding>(FragmentInviteFrdBinding::inflate) {

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.toolBarTtl = getString(R.string.Invite_Friends)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            tvCheckReport.setOnClickListener {
                displayItAddStack(InvitationCheckingFragment())

            }

        }

    }


}