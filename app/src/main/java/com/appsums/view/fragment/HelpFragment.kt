package com.appsums.view.fragment

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.appsums.R
import com.appsums.databinding.FragmentHelpBinding
import com.appsums.databinding.ItemHelpBinding
import com.appsums.model.ToolBarModel
import com.appsums.view.adapters.RecyclerCallback
import com.appsums.view.adapters.RecyclerViewGenricAdapter
import com.appsums.view.base.BaseFragment


class HelpFragment : BaseFragment<FragmentHelpBinding>(FragmentHelpBinding::inflate) {

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.notiIconShow = true
        toolBarModel.toolBarProfileShow = true
        toolBarModel.toolBarTtl = getString(R.string.Help)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            underlineText(tvSubmitNewReq)

            tvSubmitNewReq.setOnClickListener {
                displayItAddStack(SendRequestFragment())
            }

            recyclerView.layoutManager = LinearLayoutManager(requireContext())
            setAdapter1()

        }

    }

    fun setAdapter1() {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        val rvAdapProgress = RecyclerViewGenricAdapter<String, ItemHelpBinding>(
            list,
            R.layout.item_help, object :
                RecyclerCallback<ItemHelpBinding, String> {
                override fun bindData(
                    binder: ItemHelpBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                        llMain.setOnClickListener {
                            displayItAddStack(Help1Fragment())
                        }

                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }

}