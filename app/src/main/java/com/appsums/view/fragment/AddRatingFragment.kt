package com.appsums.view.fragment

import android.os.Bundle
import android.view.View
import com.appsums.R
import com.appsums.databinding.FragmentAddRatingReviewBinding
import com.appsums.model.ToolBarModel
import com.appsums.view.base.BaseFragment


class AddRatingFragment :
    BaseFragment<FragmentAddRatingReviewBinding>(FragmentAddRatingReviewBinding::inflate) {

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.toolBarTtl = getString(R.string.ADD_RATINGS_REVIEW)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {


        }

    }


}