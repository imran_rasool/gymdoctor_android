package com.appsums.view.fragment

import android.os.Bundle
import android.util.Log
import android.view.View
import com.appsums.R
import com.appsums.databinding.FragmentNotiBinding
import com.appsums.databinding.ItemNotiBinding
import com.appsums.model.GenericModel
import com.appsums.model.NotificationModel1
import com.appsums.model.ParamModel
import com.appsums.model.ToolBarModel
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.appsums.view.adapters.RecyclerCallback
import com.appsums.view.adapters.RecyclerViewGenricAdapter
import com.appsums.view.base.BaseFragment
import com.appsums.view.base.CustomDialog
import com.google.gson.Gson
import retrofit2.Response

private const val TAG = "NotificationFragment"

class NotificationFragment : BaseFragment<FragmentNotiBinding>(FragmentNotiBinding::inflate) {

    private var rvAdapNotification: RecyclerViewGenricAdapter<NotificationModel1, ItemNotiBinding>? = null
    val list = ArrayList<NotificationModel1>()

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.toolBarTtl = getString(R.string.NOTIFICATIONS)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            callAPi()

        }

    }

    fun callAPi() {
        homeActivity?.let {
            ApiCall.getNotification(it, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    Log.d(TAG, "NotificationFragment: ${Gson().toJson(mResponse.body())}")
                    list.clear()
                    mResponse.body()?.notificationData?.let { it1 -> list.addAll(it1) }

                    setAdapter1()
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    fun setAdapter1() {
        rvAdapNotification = RecyclerViewGenricAdapter<NotificationModel1, ItemNotiBinding>(
            list,
            R.layout.item_noti, object :
                RecyclerCallback<ItemNotiBinding, NotificationModel1> {
                override fun bindData(
                    binder: ItemNotiBinding,
                    model: NotificationModel1,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                        tvTitle.text = model.title
                        tvBody.text = model.body
                        tvDateTime.text = changeUTCDateFormat(model.createdAt)

                        chkBob.setOnClickListener {
                            CustomDialog(homeActivity!!).openDelNotiDialog(object :
                                CustomDialog.DialogListener {
                                override fun positiveBtn() {

                                }

                                override fun positiveBtn(paramModel: ParamModel) {

                                }

                            })

                        }
                    }
                }
            })

        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapNotification
        }
    }

}