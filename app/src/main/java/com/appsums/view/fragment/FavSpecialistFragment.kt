package com.appsums.view.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import com.appsums.R
import com.appsums.databinding.FragmentFavSpecBinding
import com.appsums.databinding.ItemFavBinding
import com.appsums.model.FavouriteListModel
import com.appsums.model.GenericModel
import com.appsums.model.ToolBarModel
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.appsums.view.adapters.RecyclerCallback
import com.appsums.view.adapters.RecyclerViewGenricAdapter
import com.appsums.view.base.BaseFragment
import com.google.gson.Gson
import retrofit2.Response

private const val TAG = "FavSpecialistFragment"

class FavSpecialistFragment :
    BaseFragment<FragmentFavSpecBinding>(FragmentFavSpecBinding::inflate) {

    val list = ArrayList<FavouriteListModel>()

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.notiIconShow = true
        toolBarModel.toolBarProfileShow = true
        toolBarModel.toolBarTtl = getString(R.string.Favourite_Specialists)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {
            callAPi()
        }

    }

    fun callAPi() {
        homeActivity?.let {
            ApiCall.getFavouriteList(it, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    Log.d(TAG, "onSuccess: ${Gson().toJson(mResponse.body())}")
                    list.clear()
                    mResponse.body()?.userList?.let { it1 -> list.addAll(it1) }

                    setAdapter1()
                }

                override fun onError(msg: String) {

                }
            })
        }
    }


    fun setAdapter1() {

        val rvAdapFavouriteList = RecyclerViewGenricAdapter<FavouriteListModel, ItemFavBinding>(
            list,
            R.layout.item_fav, object :
                RecyclerCallback<ItemFavBinding, FavouriteListModel> {
                @SuppressLint("SetTextI18n")
                override fun bindData(
                    binder: ItemFavBinding,
                    model: FavouriteListModel,
                    position: Int,
                    itemView: View
                ) {
                    binder.apply {
                        tvName.text = model.fullName
                        setImageWithUrl(model.profilePic, imgProfile)
                        tvServiceCharge.text =
                            "$" + model.serviceCharge.toString() + " Consultation Fee"
                        tvExpYearMonth.text = "" +
                                model.experience_in_years + " year and\n" + model.experience_in_months + " month Of Experience"
                        ratingStars.rating = model.rating.toFloat()
                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapFavouriteList
        }
    }

}