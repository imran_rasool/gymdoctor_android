package com.appsums.view.fragment

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.appsums.R
import com.appsums.databinding.FragmentChatBinding
import com.appsums.databinding.ItemChatMsgBinding
import com.appsums.model.ToolBarModel
import com.appsums.view.adapters.RecyclerCallback
import com.appsums.view.adapters.RecyclerViewGenricAdapter
import com.appsums.view.base.BaseFragment


class ChatFragment : BaseFragment<FragmentChatBinding>(FragmentChatBinding::inflate) {

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.imgAudiohow = true
        toolBarModel.imgVideoShow = true
        toolBarModel.backBtn = true
        toolBarModel.toolBarTtl = getString(R.string.chat)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        homeActivity?.viewDataBinding?.imgVideoCall?.setOnClickListener {
        //    displayItAddStack(VideoCallFragment())
        }

        viewDataBinding?.apply {

            recyclerView.layoutManager = LinearLayoutManager(requireContext())
            setAdapter1()

        }

    }

    fun setAdapter1() {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        val rvAdapProgress = RecyclerViewGenricAdapter<String, ItemChatMsgBinding>(
            list,
            R.layout.item_chat_msg, object :
                RecyclerCallback<ItemChatMsgBinding, String> {
                override fun bindData(
                    binder: ItemChatMsgBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }

}