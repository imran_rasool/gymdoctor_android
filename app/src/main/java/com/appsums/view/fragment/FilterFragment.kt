package com.appsums.view.fragment

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.core.os.bundleOf
import com.appsums.databinding.FragmentFilterBinding
import com.appsums.databinding.SpinnerItemBinding
import com.appsums.model.GenericModel
import com.appsums.model.LatLngSprciltyModel
import com.appsums.model.SubscriptionModel
import com.appsums.model.ToolBarModel
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.appsums.utils.GlobalConstants
import com.appsums.utils.GlobalConstants.requestKey
import com.appsums.utils.setFragmentResult
import com.appsums.utils.widgets.rangeseekbar.RangeSeekBar
import com.appsums.view.adapters.ArraySpinnerAdapter
import com.appsums.view.base.BaseFragment
import com.google.gson.Gson
import retrofit2.Response


class FilterFragment(val _id: String) :
    BaseFragment<FragmentFilterBinding>(FragmentFilterBinding::inflate) {

    private val serviceTypelist = ArrayList<SubscriptionModel>()
    var ratingStart = 0
    var ratingEnd = 0
    var costStart = 0
    var costEnd = 0

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {
            btnCancel.setOnClickListener {
                popBack()
            }
            imgBack.setOnClickListener {
                popBack()
            }

            rangeSeekBar.setOnRangeSeekBarChangeListener(object :
                RangeSeekBar.OnRangeSeekBarChangeListener {
                override fun onProgressChanged(
                    seekBar: RangeSeekBar?,
                    progressStart: Int,
                    progressEnd: Int,
                    fromUser: Boolean
                ) {
                    ratingStart = progressStart
                    ratingEnd = progressEnd

                }

                override fun onStartTrackingTouch(seekBar: RangeSeekBar?) {

                }

                override fun onStopTrackingTouch(seekBar: RangeSeekBar?) {

                }

            })

            rangeSeekBar1.setOnRangeSeekBarChangeListener(object :
                RangeSeekBar.OnRangeSeekBarChangeListener {
                override fun onProgressChanged(
                    seekBar: RangeSeekBar?,
                    progressStart: Int,
                    progressEnd: Int,
                    fromUser: Boolean
                ) {
                    costStart = progressStart
                    costEnd = progressEnd

                }

                override fun onStartTrackingTouch(seekBar: RangeSeekBar?) {

                }

                override fun onStopTrackingTouch(seekBar: RangeSeekBar?) {

                }

            })

            btn1.setOnClickListener {

                val latLngSprciltyModel = LatLngSprciltyModel(
                    specialtyId = serviceTypelist[spinner.selectedItemPosition]._id,
                    startRating = ratingStart,
                    endRating = ratingEnd,
                    costTo = costEnd,
                    costFrom = costStart,
                    distance = rangeSeekBar2.progressEnd,
                    serviceProviderTypeId = _id,
                    lat = lat,
                    long = long
                )
                val gson = Gson()
                val json = gson.toJson(latLngSprciltyModel)

                setFragmentResult(requestKey, bundleOf(GlobalConstants.FILTER_OBJECT to json))
                popBack()
            }
        }

        callAPi(_id)
    }


    fun setSpinner() {


        val arrayAdapter = ArraySpinnerAdapter(
            requireActivity(),
            serviceTypelist,
            object : ArraySpinnerAdapter.CustomSpinnerData<SpinnerItemBinding, SubscriptionModel> {
                override fun setDataSpinner(
                    binder: SpinnerItemBinding?,
                    position: Int,
                    model: SubscriptionModel
                ) {

                    binder?.textSpinner?.text = model.secondry_specialty.toString()

                }

            }
        )

        viewDataBinding?.apply {

            etSecSpeciality.setOnClickListener {
                spinner.performClick()
            }
            spinner.adapter = arrayAdapter
            spinner.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    etSecSpeciality.setText(serviceTypelist[p2].secondry_specialty.toString())
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                }
            }
        }
    }

    //  {"responseMessage":"Data found","documentList":[{"_id":"61af4d3b75b48511111643d6","primary_specialtyId":"61ae08d475b4851111161999","secondry_specialty":"wewwreetrrew","image":"https://healbucket.s3.ap-south-1.amazonaws.com/1638878399533.jpeg","status":"ACTIVE","createdAt":"2021-12-07T12:02:03.345Z","updatedAt":"2021-12-07T12:02:03.345Z","__v":0}]}

    fun callAPi(_id: String) {
        homeActivity?.let {
            ApiCall.secondrySpecialtyList(it, _id, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    val model = mResponse.body()
                    serviceTypelist.clear()
                    serviceTypelist.addAll(model?.documentList!!)
                    setSpinner()
                }

                override fun onError(msg: String) {

                }
            })
        }
    }


}