package com.appsums.view.fragment

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.appsums.R
import com.appsums.databinding.FragmentMsgBinding
import com.appsums.databinding.ItemCallBinding
import com.appsums.databinding.ItemChatBinding
import com.appsums.model.ToolBarModel
import com.appsums.view.adapters.RecyclerCallback
import com.appsums.view.adapters.RecyclerViewGenricAdapter
import com.appsums.view.base.BaseFragment
import com.google.android.material.tabs.TabLayout


class MessageFragment :
    BaseFragment<FragmentMsgBinding>(FragmentMsgBinding::inflate) {

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.notiIconShow = true
        toolBarModel.toolBarProfileShow = true
        toolBarModel.toolBarTtl = getString(R.string.INBOX)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            recyclerView.layoutManager = LinearLayoutManager(requireContext())
            setAdapter()

            if (tabLayout.tabCount <= 0) {
                tabLayout.addTab(tabLayout.newTab().setText(R.string.chat))
                tabLayout.addTab(tabLayout.newTab().setText(R.string.calls))
            }


            tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab?) {
                    selectPosition = tab?.position ?: 0

                    if (selectPosition == 1) {
                        setAdapterCall()
                    } else {
                        setAdapter()
                    }

                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {

                }

                override fun onTabReselected(tab: TabLayout.Tab?) {

                }
            })
        }
    }


    fun setAdapter() {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        val rvAdapProgress = RecyclerViewGenricAdapter<String, ItemChatBinding>(
            list,
            R.layout.item_chat, object :
                RecyclerCallback<ItemChatBinding, String> {
                override fun bindData(
                    binder: ItemChatBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                        lllMain.setOnClickListener {
                            displayItAddStack(ChatFragment())
                        }

                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }


    fun setAdapterCall() {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        val rvAdapProgress = RecyclerViewGenricAdapter<String, ItemCallBinding>(
            list,
            R.layout.item_call, object :
                RecyclerCallback<ItemCallBinding, String> {
                override fun bindData(
                    binder: ItemCallBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }


}