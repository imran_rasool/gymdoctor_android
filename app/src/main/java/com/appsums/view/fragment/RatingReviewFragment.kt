package com.appsums.view.fragment

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.appsums.R
import com.appsums.databinding.FragmentRatingReviewBinding
import com.appsums.databinding.ItemRatingBinding
import com.appsums.model.ToolBarModel
import com.appsums.view.adapters.RecyclerCallback
import com.appsums.view.adapters.RecyclerViewGenricAdapter
import com.appsums.view.base.BaseFragment


class RatingReviewFragment :
    BaseFragment<FragmentRatingReviewBinding>(FragmentRatingReviewBinding::inflate) {

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.notiIconShow = true
        toolBarModel.toolBarProfileShow = true
        toolBarModel.toolBarTtl = getString(R.string.RATINGS_REVIEW)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            recyclerView.layoutManager = LinearLayoutManager(requireContext())
            setAdapter1()

        }

    }

    fun setAdapter1() {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        val rvAdapProgress = RecyclerViewGenricAdapter<String, ItemRatingBinding>(
            list,
            R.layout.item_rating, object :
                RecyclerCallback<ItemRatingBinding, String> {
                override fun bindData(
                    binder: ItemRatingBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                        btnCall.setOnClickListener {
                            displayItAddStack(RatingReviewDetailFragment())
                        }

                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }

}