package com.appsums.view.fragment

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.appsums.R
import com.appsums.databinding.FragmentSelectAppDateBinding
import com.appsums.databinding.ItemSlotsBinding
import com.appsums.model.*
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.appsums.view.adapters.RecyclerCallback
import com.appsums.view.adapters.RecyclerViewGenricAdapter
import com.appsums.view.base.BaseFragment
import retrofit2.Response
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*


class SelectAppointmentFragment1(var model: HomeDataDocsModel, var selectedDate: LocalDate) :
    BaseFragment<FragmentSelectAppDateBinding>(FragmentSelectAppDateBinding::inflate) {

    private var modelSlots: AvailabilityModle? = null
    private val list = ArrayList<SloGetData>()
    private var rvAdapProgress: RecyclerViewGenricAdapter<SloGetData, ItemSlotsBinding>? = null
    private var binding: FragmentSelectAppDateBinding? = null
    var selectposition = -1

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.toolBarTtl = getString(R.string.SELECT_APPOINTMENT_DATE)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = viewDataBinding

        callBookMark()
        viewDataBinding?.apply {
            ll2.visibility = View.VISIBLE
            tvSelectDate1.setText(selectedDate.toString())
            imgFav.setOnClickListener {
                callAPi(
                    FavouriteModal(model._id, !model.isFavourite)
                )
            }
            setImageWithUrl(model.profilePic, img)
            tvName.text = model.fullName
            ratingStars.rating = model.rating.toFloat()
            when {
                model.experience_in_months == null -> {
                    tvExp.text =
                        "" + model.experience_in_years + "yrs " + "of Experience"
                }
                model.experience_in_years == null -> {
                    tvExp.text =
                        "" + model.experience_in_months + "mo " + "of Experience"
                }
                else -> {
                    tvExp.text =
                        "" + model.experience_in_months + "mo " + model.experience_in_years + "yrs " + "of Experience"
                }
            }

            recyclerView.layoutManager = GridLayoutManager(context, 2)
            setAdapter1(recyclerView)
            callAPi(model._id, selectedDate.toString())
            underlineText(txtUploadDoc)

            btnCall.setOnClickListener {

                if (list.isNullOrEmpty()){
                    showToast("No slots available")
                }else if (selectposition<0){
                    showToast("Please select slot")
                }else{
                    val format = "yyyy-MM-dd hh:mm a"
                    val sdf = SimpleDateFormat(format)
                    val dateObj1: Date = sdf.parse("$selectedDate ${ list[selectposition].slotTime}")
                    var dif: Long = dateObj1.getTime()
                    var selectDurPosition = modelSlots?.interval
                    if (selectDurPosition == "15") {
                        dif += 900000
                    } else if (selectDurPosition == "30") {
                        dif += 1800000
                    } else if (selectDurPosition == "45") {
                        dif += 2700000
                    } else if (selectDurPosition == "60") {
                        dif += 3600000
                    }

                    val sdf1 = SimpleDateFormat("hh:mm")

                    displayItAddStack(
                        SelectAppointmentFragment2(
                            model, BookModel(
                                model._id,
                                list[selectposition].slotTime,
                                modelSlots?._id ?: "",
                                list[selectposition]._id,
                                selectedDate.toString(),
                                selectedDate.toString() + "T" + sdf1.format(dateObj1)
                                    .toString() + ":00Z",
                                selectedDate.toString() + "T" + sdf1.format(Date(dif))
                                    .toString() + ":00Z",
                                "",
                                ""
                            )
                        )
                    )
                }

            }
        }
    }

    fun setAdapter1(recyclerView: RecyclerView) {

        rvAdapProgress = RecyclerViewGenricAdapter<SloGetData, ItemSlotsBinding>(
            list,
            R.layout.item_slots, object :
                RecyclerCallback<ItemSlotsBinding, SloGetData> {
                override fun bindData(
                    binder: ItemSlotsBinding,
                    model: SloGetData,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                        tvTime.text = model.slotTime
                        if (selectposition == position) {
//                            llMain.background =
//                                ContextCompat.getDrawable(requireContext(), R.drawable.edit_bg_red)
                            tvTime.setTextColor(setColor(R.color.colorRed))
                        } else {
//                            llMain.background =
//                                ContextCompat.getDrawable(requireContext(), R.drawable.edit_bg_red)
                            tvTime.setTextColor(setColor(R.color.black))
                        }

                        llMain.setOnClickListener {
                            selectposition = position
                            rvAdapProgress?.notifyDataSetChanged()

                        }

                    }
                }
            })
        recyclerView.adapter = rvAdapProgress
        recyclerView.isNestedScrollingEnabled = false
    }


    fun callAPi(_id: String, bookingDate: String) {
        homeActivity?.let {
            ApiCall.getSlot(it, _id, bookingDate, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    modelSlots = mResponse.body()?.slotData
                    list?.clear()
                    modelSlots?.slots?.let { it1 -> list?.addAll(it1) }
                    rvAdapProgress?.notifyDataSetChanged()


                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    fun callBookMark() {
        viewDataBinding?.apply {
            if (model.isFavourite) {
                setImgTint(imgFav, R.color.colorRed)
            } else {
                setImgTint(imgFav, R.color.colorGray4)
            }
        }
    }

    fun callAPi(body: FavouriteModal) {
        homeActivity?.let {
            ApiCall.favAPi(it, body, object :
                ResponseListener<GenericModel1> {
                override fun onSuccess(mResponse: Response<GenericModel1>) {
                    model.isFavourite = !model.isFavourite
                    callBookMark()

                }

                override fun onError(msg: String) {

                }
            })
        }
    }

}