package com.appsums.view.fragment

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.appsums.PatientDetailFragment
import com.appsums.R
import com.appsums.databinding.FragmentDetailBinding
import com.appsums.databinding.ItemOfferBinding
import com.appsums.model.GenericModel
import com.appsums.model.HomeDataDocsModel
import com.appsums.model.LatLngModel
import com.appsums.model.ToolBarModel
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.appsums.view.adapters.RecyclerCallback
import com.appsums.view.adapters.RecyclerViewGenricAdapter
import com.appsums.view.base.BaseFragment
import com.google.gson.Gson
import retrofit2.Response

private const val TAG = "DeatilFragment"

class DeatilFragment : BaseFragment<FragmentDetailBinding>(FragmentDetailBinding::inflate) {

    private var rvAdapProgress: RecyclerViewGenricAdapter<HomeDataDocsModel, ItemOfferBinding>? =
        null
    val list = ArrayList<HomeDataDocsModel>()

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.toolBarTtl = getString(R.string.LABORATERIES)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewDataBinding?.apply {
            //recyclerView.layoutManager = LinearLayoutManager(requireContext())
            setAdapter1(recyclerView)
            recyclerView.isNestedScrollingEnabled = false
        }
        val lat2 = 28.7041
        val long2 = 77.1025
        callAPi(LatLngModel(lat2, long2))
    }

    fun setAdapter1(recyclerView: RecyclerView) {
        rvAdapProgress = RecyclerViewGenricAdapter<HomeDataDocsModel, ItemOfferBinding>(
            list,
            R.layout.item_offer, object :
                RecyclerCallback<ItemOfferBinding, HomeDataDocsModel> {
                override fun bindData(
                    binder: ItemOfferBinding,
                    model: HomeDataDocsModel,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                        setImageWithUrl(model.image, img)
                        tvName.text = model.name

                        btnCall.setOnClickListener {
                            displayItNoStack(PatientDetailFragment(0))
                        }

                    }
                }
            })
        recyclerView.adapter = rvAdapProgress

    }

    fun callAPi(body: LatLngModel) {
        homeActivity?.let {
            ApiCall.nearByLaboratoryAPi(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    val model = mResponse.body()
                    Log.d(TAG, "onSuccess: ${Gson().toJson(model)}")
                    list.clear()
                    list.addAll(model?.labData!!)
                    rvAdapProgress?.notifyDataSetChanged()
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

}