//package com.appsums.view.fragment
//
//import android.Manifest
//import android.app.Dialog
//import android.content.DialogInterface
//import android.content.Intent
//import android.content.pm.PackageManager
//import android.content.res.AssetFileDescriptor
//import android.media.AudioManager
//import android.media.MediaPlayer
//import android.net.Uri
//import android.os.Bundle
//import android.util.Log
//import android.view.*
//import android.widget.EditText
//import android.widget.Toast
//import androidx.annotation.NonNull
//import androidx.appcompat.app.AlertDialog
//import androidx.core.app.ActivityCompat
//import androidx.core.content.ContextCompat
//import com.appsums.R
//import com.appsums.databinding.FragmentVideoCallBinding
//import com.appsums.model.ToolBarModel
//import com.appsums.utils.GlobalConstants
//import com.appsums.utils.SharedPrefClass
//import com.appsums.view.activities.SettingsActivity
//import com.appsums.view.base.BaseFragment
//import com.appsums.view.fragment.twilio.CameraCapturerCompat
//import com.twilio.audioswitch.AudioSwitch
//import com.twilio.video.*
//import tvi.webrtc.VideoSink
//import java.util.*
//import kotlin.collections.ArrayList
//
//
//class VideoCallFragment :
//    BaseFragment<FragmentVideoCallBinding>(FragmentVideoCallBinding::inflate) {
//
//    private var preferences: SharedPrefClass? = null
//    private val CAMERA_MIC_PERMISSION_REQUEST_CODE = 1
//    private val LOCAL_AUDIO_TRACK_NAME = "mic"
//    private val LOCAL_VIDEO_TRACK_NAME = "camera"
//    private var cameraCapturerCompat: CameraCapturerCompat? = null
//    private var localAudioTrack: LocalAudioTrack? = null
//    private var localVideoTrack: LocalVideoTrack? = null
//    private var accessToken: String? = null
//    private var audioCodec: AudioCodec? = null
//    private var videoCodec: VideoCodec? = null
//    private var roomName: String? = null
//    private var room: Room? = null
//    var player: MediaPlayer? = null
//    private var disconnectedFromOnDestroy = false
//    private var firstName: String? = null
//    private var lastName: String? = null
//    private var callType: String? = null
//    private var callerName: String? = null
//    private var pushType: String? = null
//    private var newEncodingParameters: EncodingParameters? = null
//    private var enableAutomaticSubscription = false
//    var TAG = "VideoCallFragment"
//    private var from: String? =
//        null
//    private  var profilePic:String? = null
//    private  var salutation:String? = null
//    private  var action:String? = null
//    private  var roomId:String? = null
//    private  var senderId:String? = null
//    private  var receiverId:String? = null
//    private var localParticipant: LocalParticipant? = null
//    /*
//     * Audio management
//     */
//    private var audioSwitch: AudioSwitch? = null
//    private var savedVolumeControlStream = 0
//    private var audioDeviceMenuItem: MenuItem? = null
//    private var encodingParameters: EncodingParameters? = null
//    private val localVideoView: VideoSink? = null
//    private var connectDialog: AlertDialog? = null
//
//    /*
//     * You must provide a Twilio Access Token to connect to the Video service
//     */
//    //    private static final String TWILIO_ACCESS_TOKEN = Constant.TWILIO_ACCESS_TOKEN;
//    //    private static final String ACCESS_TOKEN_SERVER = Constant.TWILIO_ACCESS_TOKEN_SERVER;
//    private var TWILIO_ACCESS_TOKEN: String? = null
//
//    override fun getToolBar(): ToolBarModel {
//        val toolBarModel = ToolBarModel()
//        toolBarModel.toolBarShow = false
//        return toolBarModel
//    }
//
//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//
//        player = MediaPlayer()
//
//        preferences = SharedPrefClass()
//
//        audioSwitch = AudioSwitch(homeActivity!!)
//        savedVolumeControlStream = homeActivity!!.getVolumeControlStream()
//        homeActivity!!.volumeControlStream = AudioManager.STREAM_VOICE_CALL
//        newEncodingParameters = encodingParameters()
//        encodingParameters = newEncodingParameters
//        intentData()
//        setAccessToken() //for additional by sohail
//        /*
//             * Check camera and microphone permissions. Needed in Android M.
//             */if (!checkPermissionForCameraAndMicrophone()) {
//            requestPermissionForCameraAndMicrophone()
//        } else {
//            createAudioAndVideoTracks()
//            setAccessToken()
//        }
//
//        /*
//             * Set the initial state of the UI
//             */
//        intializeUI()
//    }
//
//    //for incoming handle
//    private fun intentData() {
//        val intent: Bundle? = requireArguments()
//        if (intent != null) {
//            if (intent.getString(GlobalConstants.FROM) != null) {
//                from = intent.getString(GlobalConstants.FROM)
//            }
//            if (intent.getString(GlobalConstants.TWILLIO_TOKEN) != null) {
//                TWILIO_ACCESS_TOKEN = intent.getString(GlobalConstants.TWILLIO_TOKEN)
//            }
//            if (intent.getString(GlobalConstants.ROOM_ID) != null) {
//                roomName = intent.getString(GlobalConstants.ROOM_ID)
//            }
//            if (intent.getString(GlobalConstants.FIRST_NAME) != null) {
//                firstName = intent.getString(GlobalConstants.FIRST_NAME)
//            }
//            viewDataBinding?.apply {
//                if (intent.getString(GlobalConstants.LAST_NAME) != null) {
//                    lastName = intent.getString(GlobalConstants.LAST_NAME)
//                    llIncomingCall.tvCallerName.setText(firstName.toString() + " " + lastName)
//                    tvCallerName.setText(firstName.toString() + " " + lastName)
//                } else {
//                    llIncomingCall.tvCallerName.setText(firstName)
//                }
//                if (intent.getString(GlobalConstants.NAME) != null) {
//                    callerName = intent.getString(GlobalConstants.NAME)
//                }
//                if (intent.getString(GlobalConstants.PUSH_TYPE) != null) {
//                    pushType = intent.getString(GlobalConstants.PUSH_TYPE)
//                    if (pushType.equals(GlobalConstants.AUDIO)) {
//                        binding.llParent.setBackgroundResource(R.mipmap.splash)
//                        if (intent.getString(GlobalConstants.CALLING) != null) {
//                            callType = intent.getString(GlobalConstants.CALLING)
//                            if (callType.equals(GlobalConstants.DIAL_CALL)) {
//                                binding.ivLogo.setVisibility(View.VISIBLE)
//                                binding.llCallControls.setVisibility(View.VISIBLE)
//                                binding.llDialer.tvCallType.setText("Dial")
//                                binding.llIncomingCall.llIncomingCallParent.setVisibility(View.GONE)
//                                ringBell()
//                            }
//                            if (callType.equals(GlobalConstants.INCOMING_CALL)) {
//                                binding.ivLogo.setVisibility(View.GONE)
//                                binding.llDialer.tvCallType.setText("Incoming")
//                                binding.llIncomingCall.llIncomingCallParent.setVisibility(View.VISIBLE)
//                                binding.llCallControls.setVisibility(View.GONE)
//                                PlayRing()
//                                autoDeclined()
//                                binding.llIncomingCall.layoutRipplepulse.startRippleAnimation()
//                            }
//                        }
//                    }
//                }
//                //for incoming handle
//                if (intent.getString(GlobalConstants.PROFILE_PIC) != null) {
//                    profilePic = intent.getString(GlobalConstants.PROFILE_PIC)
//                }
//                if (intent.getString(GlobalConstants.SENDER_ID) != null) {
//                    senderId = intent.getString(GlobalConstants.SENDER_ID)
//                }
//                if (intent.getString(GlobalConstants.RECEIVER_ID) != null) {
//                    receiverId = intent.getString(GlobalConstants.RECEIVER_ID)
//                }
//            }
//
//        }
//    }
//
//    /*
//         * The initial state when there is no active room.
//         */
//    private fun intializeUI() {
//        //for incoming
//        requireActivity()!!.getWindow().setFlags(
//            WindowManager.LayoutParams.FLAG_FULLSCREEN or
//                    WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or
//                    WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON,
//            WindowManager.LayoutParams.FLAG_FULLSCREEN or
//                    WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or
//                    WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
//        )
//        viewDataBinding?.apply {
//            llIncomingCall.txtAnswer.setOnClickListener(object : View.OnClickListener() {
//                fun onClick(view: View?) {
//                    llIncomingCall.layoutRipplepulse.stopRippleAnimation()
//                    llIncomingCall.llIncomingCallParent.setVisibility(View.GONE)
//                    ivLogo.setVisibility(View.VISIBLE)
//                    llCallControls.setVisibility(View.VISIBLE)
//                    llDialer.tvCallType.setText("Incoming")
//                    connectToRoom(roomName)
//                }
//            })
//            llIncomingCall.txtDecline.setOnClickListener(object : View.OnClickListener() {
//                fun onClick(view: View?) {
//                    room.disconnect()
//                    if (player != null && player.isPlaying()) {
//                        player.stop()
//                    }
//                    binding.llIncomingCall.layoutRipplepulse.stopRippleAnimation()
//                    /*Intent intent=new Intent(VideoAudioActivity.this, MainActivity.class);
//                    intent.putExtra(GlobalConstants.FROM,from);
//                    startActivity(intent);
//                    finishAffinity();*/
//                }
//            })
//
//
//            //for dial
//            tvConeccting.setVisibility(View.VISIBLE)
//            disConnectActionFab.setOnClickListener(disconnectClickListener())
//            muteActionFab.show()
//            muteActionFab.setOnClickListener(muteClickListener())
//
////        binding.speakerActionFab.setOnClickListener();
//            if (callType != null && callType.equals(GlobalConstants.DIAL_CALL)) {
//                tvConeccting.setVisibility(View.VISIBLE)
//                tvConeccting.setText("Ringing...")
//                connectDialRoom()
//            }
//        }
//
//    }
//
//   override fun onCreateOptionsMenu(menu: Menu): Boolean {
//        val inflater: MenuInflater = getMenuInflater()
//        inflater.inflate(R.menu.menu_video_activity, menu)
//        audioDeviceMenuItem = menu.findItem(R.id.menu_audio_device)
//
//        /*
//             * Start the audio device selector after the menu is created and update the icon when the
//             * selected audio device changes.
//             */audioSwitch.start { audioDevices, audioDevice ->
//            updateAudioDeviceIcon(audioDevice)
//            Unit.INSTANCE
//        }
//        return true
//    }
//
//
//    override fun onStop() {
//        super.onStop()
//        if (player != null && player?.isPlaying() == true) {
//            player?.stop()
//        }
//        if (callType != null && callType.equals(GlobalConstants.DIAL_CALL)) {
//            if (player != null && player?.isPlaying() == true) {
//                player?.stop()
//            }
//            viewDataBinding?.llIncomingCall.layoutRipplepulse.stopRippleAnimation()
//        }
//    }
//
//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        return when (item.getItemId()) {
//            R.id.menu_settings -> {
//                startActivity(Intent(this, SettingsActivity::class.java))
//                true
//            }
//            R.id.menu_audio_device -> {
//                showAudioDevices()
//                true
//            }
//            else -> false
//        }
//    }
//
//  override  fun onRequestPermissionsResult(
//        requestCode: Int,
//        @NonNull permissions: Array<String?>?,
//        @NonNull grantResults: IntArray
//    ) {
//        if (requestCode == CAMERA_MIC_PERMISSION_REQUEST_CODE) {
//            var cameraAndMicPermissionGranted = true
//            for (grantResult in grantResults) {
//                cameraAndMicPermissionGranted =
//                    cameraAndMicPermissionGranted and (grantResult == PackageManager.PERMISSION_GRANTED)
//            }
//            if (cameraAndMicPermissionGranted) {
//                createAudioAndVideoTracks()
//                setAccessToken()
//            } else {
//                Toast.makeText(
//                    requireActivity(),
//                    R.string.permissions_needed,
//                    Toast.LENGTH_LONG
//                ).show()
//            }
//        }
//    }
//
//    override fun onResume() {
//        super.onResume()
//        /*
//             * Update preferred audio and video codec in case changed in settings
//             */audioCodec = getAudioCodecPreference(
//            SettingsActivity.PREF_AUDIO_CODEC,
//            SettingsActivity.PREF_AUDIO_CODEC_DEFAULT
//        )
//        videoCodec = getVideoCodecPreference(
//            SettingsActivity.PREF_VIDEO_CODEC,
//            SettingsActivity.PREF_VIDEO_CODEC_DEFAULT
//        )
//        enableAutomaticSubscription = getAutomaticSubscriptionPreference(
//            SettingsActivity.PREF_ENABLE_AUTOMATIC_SUBSCRIPTION,
//            SettingsActivity.PREF_ENABLE_AUTOMATIC_SUBSCRIPTION_DEFAULT
//        )
//        /*
//             * Get latest encoding parameters
//             */newEncodingParameters = encodingParameters
//
//        /*
//             * If the local video track was released when the app was put in the background, recreate.
//             */if (localVideoTrack == null && checkPermissionForCameraAndMicrophone()) {
//            localVideoTrack = LocalVideoTrack.create(
//                requireActivity(),
//                true,
//                cameraCapturerCompat,
//                LOCAL_VIDEO_TRACK_NAME
//            )
//            localVideoTrack?.addSink(localVideoView)
//
//            /*
//                 * If connected to a Room then share the local video track.
//                 */if (localParticipant != null) {
//                localParticipant?.publishTrack(localVideoTrack)
//
//                /*
//                     * Update encoding parameters if they have changed.
//                     */if (!newEncodingParameters.equals(encodingParameters)) {
//                    localParticipant?.setEncodingParameters(newEncodingParameters)
//                }
//            }
//        }
//
//        /*
//             * Update encoding parameters
//             */encodingParameters = newEncodingParameters
//
//        /*
//             * Update reconnecting UI
//             */if (room != null) {
//            reconnectingProgressBar.setVisibility(if (room?.getState() !== Room.State.RECONNECTING) View.GONE else View.VISIBLE)
//        }
//    }
//
//    override fun onPause() {
//        if (player != null && player?.isPlaying()==true) {
//            player?.stop()
//        }
//
//        /*
//             * Release the local video track before going in the background. requireActivity() ensures that the
//             * camera can be used by other applications while requireActivity() app is in the background.
//             */if (localVideoTrack != null) {
//            /*
//                 * If requireActivity() local video track is being shared in a Room, unpublish from room before
//                 * releasing the video track. Participants will be notified that the track has been
//                 * unpublished.
//                 */
//            if (localParticipant != null) {
//                localParticipant?.unpublishTrack(localVideoTrack!!)
//            }
//            localVideoTrack?.release()
//            localVideoTrack = null
//        }
//        super.onPause()
//    }
//
//    override fun onDestroy() {
//        if (player != null && player?.isPlaying()==true) {
//            player?.stop()
//        }
//
//        /*
//             * Tear down audio management and restore previous volume stream
//             */audioSwitch?.stop()
//        requireActivity()?.setVolumeControlStream(savedVolumeControlStream)
//
//        /*
//             * Always disconnect from the room before leaving the Activity to
//             * ensure any memory allocated to the Room resource is freed.
//             */if (room != null && room?.getState() !== Room.State.DISCONNECTED) {
//            room?.disconnect()
//            disconnectedFromOnDestroy = true
//        }
//
//        /*
//             * Release the local audio and video tracks ensuring any memory allocated to audio
//             * or video is freed.
//             */if (localAudioTrack != null) {
//            localAudioTrack?.release()
//            localAudioTrack = null
//        }
//        if (localVideoTrack != null) {
//            localVideoTrack?.release()
//            localVideoTrack = null
//        }
//        super.onDestroy()
//        popBack()
//        //finishAffinity()
//    }
//
//    private fun checkPermissionForCameraAndMicrophone(): Boolean {
//        val resultCamera: Int =
//            ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.CAMERA)
//        val resultMic: Int =
//            ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.RECORD_AUDIO)
//        return resultCamera == PackageManager.PERMISSION_GRANTED &&
//                resultMic == PackageManager.PERMISSION_GRANTED
//    }
//
//    private fun requestPermissionForCameraAndMicrophone() {
//        if (ActivityCompat.shouldShowRequestPermissionRationale(
//                requireActivity(),
//                Manifest.permission.CAMERA
//            ) ||
//            ActivityCompat.shouldShowRequestPermissionRationale(
//                requireActivity(),
//                Manifest.permission.RECORD_AUDIO
//            )
//        ) {
//            Toast.makeText(
//                requireActivity(),
//                R.string.permissions_needed,
//                Toast.LENGTH_LONG
//            ).show()
//        } else {
//            ActivityCompat.requestPermissions(
//                requireActivity(),
//                arrayOf<String>(Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO),
//                CAMERA_MIC_PERMISSION_REQUEST_CODE
//            )
//        }
//    }
//
//    private fun createAudioAndVideoTracks() {
//        // Share your microphone
//        localAudioTrack = LocalAudioTrack.create(requireActivity(), true, LOCAL_AUDIO_TRACK_NAME)
//
//        // Share your camera
//        cameraCapturerCompat =
//            CameraCapturerCompat(requireActivity(), CameraCapturerCompat.Source.FRONT_CAMERA)
//        localVideoTrack = LocalVideoTrack.create(
//            requireActivity(),
//            true,
//            cameraCapturerCompat,
//            LOCAL_VIDEO_TRACK_NAME
//        )
//    }
//
//    private fun setAccessToken() {
//        if (!Constant.USE_TOKEN_SERVER) {
//            /*
//                 * OPTION 1 - Generate an access token from the getting started portal
//                 * https://www.twilio.com/console/video/dev-tools/testing-tools and add
//                 * the variable TWILIO_ACCESS_TOKEN setting it equal to the access token
//                 * string in your local.properties file.
//                 */
//            this.accessToken = TWILIO_ACCESS_TOKEN
//        } else {
//            /*
//                 * OPTION 2 - Retrieve an access token from your own web app.
//                 * Add the variable ACCESS_TOKEN_SERVER assigning it to the url of your
//                 * token server and the variable USE_TOKEN_SERVER=true to your
//                 * local.properties file.
//                 */
//            //if (!callType.equals(GlobalConstants.INCOMING_CALL))
//            //retrieveAccessTokenfromServer();
//            //else
//            //VideoActivity.requireActivity().accessToken = TWILIO_ACCESS_TOKEN;
//            this.accessToken = TWILIO_ACCESS_TOKEN
//        }
//    }
//
//    private fun connectToRoom(roomName: String) {
//        //            ConnectOptions.Builder connectOptionsBuilder = new ConnectOptions.Builder(accessToken).roomName(roomName);
//        val connectOptionsBuilder: ConnectOptions.Builder = ConnectOptions.Builder(accessToken!!).roomName(roomName)
//
//        /*
//             * Add local audio track to connect options to share with participants.
//             */if (localAudioTrack != null) {
//            connectOptionsBuilder.audioTracks(Collections.singletonList(localAudioTrack))
//        }
//
//        /*
//             * Add local video track to connect options to share with participants.
//             */if (localVideoTrack != null) {
//            connectOptionsBuilder.videoTracks(Collections.singletonList(localVideoTrack))
//        }
//
//        /*
//             * Set the preferred audio and video codec for media.
//             */connectOptionsBuilder.preferAudioCodecs(Collections.singletonList(audioCodec))
//        connectOptionsBuilder.preferVideoCodecs(Collections.singletonList(videoCodec))
//
//        /*
//             * Set the sender side encoding parameters.
//             */connectOptionsBuilder.encodingParameters(encodingParameters!!)
//
//        /*
//             * Toggles automatic track subscription. If set to false, the LocalParticipant will receive
//             * notifications of track publish events, but will not automatically subscribe to them. If
//             * set to true, the LocalParticipant will automatically subscribe to tracks as they are
//             * published. If unset, the default is true. Note: requireActivity() feature is only available for Group
//             * Rooms. Toggling the flag in a P2P room does not modify subscription behavior.
//             */connectOptionsBuilder.enableAutomaticSubscription(enableAutomaticSubscription)
//        room = Video.connect(requireActivity(), connectOptionsBuilder.build(), roomListener())
//        audioSwitch?.activate()
//        setDisconnectAction()
//        //simpleChronometer.start();
//    }
//
//    /*
//         * Show the current available audio devices.
//         */
//    private fun showAudioDevices() {
//        val selectedDevice: AudioDevice = audioSwitch.selectedAudioDevice
//        val availableAudioDevices: List<AudioDevice> = audioSwitch.availableAudioDevices()
//        if (selectedDevice != null) {
//            val selectedDeviceIndex = availableAudioDevices.indexOf(selectedDevice)
//            val audioDeviceNames: ArrayList<String> = ArrayList()
//            for (a in availableAudioDevices) {
//                audioDeviceNames.add(a.getName())
//            }
//            AlertDialog.Builder(requireActivity())
//                .setTitle(R.string.room_screen_select_device)
//                .setSingleChoiceItems(
//                    audioDeviceNames.toArray(arrayOfNulls<CharSequence>(0)),
//                    selectedDeviceIndex
//                ) { dialog, index ->
//                    dialog.dismiss()
//                    val selectedAudioDevice: AudioDevice = availableAudioDevices[index]
//                    updateAudioDeviceIcon(selectedAudioDevice)
//                    audioSwitch?.selectDevice(selectedAudioDevice)
//                }.create().show()
//        }
//    }
//
//    /*
//         * Update the menu icon based on the currently selected audio device.
//         */
//    private fun updateAudioDeviceIcon(selectedAudioDevice: AudioDevice) {
//        var audioDeviceMenuIcon: Int = R.drawable.ic_phonelink_ring_white_24dp
//        if (selectedAudioDevice is AudioDevice.BluetoothHeadset) {
//            audioDeviceMenuIcon = R.drawable.ic_bluetooth_white_24dp
//        } else if (selectedAudioDevice is WiredHeadset) {
//            audioDeviceMenuIcon = R.drawable.ic_headset_mic_white_24dp
//        } else if (selectedAudioDevice is Earpiece) {
//            audioDeviceMenuIcon = R.drawable.ic_phonelink_ring_white_24dp
//        } else if (selectedAudioDevice is Speakerphone) {
//            audioDeviceMenuIcon = R.drawable.ic_volume_up_white_24dp
//        }
//        audioDeviceMenuItem.setIcon(audioDeviceMenuIcon)
//    }
//
//    /*
//         * Get the preferred audio codec from shared preferences
//         */
//    private fun getAudioCodecPreference(key: String, defaultValue: String): AudioCodec {
//        val audioCodecName: String = preferences?.getString(requireContext(),key, defaultValue)?:""
//        return when (audioCodecName) {
//            IsacCodec.NAME -> IsacCodec()
//            OpusCodec.NAME -> OpusCodec()
//            PcmaCodec.NAME -> PcmaCodec()
//            PcmuCodec.NAME -> PcmuCodec()
//            G722Codec.NAME -> G722Codec()
//            else -> OpusCodec()
//        }
//    }
//
//    /*
//         * Get the preferred video codec from shared preferences
//         */
//    private fun getVideoCodecPreference(key: String, defaultValue: String): VideoCodec {
//        val videoCodecName: String = preferences.getString(requireContext(),key, defaultValue)
//        return when (videoCodecName) {
//            Vp8Codec.NAME -> {
//                val simulcast: Boolean = preferences?.getBoolean(requireActivity(),
//                    SettingsActivity.PREF_VP8_SIMULCAST,
//                    SettingsActivity.PREF_VP8_SIMULCAST_DEFAULT
//                ) == true
//                Vp8Codec(simulcast)
//            }
//            H264Codec.NAME -> H264Codec()
//            Vp9Codec.NAME -> Vp9Codec()
//            else -> Vp8Codec()
//        }
//    }
//
//    private fun getAutomaticSubscriptionPreference(key: String, defaultValue: Boolean): Boolean {
//        return preferences?.getBoolean(requireActivity(),key, defaultValue) == true
//    }
//
//    private fun encodingParameters(): EncodingParameters {
//        val maxAudioBitrate: Int =
//            (preferences?.getString(requireContext(),
//                SettingsActivity.PREF_SENDER_MAX_AUDIO_BITRATE,
//                SettingsActivity.PREF_SENDER_MAX_AUDIO_BITRATE_DEFAULT
//            )?:"0") as Int
//        val maxVideoBitrate: Int =
//            (preferences?.getString(requireContext(),
//                SettingsActivity.PREF_SENDER_MAX_VIDEO_BITRATE,
//                SettingsActivity.PREF_SENDER_MAX_VIDEO_BITRATE_DEFAULT
//            )?:"0") as Int
//        return EncodingParameters(maxAudioBitrate, maxVideoBitrate)
//    }
//
//    private fun connectClickListener(roomEditText: EditText): DialogInterface.OnClickListener {
//        return DialogInterface.OnClickListener { dialog, which ->
//            /*
//                 * Connect to room
//                 */connectToRoom(roomEditText.getText().toString())
//        }
//    }
//
//    private fun disconnectClickListener(): View.OnClickListener {
//        return View.OnClickListener { v ->
//            /*
//                 * Disconnect from room
//                 */if (room != null && room?.getState() !== Room.State.DISCONNECTED) {
//            room?.disconnect()
//            disconnectedFromOnDestroy = true
//            /*  val intent = Intent(this@VideoAudioActivity, MainActivity::class.java)
//              startActivity(intent)
//              intent.putExtra(GlobalConstants.FROM, from)
//              finishAffinity()*/
//            popBack()
//        }
//            if (room != null && room?.getState() !== Room.State.CONNECTED) {
//                room?.disconnect()
//                /* val intent = Intent(this@VideoAudioActivity, MainActivity::class.java)
//                 startActivity(intent)
//                 finishAffinity()*/
//                popBack()
//            }
//        }
//    }
//
//    private fun connectActionClickListener(): View.OnClickListener {
//        return View.OnClickListener { v -> showConnectDialog() }
//    }
//
//    private fun cancelConnectDialogClickListener(): DialogInterface.OnClickListener {
//        return DialogInterface.OnClickListener { dialog, which ->
//            intializeUI()
//            connectDialog.dismiss()
//        }
//    }
//
//    private fun muteClickListener(): View.OnClickListener {
//        return View.OnClickListener { v ->
//            /*
//                 * Enable/disable the local audio track. The results of this operation are
//                 * signaled to other Participants in the same Room. When an audio track is
//                 * disabled, the audio is muted.
//                 */if (localAudioTrack != null) {
//            val enable: Boolean = !localAudioTrack?.isEnabled()
//            localAudioTrack?.enable(enable)
//            val icon: Int =
//                if (enable) R.drawable.ic_mic_white_24dp else R.drawable.ic_mic_off_black_24dp
//            muteActionFab.setImageDrawable(
//                ContextCompat.getDrawable(
//                    this@VideoAudioActivity, icon
//                )
//            )
//        }
//        }
//    }
//
//    /*
//         * The actions performed during disconnect.
//         */
//    private fun setDisconnectAction() {
//        disConnectActionFab.setImageDrawable(
//            ContextCompat.getDrawable(
//                requireActivity(),
//                R.drawable.ic_call_end_white_24px
//            )
//        )
//        disConnectActionFab.show()
//        disConnectActionFab.setOnClickListener(disconnectClickListener())
//    }
//
//    /*
//         * Creates an connect UI dialog
//         */
//    private fun showConnectDialog() {
//        val roomEditText = EditText(requireActivity())
//        connectDialog = Dialog.createConnectDialog(
//            roomEditText,
//            connectClickListener(roomEditText),
//            cancelConnectDialogClickListener(),
//            requireActivity()
//        )
//        connectDialog.show()
//    }
//
//    /*
//         * Called when remote participant joins the room
//         */
//    private fun addRemoteParticipant(remoteParticipant: RemoteParticipant) {
//        /*
//             * This app only displays video for one additional participant per Room
//             */
//        remoteParticipantIdentity = remoteParticipant.getIdentity()
//
//        /*
//             * Add remote participant renderer
//             */if (remoteParticipant.getRemoteVideoTracks().size() > 0) {
//            val remoteVideoTrackPublication: RemoteVideoTrackPublication =
//                remoteParticipant.getRemoteVideoTracks().get(0)
//
//            /*
//                 * Only render video tracks that are subscribed to
//                 */if (remoteVideoTrackPublication.isTrackSubscribed()) {
//                addRemoteParticipantVideo(remoteVideoTrackPublication.getRemoteVideoTrack())
//            }
//        }
//
//        /*
//             * Start listening for participant events
//             */remoteParticipant.setListener(remoteParticipantListener())
//    }
//
//    /*
//         * Set primary view as renderer for participant video track
//         */
//    //    private void addRemoteParticipantVideo(VideoTrack videoTrack) {
//    //        moveLocalVideoToThumbnailView();
//    //        primaryVideoView.setMirror(false);
//    //        videoTrack.addSink(primaryVideoView);
//    //    }
//    private fun addRemoteParticipantVideo(videoTrack: VideoTrack) {
//        isConnected = true
//        binding.tvConeccting.setVisibility(View.GONE)
//        simpleChronometer.setVisibility(View.VISIBLE)
//        simpleChronometer.start()
//        moveLocalVideoToThumbnailView()
//    }
//
//    private fun moveLocalVideoToThumbnailView() {}
//
//    /*
//         * Called when remote participant leaves the room
//         */
//    private fun removeRemoteParticipant(remoteParticipant: RemoteParticipant) {
//        if (!remoteParticipant.getIdentity().equals(remoteParticipantIdentity)) {
//            return
//        }
//
//        /*
//             * Remove remote participant renderer
//             */if (!remoteParticipant.getRemoteVideoTracks().isEmpty()) {
//            val remoteVideoTrackPublication: RemoteVideoTrackPublication =
//                remoteParticipant.getRemoteVideoTracks().get(0)
//
//            /*
//                 * Remove video only if subscribed to participant track
//                 */simpleChronometer.stop()
//            if (remoteVideoTrackPublication.isTrackSubscribed()) {
////                removeParticipantVideo(remoteVideoTrackPublication.getRemoteVideoTrack());
//            }
//        }
//        //if room disconnect
//        /*  val intent = Intent(this@VideoAudioActivity, MainActivity::class.java)
//          intent.putExtra(GlobalConstants.FROM, from)
//          startActivity(intent)
//          finishAffinity()*/
//        popBack()
//        //        moveLocalVideoToPrimaryView();
//    }
//
//    /*
//         * Room events listener
//         */
//    private fun roomListener(): Room.Listener {
//        return object : Listener() {
//            fun onConnected(room: Room) {
//                if (player != null && player.isPlaying()) {
//                    player.stop()
//                }
//                localParticipant = room.getLocalParticipant()
//                setTitle(room.getName())
//                for (remoteParticipant in room.getRemoteParticipants()) {
//                    addRemoteParticipant(remoteParticipant)
//                    break
//                }
//            }
//
//            fun onReconnecting(@NonNull room: Room?, @NonNull twilioException: TwilioException?) {
//                reconnectingProgressBar.setVisibility(View.VISIBLE)
//            }
//
//            fun onReconnected(@NonNull room: Room?) {
//                reconnectingProgressBar.setVisibility(View.GONE)
//            }
//
//            fun onConnectFailure(room: Room?, e: TwilioException?) {
//                audioSwitch.deactivate()
//                //configureAudio(false);
//                intializeUI()
//            }
//
//            fun onDisconnected(room: Room?, e: TwilioException?) {
//                localParticipant = null
//                //reconnectingProgressBar.setVisibility(View.GONE);
//                this@VideoAudioActivity.room = null
//                // Only reinitialize the UI if disconnect was not called from onDestroy()
//                if (!disconnectedFromOnDestroy) {
//                    audioSwitch?.deactivate()
//                    intializeUI()
//                    //moveLocalVideoToPrimaryView();
//                }
//            }
//
//            fun onParticipantConnected(room: Room?, remoteParticipant: RemoteParticipant) {
//                addRemoteParticipant(remoteParticipant)
//            }
//
//            fun onParticipantDisconnected(room: Room?, remoteParticipant: RemoteParticipant) {
//                removeRemoteParticipant(remoteParticipant)
//            }
//
//            fun onRecordingStarted(room: Room?) {
//                /*
//                     * Indicates when media shared to a Room is being recorded. Note that
//                     * recording is only available in our Group Rooms developer preview.
//                     */
//                Log.d(TAG, "onRecordingStarted")
//            }
//
//            fun onRecordingStopped(room: Room?) {
//                /*
//                     * Indicates when media shared to a Room is no longer being recorded. Note that
//                     * recording is only available in our Group Rooms developer preview.
//                     */
//                Log.d(TAG, "onRecordingStopped")
//            }
//        }
//    }
//
//    private fun remoteParticipantListener(): RemoteParticipant.Listener {
//        return object : Listener() {
//            fun onAudioTrackPublished(
//                remoteParticipant: RemoteParticipant,
//                remoteAudioTrackPublication: RemoteAudioTrackPublication
//            ) {
//                Log.i(
//                    TAG, java.lang.String.format(
//                        "onAudioTrackPublished: " +
//                                "[RemoteParticipant: identity=%s], " +
//                                "[RemoteAudioTrackPublication: sid=%s, enabled=%b, " +
//                                "subscribed=%b, name=%s]",
//                        remoteParticipant.getIdentity(),
//                        remoteAudioTrackPublication.getTrackSid(),
//                        remoteAudioTrackPublication.isTrackEnabled(),
//                        remoteAudioTrackPublication.isTrackSubscribed(),
//                        remoteAudioTrackPublication.getTrackName()
//                    )
//                )
//            }
//
//            fun onAudioTrackUnpublished(
//                remoteParticipant: RemoteParticipant,
//                remoteAudioTrackPublication: RemoteAudioTrackPublication
//            ) {
//                Log.i(
//                    TAG, java.lang.String.format(
//                        "onAudioTrackUnpublished: " +
//                                "[RemoteParticipant: identity=%s], " +
//                                "[RemoteAudioTrackPublication: sid=%s, enabled=%b, " +
//                                "subscribed=%b, name=%s]",
//                        remoteParticipant.getIdentity(),
//                        remoteAudioTrackPublication.getTrackSid(),
//                        remoteAudioTrackPublication.isTrackEnabled(),
//                        remoteAudioTrackPublication.isTrackSubscribed(),
//                        remoteAudioTrackPublication.getTrackName()
//                    )
//                )
//            }
//
//            fun onDataTrackPublished(
//                remoteParticipant: RemoteParticipant,
//                remoteDataTrackPublication: RemoteDataTrackPublication
//            ) {
//                Log.i(
//                    TAG, java.lang.String.format(
//                        "onDataTrackPublished: " +
//                                "[RemoteParticipant: identity=%s], " +
//                                "[RemoteDataTrackPublication: sid=%s, enabled=%b, " +
//                                "subscribed=%b, name=%s]",
//                        remoteParticipant.getIdentity(),
//                        remoteDataTrackPublication.getTrackSid(),
//                        remoteDataTrackPublication.isTrackEnabled(),
//                        remoteDataTrackPublication.isTrackSubscribed(),
//                        remoteDataTrackPublication.getTrackName()
//                    )
//                )
//            }
//
//            fun onDataTrackUnpublished(
//                remoteParticipant: RemoteParticipant,
//                remoteDataTrackPublication: RemoteDataTrackPublication
//            ) {
//                Log.i(
//                    TAG, java.lang.String.format(
//                        "onDataTrackUnpublished: " +
//                                "[RemoteParticipant: identity=%s], " +
//                                "[RemoteDataTrackPublication: sid=%s, enabled=%b, " +
//                                "subscribed=%b, name=%s]",
//                        remoteParticipant.getIdentity(),
//                        remoteDataTrackPublication.getTrackSid(),
//                        remoteDataTrackPublication.isTrackEnabled(),
//                        remoteDataTrackPublication.isTrackSubscribed(),
//                        remoteDataTrackPublication.getTrackName()
//                    )
//                )
//            }
//
//            fun onVideoTrackPublished(
//                remoteParticipant: RemoteParticipant,
//                remoteVideoTrackPublication: RemoteVideoTrackPublication
//            ) {
//                Log.i(
//                    TAG, java.lang.String.format(
//                        "onVideoTrackPublished: " +
//                                "[RemoteParticipant: identity=%s], " +
//                                "[RemoteVideoTrackPublication: sid=%s, enabled=%b, " +
//                                "subscribed=%b, name=%s]",
//                        remoteParticipant.getIdentity(),
//                        remoteVideoTrackPublication.getTrackSid(),
//                        remoteVideoTrackPublication.isTrackEnabled(),
//                        remoteVideoTrackPublication.isTrackSubscribed(),
//                        remoteVideoTrackPublication.getTrackName()
//                    )
//                )
//            }
//
//            fun onVideoTrackUnpublished(
//                remoteParticipant: RemoteParticipant,
//                remoteVideoTrackPublication: RemoteVideoTrackPublication
//            ) {
//                Log.i(
//                    TAG, java.lang.String.format(
//                        "onVideoTrackUnpublished: " +
//                                "[RemoteParticipant: identity=%s], " +
//                                "[RemoteVideoTrackPublication: sid=%s, enabled=%b, " +
//                                "subscribed=%b, name=%s]",
//                        remoteParticipant.getIdentity(),
//                        remoteVideoTrackPublication.getTrackSid(),
//                        remoteVideoTrackPublication.isTrackEnabled(),
//                        remoteVideoTrackPublication.isTrackSubscribed(),
//                        remoteVideoTrackPublication.getTrackName()
//                    )
//                )
//            }
//
//            fun onAudioTrackSubscribed(
//                remoteParticipant: RemoteParticipant,
//                remoteAudioTrackPublication: RemoteAudioTrackPublication?,
//                remoteAudioTrack: RemoteAudioTrack
//            ) {
//                Log.i(
//                    TAG, java.lang.String.format(
//                        "onAudioTrackSubscribed: " +
//                                "[RemoteParticipant: identity=%s], " +
//                                "[RemoteAudioTrack: enabled=%b, playbackEnabled=%b, name=%s]",
//                        remoteParticipant.getIdentity(),
//                        remoteAudioTrack.isEnabled(),
//                        remoteAudioTrack.isPlaybackEnabled(),
//                        remoteAudioTrack.getName()
//                    )
//                )
//            }
//
//            fun onAudioTrackUnsubscribed(
//                remoteParticipant: RemoteParticipant,
//                remoteAudioTrackPublication: RemoteAudioTrackPublication?,
//                remoteAudioTrack: RemoteAudioTrack
//            ) {
//                Log.i(
//                    TAG, java.lang.String.format(
//                        "onAudioTrackUnsubscribed: " +
//                                "[RemoteParticipant: identity=%s], " +
//                                "[RemoteAudioTrack: enabled=%b, playbackEnabled=%b, name=%s]",
//                        remoteParticipant.getIdentity(),
//                        remoteAudioTrack.isEnabled(),
//                        remoteAudioTrack.isPlaybackEnabled(),
//                        remoteAudioTrack.getName()
//                    )
//                )
//            }
//
//            fun onAudioTrackSubscriptionFailed(
//                remoteParticipant: RemoteParticipant,
//                remoteAudioTrackPublication: RemoteAudioTrackPublication,
//                twilioException: TwilioException
//            ) {
//                Log.i(
//                    TAG, java.lang.String.format(
//                        "onAudioTrackSubscriptionFailed: " +
//                                "[RemoteParticipant: identity=%s], " +
//                                "[RemoteAudioTrackPublication: sid=%b, name=%s]" +
//                                "[TwilioException: code=%d, message=%s]",
//                        remoteParticipant.getIdentity(),
//                        remoteAudioTrackPublication.getTrackSid(),
//                        remoteAudioTrackPublication.getTrackName(),
//                        twilioException.getCode(),
//                        twilioException.getMessage()
//                    )
//                )
//            }
//
//            fun onDataTrackSubscribed(
//                remoteParticipant: RemoteParticipant,
//                remoteDataTrackPublication: RemoteDataTrackPublication?,
//                remoteDataTrack: RemoteDataTrack
//            ) {
//                Log.i(
//                    TAG, java.lang.String.format(
//                        "onDataTrackSubscribed: " +
//                                "[RemoteParticipant: identity=%s], " +
//                                "[RemoteDataTrack: enabled=%b, name=%s]",
//                        remoteParticipant.getIdentity(),
//                        remoteDataTrack.isEnabled(),
//                        remoteDataTrack.getName()
//                    )
//                )
//            }
//
//
//            fun onDataTrackUnsubscribed(
//                remoteParticipant: RemoteParticipant,
//                remoteDataTrackPublication: RemoteDataTrackPublication?,
//                remoteDataTrack: RemoteDataTrack
//            ) {
//                Log.i(
//                    TAG, java.lang.String.format(
//                        "onDataTrackUnsubscribed: " +
//                                "[RemoteParticipant: identity=%s], " +
//                                "[RemoteDataTrack: enabled=%b, name=%s]",
//                        remoteParticipant.getIdentity(),
//                        remoteDataTrack.isEnabled(),
//                        remoteDataTrack.getName()
//                    )
//                )
//            }
//
//            fun onDataTrackSubscriptionFailed(
//                remoteParticipant: RemoteParticipant,
//                remoteDataTrackPublication: RemoteDataTrackPublication,
//                twilioException: TwilioException
//            ) {
//                Log.i(
//                    TAG, java.lang.String.format(
//                        "onDataTrackSubscriptionFailed: " +
//                                "[RemoteParticipant: identity=%s], " +
//                                "[RemoteDataTrackPublication: sid=%b, name=%s]" +
//                                "[TwilioException: code=%d, message=%s]",
//                        remoteParticipant.getIdentity(),
//                        remoteDataTrackPublication.getTrackSid(),
//                        remoteDataTrackPublication.getTrackName(),
//                        twilioException.getCode(),
//                        twilioException.getMessage()
//                    )
//                )
//            }
//
//            fun onVideoTrackSubscribed(
//                remoteParticipant: RemoteParticipant,
//                remoteVideoTrackPublication: RemoteVideoTrackPublication?,
//                remoteVideoTrack: RemoteVideoTrack
//            ) {
//                Log.i(
//                    TAG, java.lang.String.format(
//                        "onVideoTrackSubscribed: " +
//                                "[RemoteParticipant: identity=%s], " +
//                                "[RemoteVideoTrack: enabled=%b, name=%s]",
//                        remoteParticipant.getIdentity(),
//                        remoteVideoTrack.isEnabled(),
//                        remoteVideoTrack.getName()
//                    )
//                )
//                addRemoteParticipantVideo(remoteVideoTrack)
//            }
//
//            fun onVideoTrackUnsubscribed(
//                remoteParticipant: RemoteParticipant,
//                remoteVideoTrackPublication: RemoteVideoTrackPublication?,
//                remoteVideoTrack: RemoteVideoTrack
//            ) {
//                Log.i(
//                    TAG, java.lang.String.format(
//                        "onVideoTrackUnsubscribed: " +
//                                "[RemoteParticipant: identity=%s], " +
//                                "[RemoteVideoTrack: enabled=%b, name=%s]",
//                        remoteParticipant.getIdentity(),
//                        remoteVideoTrack.isEnabled(),
//                        remoteVideoTrack.getName()
//                    )
//                )
//                //                removeParticipantVideo(remoteVideoTrack);
//            }
//
//            fun onVideoTrackSubscriptionFailed(
//                remoteParticipant: RemoteParticipant,
//                remoteVideoTrackPublication: RemoteVideoTrackPublication,
//                twilioException: TwilioException
//            ) {
//                Log.i(
//                    TAG, java.lang.String.format(
//                        "onVideoTrackSubscriptionFailed: " +
//                                "[RemoteParticipant: identity=%s], " +
//                                "[RemoteVideoTrackPublication: sid=%b, name=%s]" +
//                                "[TwilioException: code=%d, message=%s]",
//                        remoteParticipant.getIdentity(),
//                        remoteVideoTrackPublication.getTrackSid(),
//                        remoteVideoTrackPublication.getTrackName(),
//                        twilioException.getCode(),
//                        twilioException.getMessage()
//                    )
//                )
//                Snackbar.make(
//                    binding.llCallControls,
//                    java.lang.String.format(
//                        "Failed to subscribe to %s video track",
//                        remoteParticipant.getIdentity()
//                    ),
//                    Snackbar.LENGTH_LONG
//                )
//                    .show()
//            }
//
//            fun onAudioTrackEnabled(
//                remoteParticipant: RemoteParticipant?,
//                remoteAudioTrackPublication: RemoteAudioTrackPublication?
//            ) {
//            }
//
//            fun onAudioTrackDisabled(
//                remoteParticipant: RemoteParticipant?,
//                remoteAudioTrackPublication: RemoteAudioTrackPublication?
//            ) {
//            }
//
//            fun onVideoTrackEnabled(
//                remoteParticipant: RemoteParticipant?,
//                remoteVideoTrackPublication: RemoteVideoTrackPublication?
//            ) {
//            }
//
//            fun onVideoTrackDisabled(
//                remoteParticipant: RemoteParticipant?,
//                remoteVideoTrackPublication: RemoteVideoTrackPublication?
//            ) {
//            }
//        }
//    }
//
//    fun onBackPressed() {
//        super.onBackPressed()
//        if (callType != null && callType.equals(GlobalConstants.INCOMING_CALL)) {
//            val intent = Intent(this@VideoAudioActivity, MainActivity::class.java)
//            startActivity(intent)
//            intent.putExtra(GlobalConstants.FROM, from)
//            finishAffinity()
//        }
//        if (callType != null && callType.equals(GlobalConstants.DIAL_CALL)) {
//            finishAffinity()
//        }
//    }
//
//    private fun ringBell() {
//        if (callType != null && callType.equals(GlobalConstants.DIAL_CALL)) {
//            var afd: AssetFileDescriptor? = null
//            try {
//                afd = requireActivity().getAssets().openFd("ringback.mp3")
//                //player = new MediaPlayer();
//                player?.setAudioStreamType(AudioManager.STREAM_VOICE_CALL)
//                player?.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength())
//                afd.close()
//                player?.prepare()
//                player?.setVolume(1f, 1f)
//                player?.setLooping(true)
//                player?.start()
//            } catch (e: Exception) {
//                e.printStackTrace()
//            }
//        }
//    }
//
//    fun PlayRing() {
//        val path: Uri =
//            Uri.parse("android.resource://" + requireActivity()?.getPackageName().toString() + "/" + R.raw.incoming)
//        Log.e("Path ", "PlayRing: $path")
//        player = MediaPlayer.create(requireActivity(), path)
//        player?.setLooping(true)
//        player?.start()
//    }
//
//    private fun autoDeclined() {
//        if (room != null && room.getState() !== Room.State.CONNECTED) {
//            Handler().postDelayed(Runnable {
//                if (player != null && player.isPlaying()) {
//                    player.stop()
//                }
//                //timer.cancel();
//                binding.llIncomingCall.layoutRipplepulse.stopRippleAnimation()
//                val intent = Intent(this@VideoAudioActivity, MainActivity::class.java)
//                intent.putExtra(GlobalConstants.FROM, from)
//                startActivity(intent)
//                finishAffinity()
//            }, 25000)
//        }
//    }
//
//    private fun connectDialRoom() {
//        Handler().postDelayed(Runnable {
//            if (callType != null && callType.equals(GlobalConstants.DIAL_CALL)) {
//                connectToRoom(roomName)
//            }
//        }, 3000)
//    }
//
//    fun callCallDisconnectApi(context: Context?, senderId: String?) {
//        val apiRequest = ApiRequest()
//        apiRequest.setSenderId(senderId)
//        apiRequest.setPushType(GlobalConstants.AUDIO)
//        ApiUtils.callCallDisconnectApi(context, apiRequest, object : ApiResponseInterface() {
//            fun onResponse(response: Response<ApiResponse?>) {
//                if (response.code() === GlobalConstants.SUCCESS_CODE) {
//                    if (!response.body().getPushdata().isAccepted()) {
//                        val intent = Intent(this@VideoAudioActivity, MainActivity::class.java)
//                        intent.putExtra(GlobalConstants.FROM, from)
//                        startActivity(intent)
//                        finishAffinity()
//                    }
//                }
//            }
//
//            fun onFailure() {}
//        })
//    }
//
//}