package com.appsums.view.fragment

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import com.appsums.DasboardFragment
import com.appsums.R
import com.appsums.databinding.FragmentClientInfoBinding
import com.appsums.databinding.SpinnerItemBinding
import com.appsums.model.GenericModel
import com.appsums.model.ProfessionalDoctorModel
import com.appsums.model.ToolBarModel
import com.appsums.network.ApiCallDoctor
import com.appsums.network.ResponseListener
import com.appsums.utils.CheckPermission
import com.appsums.utils.TakePictureUtils
import com.appsums.utils.cropimage.CropImage
import com.appsums.view.adapters.ArraySpinnerAdapter
import com.appsums.view.base.BaseFragment
import com.appsums.view.fragment.ScheduleActivityFragment.Companion.availability
import retrofit2.Response
import java.io.*


class ClientInfoFragment(val from: Int) :
  BaseFragment<FragmentClientInfoBinding>(FragmentClientInfoBinding::inflate) {
  companion object {
    var primaryId: String = ""
    var secondaryId: String = ""
    var primaryName: String = ""
    var secondaryName: String = ""
  }

  var yrList = ArrayList<String>()
  var monthList = ArrayList<String>()

  private val tempImage = "tempImage"
  private var file: File? = null
  private var path: String? = null
  private var url: String = ""

  override fun getToolBar(): ToolBarModel? {
    val toolBarModel = ToolBarModel()
    toolBarModel.toolBarShow = false
    return toolBarModel
  }

  override fun onResume() {
    super.onResume()
    viewDataBinding?.apply {
      tvprimarySpeciality.setText(primaryName)
      tvSecSpeicality.setText(secondaryName)
    }
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    yrList.clear()
    for (i in 0 until 10) {
      yrList.add("" + i)
    }
    monthList.clear()
    for (i in 0 until 12) {
      monthList.add("" + i)
    }

    viewDataBinding?.apply {
      setSpinner()
      setSpinner1()

      etAvailibility.setOnClickListener {
        displayItAddStack(ScheduleActivityFragment())
      }
      tvprimarySpeciality.setText(primaryName)
      tvSecSpeicality.setText(secondaryName)

      btnCall.setOnClickListener {
        if (isValidate()) {
          var secondaryList = ArrayList<String>()
          secondaryList.add(secondaryId)
          var professionalDoctorModel = ProfessionalDoctorModel(
            url,
            primaryId,
            secondaryList,
            etLicenseNo.text.toString().trim(),
            etYr.text.toString().toInt(),
            etMonth.text.toString().toInt(),
            etPrice.text.toString().toInt(),availability
          )
          callAPi(professionalDoctorModel)
        }

      }

      tvprimarySpeciality.setOnClickListener {
        displayItAddStack(SecondarySpecialityFragment(0))
      }
      tvSecSpeicality.setOnClickListener {
        if (!primaryId.isNullOrEmpty())
          displayItAddStack(SecondarySpecialityFragment(1))
      }

      imgProfileAvatar.setOnClickListener {
        checkPermission()
      }

    }

  }

  fun callAPi(body: ProfessionalDoctorModel) {
    homeActivity?.let {
      ApiCallDoctor.createProfessionalAPi(it, body, object : ResponseListener<GenericModel> {
        override fun onSuccess(mResponse: Response<GenericModel>) {
          if (from == 1) {
            popBack()
          } else {
            displayItNoStack(DasboardFragment())
          }
        }

        override fun onError(msg: String) {

        }
      })
    }
  }

  private fun checkPermission() {
    if (CheckPermission.checkIsMarshMallowVersion()) {
      if (CheckPermission.checkCameraStoragePermission(requireContext())) {
        showImageDailoge(requireActivity(), tempImage, false)
      } else {
        CheckPermission.requestCameraStoragePermission(requireActivity())
      }
    } else {
      showImageDailoge(requireActivity(), tempImage, false)
    }
  }

  override fun onRequestPermissionsResult(
    requestCode: Int,
    permissions: Array<String?>,
    grantResults: IntArray
  ) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    when (requestCode) {
      CheckPermission.REQUEST_CODE_CAMERA_STORAGE_PERMISSION -> {
        if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          showImageDailoge(requireActivity(), tempImage, false)
        }
      }
    }
  }

  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

    if (resultCode == Activity.RESULT_OK) when (requestCode) {
      TakePictureUtils.TAKE_PICTURE -> TakePictureUtils.startCropImage(
        context as Activity?,
        tempImage + ".jpg",
        500,
        500
      )
      TakePictureUtils.PICK_GALLERY -> {
        try {
          val inputStream: InputStream? =
            homeActivity!!.getContentResolver().openInputStream(
              data!!.data!!
            )
          val fileOutputStream: FileOutputStream =
            FileOutputStream(
              File(
                requireContext().getExternalFilesDir("temp"),
                tempImage + ".jpg"
              )
            )
          TakePictureUtils.copyStream(inputStream, fileOutputStream)
          fileOutputStream.close()
          inputStream?.close()
        } catch (e: FileNotFoundException) {
          e.printStackTrace()
        } catch (e: IOException) {
          e.printStackTrace()
        }
        TakePictureUtils.startCropImage(context as Activity?, tempImage + ".jpg", 500, 500)
      }
      TakePictureUtils.CROP_FROM_CAMERA -> {
        path = null
        if (data != null) {
          path = data.getStringExtra(CropImage.IMAGE_PATH)
          if (path != null) file = File(path)
          //    viewModel.callApiFileToUrl(context, file)
          if (path == null) {
            return
          }
          callAPi(path!!)
          setImageWithUri(file, viewDataBinding?.imgProfileAvatar)
//                    Glide.with(context!!)
//                        .load(file)
//                        .placeholder(R.drawable.dummy)
//                        .into<Target<Drawable>>(viewDataBinding)
        }
      }
    }
  }

  fun callAPi(body: String) {
    homeActivity?.let {
      ApiCallDoctor.uploadImg(it, body, object : ResponseListener<GenericModel> {
        override fun onSuccess(mResponse: Response<GenericModel>) {
          url = mResponse.body()?.result?.url ?: ""

        }

        override fun onError(msg: String) {
          Log.d("Dddd==", msg.toString())
        }
      })
    }
  }

  fun setSpinner() {

    val arrayAdapter = ArraySpinnerAdapter(
      requireActivity(),
      yrList,
      object : ArraySpinnerAdapter.CustomSpinnerData<SpinnerItemBinding, String> {
        override fun setDataSpinner(
          binder: SpinnerItemBinding?,
          position: Int,
          model: String
        ) {

          binder?.textSpinner?.text = model.toString()

        }

      }
    )

    viewDataBinding?.apply {

      etYr.setOnClickListener {
        spinner.performClick()
      }
      spinner.adapter = arrayAdapter
      spinner.onItemSelectedListener = object :
        AdapterView.OnItemSelectedListener {
        override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
          etYr.setText(yrList[p2].toString())
        }

        override fun onNothingSelected(p0: AdapterView<*>?) {
        }
      }
    }
  }

  fun setSpinner1() {

    val arrayAdapter = ArraySpinnerAdapter(
      requireActivity(),
      monthList,
      object : ArraySpinnerAdapter.CustomSpinnerData<SpinnerItemBinding, String> {
        override fun setDataSpinner(
          binder: SpinnerItemBinding?,
          position: Int,
          model: String
        ) {

          binder?.textSpinner?.text = model.toString()

        }

      }
    )

    viewDataBinding?.apply {

      etMonth.setOnClickListener {
        spinner1.performClick()
      }
      spinner1.adapter = arrayAdapter
      spinner1.onItemSelectedListener = object :
        AdapterView.OnItemSelectedListener {
        override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
          etMonth.setText(monthList[p2].toString())
        }

        override fun onNothingSelected(p0: AdapterView<*>?) {
        }
      }
    }
  }

  fun isValidate(): Boolean {
    viewDataBinding?.apply {
      if (tvprimarySpeciality.text.isNullOrEmpty()) {
        showToast(getString(R.string.Select_p_Speciality))
        return false
      } else if (tvSecSpeicality.text.isNullOrEmpty()) {
        showToast(getString(R.string.Select_s_Speciality))
        return false
      } else if (etLicenseNo.text.isNullOrEmpty()) {
        showToast(getString(R.string.et_License_Number))
        return false
      } else if (etYr.text.isNullOrEmpty() || etMonth.text.isNullOrEmpty()) {
        showToast(getString(R.string.Select_exp))
        return false
      } else if (availability.isNullOrEmpty()) {
        showToast(getString(R.string.Select_Availability))
        return false
      } else if (etPrice.text.isNullOrEmpty()) {
        showToast(getString(R.string.et_Price))
        return false
      } else {
        return true
      }
    }
    return true
  }

}