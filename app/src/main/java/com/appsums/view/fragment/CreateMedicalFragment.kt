package com.appsums.view.fragment

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import com.appsums.DasboardFragment
import com.appsums.R
import com.appsums.databinding.FragmentCraeteMedicalBinding
import com.appsums.databinding.SpinnerItemBinding
import com.appsums.model.*
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.appsums.utils.CheckPermission
import com.appsums.utils.GlobalConstants
import com.appsums.utils.SharedPrefClass
import com.appsums.utils.TakePictureUtils
import com.appsums.utils.cropimage.CropImage
import com.appsums.view.adapters.ArraySpinnerAdapter
import com.appsums.view.base.BaseFragment
import com.google.gson.Gson
import retrofit2.Response
import java.io.*

private const val TAG = "CreateMedicalFragment"

class CreateMedicalFragment(var from: Int, var model: UserData?) :
    BaseFragment<FragmentCraeteMedicalBinding>(FragmentCraeteMedicalBinding::inflate) {

    private val tempImage = "tempImage"
    private var file: File? = null
    private var path: String? = null
    private var url: String = ""

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.toolBarTtl = getString(R.string.MEDICAL_DETAILS)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            if (model != null) {
                etHt.setText(model?.height)
                etSelectGrp.setText(model?.height)
                etDiabetes.setText(model?.iSDiabetes.toString())
                etAllergy.setText(model?.allergyDeatils)
                etDiaDetail.setText(model?.diabetesDetails)
                etWt.setText(model?.weight)
                etNotes.setText(model?.familyNotes)
                url = model?.medicalFile ?: ""
                imgProfileAvatar.setText(url)
            }

            imgProfileAvatar.setOnClickListener {
                checkPermission()
            }

            btnCall.setOnClickListener {
                if (model != null) {
                    callAPi(
                        EditProfileModel(
                            model!!.firstName,
                            model!!.lastName,
                            model!!.gender,
                            model!!.date_of_birth,
                            model!!.marital_status,
                            model!!.location?.address,
                            model!!.bio,
                            model!!.profilePic,
                            model!!.location.coordinates[0],
                            model!!.location.coordinates[1],
//                                (SharedPrefClass().getPrefValue(
//                                    requireContext(),
//                                    GlobalConstants.LATTITUDE
//                                ) ?: 28.536288450553798) as Double,
//                                (SharedPrefClass().getPrefValue(
//                                    requireContext(),
//                                    GlobalConstants.LONGITUDE
//                                ) ?: 77.38301406357039) as Double,

                            etHt.text.toString(),
                            etWt.text.toString(),
                            etSelectGrp.text.toString(),
                            etDiaDetail.text.toString(),
                            etAllergy.text.toString(),
                            etNotes.text.toString(),
                            url, etDiabetes.text.toString().toBoolean()
                            //28.536288450553798, 77.38301406357039
                        )
                    )
                } else {
                    if (isValidate())
                        callAPi(
                            MedicalModel(
                                etHt.text.toString(),
                                etWt.text.toString(),
                                etSelectGrp.text.toString(),
                                etDiaDetail.text.toString(),
                                etAllergy.text.toString(),
                                etNotes.text.toString(),
                                url, etDiabetes.text.toString().toBoolean()
                            )
                        )
                }
            }

        }

        setSpinner()
        setSpinner1()
    }

    fun setSpinner() {

        val list = ArrayList<Boolean>()
        list.add(true)
        list.add(false)

        val arrayAdapter = ArraySpinnerAdapter(
            requireActivity(),
            list,
            object : ArraySpinnerAdapter.CustomSpinnerData<SpinnerItemBinding, Boolean> {
                override fun setDataSpinner(
                    binder: SpinnerItemBinding?,
                    position: Int,
                    model: Boolean
                ) {

                    binder?.textSpinner?.text = model.toString()

                }

            }
        )

        viewDataBinding?.apply {

            etDiabetes.setOnClickListener {
                spinner.performClick()
            }
            spinner.adapter = arrayAdapter
            spinner.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    etDiabetes.setText(list[p2].toString())
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                }
            }
        }
    }

    fun setSpinner1() {

        val list = ArrayList<String>()
        list.add("A")
        list.add("B")
        list.add("AB")
        list.add("A+")
        list.add("O")

        val arrayAdapter = ArraySpinnerAdapter(
            requireActivity(),
            list,
            object : ArraySpinnerAdapter.CustomSpinnerData<SpinnerItemBinding, String> {
                override fun setDataSpinner(
                    binder: SpinnerItemBinding?,
                    position: Int,
                    model: String
                ) {

                    binder?.textSpinner?.text = model.toString()

                }

            }
        )

        viewDataBinding?.apply {

            etSelectGrp.setOnClickListener {
                spinner1.performClick()
            }
            spinner1.adapter = arrayAdapter
            spinner1.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    etSelectGrp.setText(list[p2].toString())
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                }
            }
        }
    }

    fun callAPi(body: MedicalModel) {
        homeActivity?.let {
            ApiCall.createMedicalProfileAPi(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {

                    SharedPrefClass().putObject(
                        it,
                        GlobalConstants.MEDICAL_STATUS,
                        true
                    )

                    displayItNoStack(DasboardFragment())
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    fun callAPi(body: EditProfileModel) {
        homeActivity?.let {
            ApiCall.editProfileAPi(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                   // Log.d(TAG, "onSuccess: ${Gson().toJson(mResponse)}")
                    popBack()
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    fun callAPi(body: String) {
        homeActivity?.let {
            ApiCall.uploadImg(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    url = mResponse.body()?.result?.url ?: ""
                    viewDataBinding?.imgProfileAvatar?.setText(url)
                }

                override fun onError(msg: String) {
                    showToast(msg)
                    Log.d("Dddd==", msg.toString())
                }
            })
        }
    }


    private fun checkPermission() {
        if (CheckPermission.checkIsMarshMallowVersion()) {
            if (CheckPermission.checkCameraStoragePermission(requireContext())) {
                showImageDailoge(requireActivity(), tempImage, false)
            } else {
                CheckPermission.requestCameraStoragePermission(requireActivity())
            }
        } else {
            showImageDailoge(requireActivity(), tempImage, false)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            CheckPermission.REQUEST_CODE_CAMERA_STORAGE_PERMISSION -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showImageDailoge(requireActivity(), tempImage, false)
                }
            }
        }
    }

    fun isValidate(): Boolean {
        viewDataBinding?.apply {
            if (etHt.text.isNullOrEmpty()) {
                showToast(getString(R.string.et_ht))
                return false
            } else if (etWt.text.isNullOrEmpty()) {
                showToast(getString(R.string.et_wt))
                return false
            } else if (etDiaDetail.text.isNullOrEmpty()) {
                showToast(getString(R.string.et_Diabetes_Details))
                return false
            } else if (etAllergy.text.isNullOrEmpty()) {
                showToast(getString(R.string.Do_allergies))
                return false
            } else if (etNotes.text.isNullOrEmpty()) {
                showToast(getString(R.string.et_Family_Notes))
                return false
            } else {
                return true
            }
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (resultCode == Activity.RESULT_OK) when (requestCode) {
            TakePictureUtils.TAKE_PICTURE -> TakePictureUtils.startCropImage(
                context as Activity?,
                tempImage + ".jpg",
                500,
                500
            )
            TakePictureUtils.PICK_GALLERY -> {
                try {
                    val inputStream: InputStream? =
                        homeActivity!!.getContentResolver().openInputStream(
                            data!!.data!!
                        )
                    val fileOutputStream: FileOutputStream =
                        FileOutputStream(
                            File(
                                requireContext().getExternalFilesDir("temp"),
                                tempImage + ".jpg"
                            )
                        )
                    TakePictureUtils.copyStream(inputStream, fileOutputStream)
                    fileOutputStream.close()
                    inputStream?.close()
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                TakePictureUtils.startCropImage(context as Activity?, tempImage + ".jpg", 500, 500)
            }
            TakePictureUtils.CROP_FROM_CAMERA -> {
                path = null
                if (data != null) {
                    path = data.getStringExtra(CropImage.IMAGE_PATH)
                    if (path != null) file = File(path)
                    //    viewModel.callApiFileToUrl(context, file)
                    if (path == null) {
                        return
                    }
                    callAPi(path!!)

                    //      viewDataBinding?.imgProfileAvatar?.setText(path)
                    //   setImageWithUri1(file, viewDataBinding?.imgProfileAvatar)
//                    Glide.with(context!!)
//                        .load(file)
//                        .placeholder(R.drawable.dummy)
//                        .into<Target<Drawable>>(viewDataBinding)
                }
            }
        }
    }


}