package com.appsums.view.fragment

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.appsums.R
import com.appsums.databinding.FragmentPhysicanDetailBinding
import com.appsums.databinding.ItemRatingAppointmentBinding
import com.appsums.databinding.ItemRatingBinding
import com.appsums.model.ToolBarModel
import com.appsums.view.adapters.RecyclerCallback
import com.appsums.view.adapters.RecyclerViewGenricAdapter
import com.appsums.view.base.BaseFragment


class PhysicanDetailFragment :
    BaseFragment<FragmentPhysicanDetailBinding>(FragmentPhysicanDetailBinding::inflate) {

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.toolBarTtl = getString(R.string.VIEW_DETAILS)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            underlineText(tvViewReview)

            recyclerView.layoutManager = LinearLayoutManager(requireContext())
            setAdapter1()


        }

    }


    fun setAdapter1() {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        val rvAdapProgress = RecyclerViewGenricAdapter<String, ItemRatingAppointmentBinding>(
            list,
            R.layout.item_rating_appointment, object :
                RecyclerCallback<ItemRatingAppointmentBinding, String> {
                override fun bindData(
                    binder: ItemRatingAppointmentBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {


                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }


}