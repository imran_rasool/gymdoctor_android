package com.appsums.view.fragment

import android.os.Bundle
import android.view.View
import com.appsums.databinding.FragmentWebviewBinding
import com.appsums.model.ToolBarModel
import com.appsums.view.base.BaseFragment


class WebViewFragment(var ttl: String) :
    BaseFragment<FragmentWebviewBinding>(FragmentWebviewBinding::inflate) {

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.toolBarTtlShow = true
        toolBarModel.toolBarTtl = ttl
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {


        }

    }


}