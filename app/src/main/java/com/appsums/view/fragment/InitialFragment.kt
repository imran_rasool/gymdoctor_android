package com.appsums.view.fragment

import android.os.Bundle
import android.util.Log
import android.view.View
import com.appsums.LoginFragment
import com.appsums.SignUpFragment
import com.appsums.databinding.FragmentInitialBinding
import com.appsums.model.ToolBarModel
import com.appsums.view.base.BaseFragment
import com.appsums.databinding.FragmentLoginBinding
import com.appsums.model.GenericModel
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.google.gson.Gson
import retrofit2.Response


class InitialFragment : BaseFragment<FragmentInitialBinding>(FragmentInitialBinding::inflate) {

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            btnlogin.setOnClickListener {
                displayItNoStack(LoginFragment())
            }

            btnCraeteAcc.setOnClickListener {
                displayItNoStack(SignUpFragment())
            }

        }
    }


}