package com.appsums.view.fragment

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.appsums.R
import com.appsums.databinding.FragmentScheduleAvailibilityBinding
import com.appsums.databinding.ItemDayBinding
import com.appsums.databinding.ItemTimeSlotsBinding
import com.appsums.databinding.SpinnerItemBinding
import com.appsums.model.AvailabilityDoctorModle
import com.appsums.model.SloData
import com.appsums.model.ToolBarModel
import com.appsums.view.adapters.ArraySpinnerAdapter
import com.appsums.view.adapters.RecyclerCallback
import com.appsums.view.adapters.RecyclerViewGenricAdapter
import com.appsums.view.base.BaseFragment
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class ScheduleActivityFragment :
    BaseFragment<FragmentScheduleAvailibilityBinding>(FragmentScheduleAvailibilityBinding::inflate) {

    private var rvAdapDay: RecyclerViewGenricAdapter<ScheduleActivityFragment.DaySelect, ItemDayBinding>?=null
    companion object {
        var availability = ArrayList<AvailabilityDoctorModle>()
    }

    data class DaySelect(var name: String, var isSelect: Boolean)

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.toolBarTtl = getString(R.string.SCHEDULE_AVAILABILITY)
        return toolBarModel
    }

    private var rvAdapProgress: RecyclerViewGenricAdapter<SloData, ItemTimeSlotsBinding>? = null
    var dayList = ArrayList<DaySelect>()
    var slotsList = ArrayList<String>()
    val timeSlotsList = ArrayList<SloData>()
    var selectDurPosition = 0
    var selectDayPosition = 0
    var selectDayName = ""

    fun setDayList(){
        dayList.clear()
        dayList.add(DaySelect("Mon", false))
        dayList.add(DaySelect("Tue", false))
        dayList.add(DaySelect("Wed", false))
        dayList.add(DaySelect("Thu", false))
        dayList.add(DaySelect("Fri",false))
        dayList.add(DaySelect("Sat", false))
        dayList.add(DaySelect("Sun", false))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        slotsList.add("15 Mins")
        slotsList.add("30 Mins")
        slotsList.add("45 Mins")
        slotsList.add("60 Mins")

      setDayList()

        viewDataBinding?.apply {
            setSpinner()
            recyclerView.layoutManager = GridLayoutManager(context, 2)

            val layoutManager = FlexboxLayoutManager(context)
            layoutManager.flexDirection = FlexDirection.ROW
            layoutManager.justifyContent = JustifyContent.CENTER

            recyclerView1.layoutManager = layoutManager
//                StaggeredGridLayoutManager(requireContext(), false)
            setAdapter1(recyclerView)
            setAdapter(recyclerView1)

            etStartTime.setOnClickListener {
                timePicker(object : FetchTimeSlot {
                    override fun getTimeSlot(formattedTime: String) {
                        etStartTime.setText(formattedTime)
                    }
                })
            }

            eEndTime.setOnClickListener {
                if (!etStartTime.text.isNullOrEmpty()) {
                    timePicker(object : FetchTimeSlot {
                        override fun getTimeSlot(formattedTime: String) {
                            eEndTime.setText(formattedTime)

                            displayTimeSlots(
                                etStartTime.text.toString(),
                                eEndTime.text.toString()
                            )
                        }
                    })

                }
            }

            btnCall.setOnClickListener {
                popBack()
            }
        }

    }

    interface FetchTimeSlot {
        fun getTimeSlot(formattedTime: String)
    }

    fun setAdapter1(recyclerView: RecyclerView) {
        rvAdapProgress = RecyclerViewGenricAdapter(
            timeSlotsList,
            R.layout.item_time_slots, object :
                RecyclerCallback<ItemTimeSlotsBinding, SloData> {
                override fun bindData(
                    binder: ItemTimeSlotsBinding,
                    model: SloData,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {
                        tvSlot.text = model.slotTime
                    }
                }
            })
        recyclerView.adapter = rvAdapProgress
        recyclerView.isNestedScrollingEnabled = false

    }

    fun setAdapter(recyclerView: RecyclerView) {

         rvAdapDay = RecyclerViewGenricAdapter<DaySelect, ItemDayBinding>(
            dayList,
            R.layout.item_day, object :
                RecyclerCallback<ItemDayBinding, DaySelect> {
                override fun bindData(
                    binder: ItemDayBinding,
                    model: DaySelect,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {
                        tvDay.text = model.name

                        if (model.isSelect){
                            cardView.setCardBackgroundColor(ContextCompat.getColor(requireContext(),R.color.colorRed))
                            tvDay.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))

                        }else{
                            cardView.setCardBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorGray3))
                            tvDay.setTextColor(ContextCompat.getColor(requireContext(), R.color.black))
                        }

                        cardView.setOnClickListener {
                            timeSlotsList?.clear()
                            rvAdapProgress?.notifyDataSetChanged()
                            viewDataBinding?.etStartTime?.setText("--")
                            viewDataBinding?.eEndTime?.setText("--")
                            dayList[position].isSelect = !dayList[position].isSelect
                            selectDayPosition = position
                            selectDayName = dayList[position].name

                            cardView.setCardBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorRed))
                            tvDay.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))

                            var checkExist = 0
                            for (i in availability) {
                                if (i.day == model.name) {
                                    checkExist = 1
                                    if (!i.slots.isNullOrEmpty()) {
                                        timeSlotsList?.clear()
                                        timeSlotsList?.addAll(i.slots!!)
                                        rvAdapProgress?.notifyDataSetChanged()
                                    }
                                }
                            }

                            if (checkExist == 0) {
                                availability.add(AvailabilityDoctorModle(model.name))
                            }


                        }
                    }
                }
            })
        recyclerView.adapter = rvAdapDay
        recyclerView.isNestedScrollingEnabled = false

    }

    fun setSpinner() {

        val arrayAdapter = ArraySpinnerAdapter(
            requireActivity(),
            slotsList,
            object : ArraySpinnerAdapter.CustomSpinnerData<SpinnerItemBinding, String> {
                override fun setDataSpinner(
                    binder: SpinnerItemBinding?,
                    position: Int,
                    model: String
                ) {

                    binder?.textSpinner?.text = model.toString()



                }

            }
        )

        viewDataBinding?.apply {

            etSlotDuration.setOnClickListener {
                spinner.performClick()
            }
            spinner.adapter = arrayAdapter
            spinner.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    etSlotDuration.setText(slotsList[p2])
                    selectDurPosition = p2
                    viewDataBinding?.etStartTime?.setText("--")
                    viewDataBinding?.eEndTime?.setText("--")
                    setDayList()
                    availability.clear()
                    rvAdapDay?.notifyDataSetChanged()
                    timeSlotsList.clear()
                    rvAdapProgress?.notifyDataSetChanged()
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                }
            }
        }
    }

    fun displayTimeSlots(startTime: String, endTime: String) {
        timeSlotsList?.clear()
        val timeValue = "2015-10-28T18:37:04.899+05:30"
        val stringTokenizer = StringTokenizer(timeValue, "T")
        val dateValue: String = stringTokenizer.nextElement().toString()
        val endDateValue = "2015-10-28"

        val time1 = startTime
        val time2 = endTime
        val format = "yyyy-MM-dd hh:mm a"
        val sdf = SimpleDateFormat(format)
        try {
            val dateObj1: Date = sdf.parse("$dateValue $time1")
            val dateObj2: Date = sdf.parse("$endDateValue $time2")
            var dif: Long = dateObj1.getTime()
            while (dif < dateObj2.getTime()) {
                val slot1 = Date(dif)
                if (selectDurPosition == 0) {
                    dif += 900000
                } else if (selectDurPosition == 1) {
                    dif += 1800000
                } else if (selectDurPosition == 2) {
                    dif += 2700000
                } else if (selectDurPosition == 3) {
                    dif += 3600000
                }
                val slot2 = Date(dif)
                val sdf1 = SimpleDateFormat("hh:mm a")

                timeSlotsList.add(
                    SloData(sdf1.format(slot1).toString())
                )
            }
            rvAdapProgress?.notifyDataSetChanged()
            for (i in 0 until availability.size) {
                if (availability[i].day == selectDayName) {
                    availability[i].slots?.clear()
                    availability[i].startTime=viewDataBinding?.etStartTime?.text.toString()
                    availability[i].endTime=viewDataBinding?.eEndTime?.text.toString()
                    availability[i].interval=viewDataBinding?.etSlotDuration?.text.toString().replace("Mins","")
                    availability[i].slots?.addAll(timeSlotsList)
                }
            }


        } catch (ex: Exception) {
            Log.d("Ddd====", ex.toString())
            ex.printStackTrace()
        }
    }

}