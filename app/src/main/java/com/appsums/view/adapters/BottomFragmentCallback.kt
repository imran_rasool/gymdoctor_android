package com.appsums.view.adapters

import androidx.viewbinding.ViewBinding


interface BottomFragmentCallback<VM : ViewBinding, T> {
    fun bindData(binder: VM)

}
