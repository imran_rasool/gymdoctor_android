package com.appsums.view.adapters

import androidx.viewbinding.ViewBinding

interface TabPagerCallback<VM : ViewBinding> {
    fun bindData(binder: VM?, position: Int)

    fun setTitle(position: Int): String
}

