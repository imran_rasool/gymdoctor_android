package com.appsums.view.listener

import com.appsums.model.ToolBarModel


interface FragmentView {
    fun getToolBar(): ToolBarModel?
}