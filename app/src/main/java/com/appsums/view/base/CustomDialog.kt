package com.appsums.view.base

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import com.appsums.R
import com.appsums.databinding.*
import com.appsums.model.ParamModel
import com.appsums.model.SubscriptionModel
import com.appsums.view.activities.HomeActivity


class CustomDialog(val mContainer: HomeActivity) {

    var dialog: Dialog? = null

    private fun initDialog(layout: View): Dialog {
        if (dialog != null) {
            dialog!!.dismiss()
            dialog = null
        }
        dialog = CustomDialog(mContainer, R.style.mytheme)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(true)
        dialog!!.setContentView(layout)
        return dialog!!
    }

    class CustomDialog(context: Context, themeId: Int) : Dialog(context, themeId) {
        override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
            currentFocus?.let {
                val imm: InputMethodManager = context.applicationContext.getSystemService(
                    Context.INPUT_METHOD_SERVICE
                ) as (InputMethodManager)
                imm.hideSoftInputFromWindow(it.windowToken, 0)
            }
            return super.dispatchTouchEvent(ev)
        }
    }


    fun openCancelAppDialog(
        action: DialogListener
    ) {
        val binding = DataBindingUtil
            .inflate<DialogCancalAppointmntBinding>(
                LayoutInflater.from(mContainer),
                R.layout.dialog_cancal_appointmnt,
                null,
                false
            )

        val dialog = initDialog(binding.root)
        dialog.setCancelable(false)
        dialog.show()

        binding.apply {
            yesBT.setOnClickListener {
                action.positiveBtn()
                dialog.dismiss()
            }

            noBT.setOnClickListener {
                dialog.dismiss()
            }
        }
    }

    fun openSubscriptionDialog(
        subscriptionModel: SubscriptionModel,
        action: DialogListener
    ) {
        val binding = DataBindingUtil
            .inflate<DialogSubscriptionBinding>(
                LayoutInflater.from(mContainer),
                R.layout.dialog_subscription,
                null,
                false
            )

        val dialog = initDialog(binding.root)
        dialog.setCancelable(true)
        dialog.show()

        binding.apply {

            tvPrice.setText("$" + subscriptionModel.price)
            tvSubTotal.setText("$" + subscriptionModel.price)
            tvTotal.setText("$" + subscriptionModel.price)

            tvApplyCoupon.setOnClickListener {
                dialog.dismiss()
                action.positiveBtn(ParamModel("1"))

            }

            btn.setOnClickListener {
                action.positiveBtn()
                dialog.dismiss()
            }

        }
    }

    fun docOpenSubscriptionDialog(
        subscriptionModel: SubscriptionModel,
        action: DialogListener
    ) {
        val binding = DataBindingUtil
            .inflate<DialogSubscriptionBinding>(
                LayoutInflater.from(mContainer),
                R.layout.dialog_subscription,
                null,
                false
            )

        val dialog = initDialog(binding.root)
        dialog.setCancelable(true)
        dialog.show()

        binding.apply {
            tvTtlRef.visibility=View.VISIBLE
            tvReferalPt.visibility=View.VISIBLE
            tvtax.visibility=View.GONE
            tvPrice.visibility=View.GONE

            tvReferalPt.setText("$" + subscriptionModel.price)
            tvSubTotal.setText("$" + subscriptionModel.price)
            tvTotal.setText("$" + subscriptionModel.price)

            tvApplyCoupon.setOnClickListener {
                dialog.dismiss()
                action.positiveBtn(ParamModel("1"))

            }

            btn.setOnClickListener {
                action.positiveBtn()
                dialog.dismiss()
            }

        }
    }

    fun openCancelReasonAppDialog(
        action: DialogListener
    ) {
        val binding = DataBindingUtil
            .inflate<DialogReasonBinding>(
                LayoutInflater.from(mContainer),
                R.layout.dialog_reason,
                null,
                false
            )

        val dialog = initDialog(binding.root)
        dialog.setCancelable(false)
        dialog.show()

        binding.apply {
            btn1.setOnClickListener {
                action.positiveBtn()
                dialog.dismiss()
            }

            btn2.setOnClickListener {
                dialog.dismiss()
            }
        }
    }

    fun openDelPresDialog(
        action: DialogListener
    ) {
        val binding = DataBindingUtil
            .inflate<DialogDelPresBinding>(
                LayoutInflater.from(mContainer),
                R.layout.dialog_del_pres,
                null,
                false
            )

        val dialog = initDialog(binding.root)
        dialog.setCancelable(false)
        dialog.show()

        binding.apply {
            yesBT.setOnClickListener {
                action.positiveBtn()
                dialog.dismiss()
            }

            noBT.setOnClickListener {
                dialog.dismiss()
            }
        }
    }

    fun openChkAppDialog(
        action: DialogListener
    ) {
        val binding = DataBindingUtil
            .inflate<DialogChkAppBinding>(
                LayoutInflater.from(mContainer),
                R.layout.dialog_chk_app,
                null,
                false
            )

        val dialog = initDialog(binding.root)
        dialog.setCancelable(false)
        dialog.show()

        binding.apply {
            noBT.setOnClickListener {
                action.positiveBtn()
                dialog.dismiss()
            }

        }
    }

    fun openLogoutDialog(
        action: DialogListener
    ) {
        val binding = DataBindingUtil
            .inflate<DialogLogoutBinding>(
                LayoutInflater.from(mContainer),
                R.layout.dialog_logout,
                null,
                false
            )

        val dialog = initDialog(binding.root)
        dialog.setCancelable(false)
        dialog.show()

        binding.apply {
            yesBT.setOnClickListener {
                action.positiveBtn()
                dialog.dismiss()
            }

            noBT.setOnClickListener {
                dialog.dismiss()
            }
        }
    }

    fun openRemoveCardDialog(
        action: DialogListener
    ) {
        val binding = DataBindingUtil
            .inflate<DialogRemoveCardBinding>(
                LayoutInflater.from(mContainer),
                R.layout.dialog_remove_card,
                null,
                false
            )

        val dialog = initDialog(binding.root)
        dialog.setCancelable(false)
        dialog.show()

        binding.apply {
            yesBT.setOnClickListener {
                action.positiveBtn()
                dialog.dismiss()
            }

            noBT.setOnClickListener {
                dialog.dismiss()
            }
        }
    }

    fun openDelNotiDialog(
        action: DialogListener
    ) {
        val binding = DataBindingUtil
            .inflate<DialogDelNotiBinding>(
                LayoutInflater.from(mContainer),
                R.layout.dialog_del_noti,
                null,
                false
            )

        val dialog = initDialog(binding.root)
        dialog.setCancelable(false)
        dialog.show()

        binding.apply {
            yesBT.setOnClickListener {
                action.positiveBtn()
                dialog.dismiss()
            }

            noBT.setOnClickListener {
                dialog.dismiss()
            }
        }
    }


    interface DialogListener {
        fun positiveBtn()
        fun positiveBtn(paramModel: ParamModel)
    }


}