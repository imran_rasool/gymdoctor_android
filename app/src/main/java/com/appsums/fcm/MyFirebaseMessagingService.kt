package com.appsums.fcm

import android.content.ContentValues.TAG
import android.content.Intent
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.appsums.application.GymApplication
import com.appsums.utils.GlobalConstants
import com.appsums.utils.SharedPrefClass
import com.appsums.view.fragment.VideoAudioActivity
import com.appsums.view.fragment.twilio.VideoActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MyFirebaseMessagingService : FirebaseMessagingService() {

    val min = 20
    val max = 80
    var intent: Intent? = null

    override fun onNewToken(token: String) {
        Log.d(TAG, "Refreshed token: $token")

        Handler(Looper.getMainLooper()).post(Runnable {
            SharedPrefClass().putObject(
                GymApplication.instance,
                GlobalConstants.DEVICE_ID,
                token
            )
        })

    }


    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.from)
        Log.d(TAG, "NotificationData  " + remoteMessage.data)
        val data = remoteMessage.data
        if (data.get("pushType") == GlobalConstants.AUDIO) {
            val mainIntent = Intent(this, VideoAudioActivity::class.java)
            mainIntent.putExtra(GlobalConstants.TWILLIO_TOKEN, data.get("twilioToken"))
            //mainIntent.putExtra(GlobalConstants.ROOM_ID, data.get("roomId"));
            mainIntent.putExtra(GlobalConstants.ROOM_ID, data.get("room"))
            mainIntent.putExtra(GlobalConstants.SENDER_ID, data.get("senderId"))
            mainIntent.putExtra(GlobalConstants.RECEIVER_ID, data.get("receiverId"))
            mainIntent.putExtra(GlobalConstants.FIRST_NAME, data.get("fullName"))
//            mainIntent.putExtra(GlobalConstants.LAST_NAME, data.get("lastName"))
            mainIntent.putExtra(GlobalConstants.PUSH_TYPE, data.get("pushType"))
        /*    mainIntent.putExtra(GlobalConstants.PROFILE_PIC, data.get("profilePic"))
            mainIntent.putExtra(
                GlobalConstants.FROM,
                PreferencesManager.Companion.getStringPreferences(this, GlobalConstants.FROM)
            )*/
            mainIntent.putExtra(GlobalConstants.ACTION, GlobalConstants.CALLING)
            mainIntent.putExtra(GlobalConstants.CALLING, GlobalConstants.INCOMING_CALL)
            mainIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(mainIntent)
        }


        /*Intent intent = new Intent(this, IncomingCallActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("twillioToken", data.get("twillioToken"));
            bundle.putString("roomId", data.get("roomId"));
            bundle.putString("salutation", data.get("salutation"));
            bundle.putString("firstName", data.get("firstName"));
            bundle.putString("lastName", data.get("lastName"));
            intent.putExtras(bundle);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);*/
        //by sohail
        if (data["pushType"] == GlobalConstants.VIDEO) {
            val mainIntent = Intent(this, VideoActivity::class.java)
            mainIntent.putExtra(GlobalConstants.TWILLIO_TOKEN, data["twilioToken"])
            //mainIntent.putExtra(GlobalConstants.ROOM_ID, data.get("roomId"));
            mainIntent.putExtra(GlobalConstants.ROOM_ID, data["room"])
            mainIntent.putExtra(GlobalConstants.SENDER_ID, data["senderId"])
            mainIntent.putExtra(GlobalConstants.RECEIVER_ID, data["receiverId"])
//            mainIntent.putExtra(GlobalConstants.IS_CALL_ACCEPT, data["isAccepted"])
            //mainIntent.putExtra(GlobalConstants.SALUTATION, data.get("salutation"));
            mainIntent.putExtra(GlobalConstants.FIRST_NAME, data["fullName"])
//            mainIntent.putExtra(GlobalConstants.LAST_NAME, data["lastName"])
            mainIntent.putExtra(GlobalConstants.PUSH_TYPE, data["pushType"])
//            mainIntent.putExtra(GlobalConstants.PROFILE_PIC, data["profilePic"])
         /*   mainIntent.putExtra(
                GlobalConstants.FROM,
                PreferencesManager.Companion.getStringPreferences(this, GlobalConstants.FROM)
            )*/
            mainIntent.putExtra(GlobalConstants.ACTION, GlobalConstants.CALLING)
            mainIntent.putExtra(GlobalConstants.CALLING, GlobalConstants.INCOMING_CALL)
            mainIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(mainIntent)
        }
    }



}