package com.appsums.network

import com.appsums.model.*
import com.google.gson.JsonObject
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface API {


    @POST("patient/resetPassword")
    fun resetPassword(
        @Body body: ResetPasswordModel
    ): Call<GenericModel>

    @POST("patient/signup")
    fun signUp(
        @Body body: SignupModel
    ): Call<GenericModel>


    @POST("doctor/signup")
    fun dsignUp(
        @Body body: SignupModel
    ): Call<GenericModel>


    @POST("doctor/verifyOtp")
    fun dverifyOtp(
        @Body body: OtpModel
    ): Call<GenericModel>

    @POST("patient/verifyOtp")
    fun verifyOtp(
        @Body body: OtpModel
    ): Call<GenericModel>

    @POST("patient/login")
    fun login(
        @Body body: LoginModel
    ): Call<GenericModel>


    @POST("doctor/login")
    fun dlogin(
        @Body body: LoginModel
    ): Call<GenericModel>

    @POST("patient/forgotPassword")
    fun forgotPassword(
        @Body body: ForgotPasswordModel
    ): Call<GenericModel>

    @POST("patient/forgotPassword")
    fun forgotPassword(
        @Body body: ForgotPasswordNoModel
    ): Call<GenericModel>


    @POST("patient/login")
    fun login(
        @Body body: LoginPhnModel
    ): Call<GenericModel>


    @POST("patient/homeData")
    fun homeApi(
        @Body body: LatLngModel
    ): Call<GenericModel>

    @POST("patient/nearByLaboratory")
    fun nearByLaboratoryApi(
        @Body body: LatLngModel
    ): Call<GenericModel>

    @POST("patient/nearByServiceProvider")
    fun nearByServiceProvider(
        @Body body: LatLngSprciltyModel
    ): Call<GenericModel1>

    @POST("patient/favourite")
    fun favStatusAPi(
        @Body body: FavouriteModal
    ): Call<GenericModel1>

    @POST("patient/laboratoryHomeData")
    fun laboratoryHomeDataApi(
        @Body body: LatLngModel
    ): Call<GenericModel>

    @POST("doctor/createPersonalProfile")
    fun dcreateProfileApi(
        @Body body: ProfileDoctorModel
    ): Call<GenericModel>


    @POST("doctor/createProfessionalProfile")
    fun dcreateProfessionalApi(
        @Body body: ProfessionalDoctorModel
    ): Call<GenericModel>


    @POST("patient/createPersonalProfile")
    fun createProfileApi(
        @Body body: ProfileModel
    ): Call<GenericModel>

    @POST("patient/editProfile")
    fun editProfileApi(
        @Body body: EditProfileModel
    ): Call<GenericModel>

    @POST("call/getTwilioToken")
    fun callTwillioToken(
        @Body body: TwillioModel
    ): Call<GenericModel>


    @POST("patient/createMedicalProfile")
    fun createMedicalProfileApi(
        @Body body: MedicalModel
    ): Call<GenericModel>

    @POST("patient/BuySubscription")
    fun buySubscriptionApi(
        @Body body: BuySubscriptionModel
    ): Call<GenericModel>

    @POST("doctor/BuySubscription")
    fun dbuySubscriptionApi(
        @Body body: BuySubscriptionModel
    ): Call<GenericModel>

    @POST("patient/resendOtp")
    fun resendOtpApi(
        @Body body: ResendOtpModel
    ): Call<GenericModel>

    @POST("patient/bookAppointment")
    fun bookAppointmentApi(
        @Body body: BookModel
    ): Call<GenericModel>

    @POST("doctor/resendOtp")
    fun dresendOtpApi(
        @Body body: ResendOtpModel
    ): Call<GenericModel>

    @GET("patient/myProfile")
    fun profile(
    ): Call<GenericModel>

    @GET("patient/serviceProviderTypeList")
    fun serviceProviderTypeList(
    ): Call<GenericModel>

    @GET("patient/upcomingAppointments")
    fun upcomingAppointmentsList(
    ): Call<GenericModel>

    @GET("patient/previousAppointments")
    fun previousAppointmentsList(
    ): Call<GenericModel>

    @GET("patient/secondrySpecialtyList")
    fun secondrySpecialtyList(
        @Query("primary_specialtyId") id: String
    ): Call<GenericModel>


    @GET("patient/getSlot")
    fun getSlot(
        @Query("serviceProviderId") serviceProviderId: String,
        @Query("bookingDate") bookingDate: String
    ): Call<GenericModel>

    @GET("patient/viewAppointment/{id}")
    fun viewAppointment(
        @Path("id") id: String
    ): Call<GenericModel>


    @GET("doctor/secondrySpecialtyList")
    fun dsecondrySpecialtyList(
        @Query("primary_specialtyId") id: String
    ): Call<GenericModel>

    @GET("patient/subscriptionList")
    fun subscriptionListApi(
    ): Call<GenericModel>

    @GET("doctor/subscriptionList")
    fun dsubscriptionListApi(
    ): Call<GenericModel>

    @GET("doctor/primarySpecialtyList")
    fun primarySpecialtyListApi(
    ): Call<GenericModel>

    @GET("patient/couponList")
    fun couponsListApi(
    ): Call<GenericModel>


    @GET("doctor/couponList")
    fun dcouponsListApi(
    ): Call<GenericModel>

    @Multipart
    @POST("common/getUrl")
    fun imgUploadApi(@PartMap hashMap: HashMap<String, RequestBody>): Call<GenericModel>

    @GET("getbanners")
    fun getBanner(@QueryMap hashMap: Map<String, String>): Call<JsonObject>

    @GET("checkOut")
    fun getChkOut(@QueryMap hashMap: Map<String, String>): Call<JsonObject>

    @FormUrlEncoded
    @POST("allcategories")
    fun getAllCategories(@FieldMap hashMap: Map<String, String>): Call<JsonObject>

    @GET("getProductDetails")
    fun getProductDetail(@QueryMap hashMap: Map<String, String>): Call<JsonObject>

    @GET("customerCartList")
    fun getCartList(@QueryMap hashMap: Map<String, String>): Call<JsonObject>

    @FormUrlEncoded
    @POST("contactus")
    fun getContactUs(@FieldMap hashMap: Map<String, String>): Call<JsonObject>

    @FormUrlEncoded
    @POST("addToCart")
    fun getAddCart(@FieldMap hashMap: Map<String, String>): Call<JsonObject>

    @FormUrlEncoded
    @POST("loginWithOtp")
    fun getPhoneNo(@FieldMap hashMap: Map<String, String>): Call<JsonObject>

    @FormUrlEncoded
    @POST("loginWithOtp")
    fun getSms(@FieldMap hashMap: Map<String, String>): Call<JsonObject>

    @FormUrlEncoded
    @POST("placeOrder")
    fun getOrderPlace(@FieldMap hashMap: Map<String, String>): Call<JsonObject>

    @GET("customerOrders")
    fun getOrderList(@QueryMap hashMap: Map<String, String>): Call<JsonObject>

    @GET("customerSingleOrderDetails")
    fun getSingleOrderDetail(@QueryMap data: HashMap<String, String>?): Call<JsonObject>

    @GET("notificationlist")
    fun getNotification(@Query("user_id") userId: String): Call<JsonObject>

    @GET("readnotification")
    fun readNotification(@Query("notification_id") userId: String): Call<JsonObject>

    @POST("logoutDeliveryBoy")
    fun logout(@QueryMap data: HashMap<String, String>?): Call<JsonObject>

    @POST("repeatOrder")
    fun repeatOrder(@QueryMap data: HashMap<String, String>?): Call<JsonObject>

    @POST("checktime")
    fun checkTime(): Call<JsonObject>

    @POST("getstores")
    fun getStores(@QueryMap data: HashMap<String, String>?): Call<JsonObject>

    @POST("checklocation")
    fun checkLocStore(@QueryMap data: HashMap<String, String>?): Call<JsonObject>

    @POST("clearstorecart")
    fun clrCartStore(@QueryMap data: HashMap<String, String>?): Call<JsonObject>

    @GET("patient/notificationList")
    fun getNotification(): Call<GenericModel>

    @GET("patient/favouriteList")
    fun getFavouriteList(): Call<GenericModel>

    @GET("patient/myWallet")
    fun myWallet(): Call<GenericModel>

    @GET("patient/viewDoctorDetails/{id}")
    fun viewDoctorDetail(
        @Path("id") id: String
    ): Call<GenericModel1>

    @GET("patient/viewLaboratory/{id}")
    fun viewLaboratory(
        @Path("id") id: String
    ): Call<GenericModel>

}