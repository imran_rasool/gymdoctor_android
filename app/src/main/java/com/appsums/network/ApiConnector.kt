//package com.freshbasketuser.network
//
//import androidx.fragment.app.FragmentManager
//import com.google.gson.JsonObject
//
//import org.json.JSONObject
//
//class ApiConnector<T>(
////    private val mContext: ThrifterzActivity,
//    private val service: Single<JsonObject>,
//    private val pojoClassType: Class<T>,
//    private val showLoader: Boolean) {
//
//    internal fun initApi(mCallBack: onApiCallBack<T>) {
//        if (mContext.getInternetConnectivityStatus()) {
//            if (showLoader) {
//                mContext.showLoader()
//            }
//            mContext.getDisposable()!!.add(
//                service.subscribeOn(Schedulers.io())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribeWith(object : DisposableSingleObserver<JsonObject>() {
//                        override fun onSuccess(t: JsonObject) {
//                            mContext.hideLoader()
//                            if (t.size() != 0) {
//                                mCallBack.onSuccess(Gson().fromJson(t, pojoClassType))
//                            } else {
//                                mCallBack.onFailure(mContext.getString(R.string.internet_server_error_message))
//                            }
//                        }
//
//                        override fun onError(e: Throwable) {
//                            mContext.hideLoader()
//                            errorHandling(e)
//                        }
//                    })
//            )
//        } else {
//            mCallBack.onFailure("Please check your internet connectivity")
//        }
//    }
//
//    interface onApiCallBack<T> {
//        fun onSuccess(response: T)
//        fun onFailure(message: String)
//    }
//
//    fun errorHandling(e: Throwable) {
//        try {
//            val obj = JSONObject((e as HttpException).response().errorBody()!!.string())
//            val errorMessage = obj.optJSONObject("error").optString("message")
//            val errorCode = e.code()
//            when (errorCode) {
//
//                UN_AUTHORIZED -> {
//                    mContext.showErrorDialog(mContext.getString(R.string.session_expire_text),
//                        object : onMessageButtonClickDialog {
//                            override fun onYesClick(tag: String) {
//                                SharedPrefsManager.clearPrefs(mContext)
//                                mContext.replaceFragment(FragmentLogin(), true, true)
//                                mContext.supportFragmentManager.popBackStackImmediate(
//                                    null,
//                                    FragmentManager.POP_BACK_STACK_INCLUSIVE
//                                )
//                            }
//
//                            override fun onNoClick(tag: String) {
//
//                            }
//                        })
//                }
//                INTERNAL_SERVER_ERROR -> {
//                    mContext.showErrorDialog(mContext.getString(R.string.internet_server_error_message))
//                }
//                BAD_REQUEST -> {
//                    if (mContext.currentFragment is FragmentChangePhoneNumber) {
//                        if (errorMessage.contains("Entered mobile number already")) {
//                            (mContext.currentFragment as FragmentChangePhoneNumber).showAlreadyMessage()
//                        } else {
//                            mContext.showErrorDialog(mContext.getString(R.string.internet_server_error_message))
//                        }
//                    }else if (mContext.currentFragment is FragmentClosetItemDetail){
//                        mContext.showErrorDialog(errorMessage)
//                    } else {
//                        mContext.showErrorDialog(errorMessage)
//                    }
//                }
//                SUSSPENDED -> {
//                    mContext.showErrorDialog("Your account has been suspended please contact with \nadmin: admin@gmail.com")
//                }
//                else -> {
//                    mContext.showErrorDialog(mContext.getString(R.string.internet_server_error_message))
//                }
//            }
//        } catch (e: Exception) {
//            e.printStackTrace()
//            mContext.showErrorDialog(mContext.getString(R.string.internet_server_error_message))
//        }
//    }
//
//    val UN_AUTHORIZED = 401
//    val INTERNAL_SERVER_ERROR = 500
//    val BAD_REQUEST = 400
//    val SUSSPENDED = 406
//
//}