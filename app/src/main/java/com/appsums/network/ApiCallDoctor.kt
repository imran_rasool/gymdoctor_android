package com.appsums.network

import com.appsums.model.*
import com.appsums.utils.GlobalConstants
import com.appsums.utils.SharedPrefClass
import com.appsums.view.activities.HomeActivity
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.io.File

/**
 * Created by Indu Bala on 27/11/21.
 */
object ApiCallDoctor {


    fun couponsListApi(
        homeActivity: HomeActivity, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .dcouponsListApi()
            )
        }
    }


    fun subscriptionListApi(
        homeActivity: HomeActivity, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .dsubscriptionListApi()
            )
        }
    }

    fun profileApi(
        homeActivity: HomeActivity, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .profile()
            )
        }
    }

    fun homeAPi(
        homeActivity: HomeActivity, body: LatLngModel, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .homeApi(body)
            )
        }
    }

   /* fun nearByServiceProviderAPi(
        homeActivity: HomeActivity,
        body: LatLngSprciltyModel,
        callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .nearByServiceProvider(body)
            )
        }
    }*/

    fun laboratoryHomeDataAPi(
        homeActivity: HomeActivity, body: LatLngModel, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .laboratoryHomeDataApi(body)
            )
        }
    }

    fun serviceProviderTypeListAPi(
        homeActivity: HomeActivity, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .serviceProviderTypeList()
            )
        }
    }

    fun secondrySpecialtyList(
        homeActivity: HomeActivity, id: String, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .dsecondrySpecialtyList(id)
            )
        }
    }

    fun primarySpecialtyListApi(
        homeActivity: HomeActivity, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .primarySpecialtyListApi()
            )
        }
    }

    fun createProfileAPi(
        homeActivity: HomeActivity,
        body: ProfileDoctorModel,
        callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .dcreateProfileApi(body)
            )
        }
    }

    fun createProfessionalAPi(
        homeActivity: HomeActivity,
        body: ProfessionalDoctorModel,
        callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .dcreateProfessionalApi(body)
            )
        }
    }

    fun editProfileAPi(
        homeActivity: HomeActivity, body: EditProfileModel, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .editProfileApi(body)
            )
        }
    }

    fun buySubscriptionAPi(
        homeActivity: HomeActivity,
        body: BuySubscriptionModel,
        callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .dbuySubscriptionApi(body)
            )
        }
    }

    fun resendOtpAPi(
        homeActivity: HomeActivity,
        body: ResendOtpModel,
        callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .dresendOtpApi(body)
            )
        }
    }

    fun uploadImg(
        homeActivity: HomeActivity,
        imageFile: String,
        callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            val hashMap = HashMap<String, RequestBody>()
            val userImage = File(imageFile)
            val body = userImage!!.asRequestBody("image/*".toMediaTypeOrNull())
            hashMap["file\"; filename=\"" + userImage.name] = body


            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .imgUploadApi(hashMap)
            )
        }
    }

    fun createMedicalProfileAPi(
        homeActivity: HomeActivity, body: MedicalModel, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .createMedicalProfileApi(body)
            )
        }
    }

    fun loginAPi(
        homeActivity: HomeActivity, body: LoginModel, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .dlogin(body)
            )
        }
    }

    fun forgotPasswordAPi(
        homeActivity: HomeActivity,
        body: ForgotPasswordModel,
        callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .forgotPassword(body)
            )
        }
    }

    fun forgotPasswordAPi(
        homeActivity: HomeActivity,
        body: ForgotPasswordNoModel,
        callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .forgotPassword(body)
            )
        }
    }


    fun loginAPi(
        homeActivity: HomeActivity, body: LoginPhnModel, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .login(body)
            )
        }
    }


    fun signupAPi(
        homeActivity: HomeActivity, body: SignupModel, callback: ResponseListener<GenericModel>) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .dsignUp(body)
            )
        }
    }

    fun resetPasswordAPi(
        homeActivity: HomeActivity,
        body: ResetPasswordModel,
        callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .resetPassword(body)
            )
        }
    }


    fun otpAPi(
        homeActivity: HomeActivity, body: OtpModel, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        homeActivity.hideLoader()
                        checkCode(mResponse, callback, homeActivity)
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .dverifyOtp(body)
            )
        }
    }


    fun checkCode(
        mResponse: Response<GenericModel>,
        callback: ResponseListener<GenericModel>,
        homeActivity: HomeActivity
    ) {
        if (mResponse.code() == 200) {
            homeActivity.showToast(mResponse.body()?.responseMessage)
            callback.onSuccess(mResponse)
        } else {
            val jObjError = JSONObject(mResponse.errorBody()?.string())
            homeActivity.showToast(jObjError.getString("responseMessage"))

            if (jObjError.has("result")) {
                var jsonObject = jObjError.getJSONObject("result")
                if (jsonObject.has("isVerified"))
                    if (jsonObject.getBoolean("isVerified")) {
                        callback.onError(true)
                    } else {
                        if (jsonObject.has("_id"))
                            SharedPrefClass().putObject(
                                homeActivity,
                                GlobalConstants.USERID,
                                jsonObject.getString("_id")
                            )
                        callback.onError(false)
                    }

            }

        }
    }
}

