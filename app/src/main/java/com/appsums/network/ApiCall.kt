package com.appsums.network

import com.appsums.model.*
import com.appsums.utils.GlobalConstants
import com.appsums.utils.SharedPrefClass
import com.appsums.view.activities.HomeActivity
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.io.File

/**
 * Created by Indu Bala on 27/11/21.
 */
object ApiCall {


    fun couponsListApi(
        homeActivity: HomeActivity, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .couponsListApi()
            )
        }
    }


    fun subscriptionListApi(
        homeActivity: HomeActivity, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .subscriptionListApi()
            )
        }
    }

    fun profileApi(
        homeActivity: HomeActivity, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .profile()
            )
        }
    }

    fun homeAPi(
        homeActivity: HomeActivity, body: LatLngModel, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .homeApi(body)
            )
        }
    }

    fun nearByLaboratoryAPi(
        homeActivity: HomeActivity, body: LatLngModel, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .nearByLaboratoryApi(body)
            )
        }
    }

    fun nearByServiceProviderAPi(
        homeActivity: HomeActivity,
        body: LatLngSprciltyModel,
        callback: ResponseListener<GenericModel1>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel1>()
            mApiService.get(
                object : ApiResponse<GenericModel1> {
                    override fun onResponse(mResponse: Response<GenericModel1>) {
                        checkCode1(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel1>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .nearByServiceProvider(body)
            )
        }
    }

    fun favAPi(
        homeActivity: HomeActivity,
        body: FavouriteModal,
        callback: ResponseListener<GenericModel1>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel1>()
            mApiService.get(
                object : ApiResponse<GenericModel1> {
                    override fun onResponse(mResponse: Response<GenericModel1>) {
                        checkCode1(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel1>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .favStatusAPi(body)
            )
        }
    }

    fun laboratoryHomeDataAPi(
        homeActivity: HomeActivity, body: LatLngModel, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .laboratoryHomeDataApi(body)
            )
        }
    }

    fun serviceProviderTypeListAPi(
        homeActivity: HomeActivity, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .serviceProviderTypeList()
            )
        }
    }

    fun previousAppointmentsList(
        homeActivity: HomeActivity, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .previousAppointmentsList()
            )
        }
    }

    fun upcomingAppointmentsList(
        homeActivity: HomeActivity, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .upcomingAppointmentsList()
            )
        }
    }

    fun secondrySpecialtyList(
        homeActivity: HomeActivity, id: String, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .secondrySpecialtyList(id)
            )
        }
    }

    fun getSlot(
        homeActivity: HomeActivity,
        id: String,
        bookingDate: String,
        callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .getSlot(id, bookingDate)
            )
        }
    }


    fun viewAppointment(
        homeActivity: HomeActivity, id: String, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .viewAppointment(id)
            )
        }
    }

    fun createProfileAPi(
        homeActivity: HomeActivity, body: ProfileModel, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .createProfileApi(body)
            )
        }
    }

    fun editProfileAPi(
        homeActivity: HomeActivity, body: EditProfileModel, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .editProfileApi(body)
            )
        }
    }

    fun getTwillioTokenApi(
        homeActivity: HomeActivity, body: TwillioModel, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .callTwillioToken(body)
            )
        }
    }

    fun buySubscriptionAPi(
        homeActivity: HomeActivity,
        body: BuySubscriptionModel,
        callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .buySubscriptionApi(body)
            )
        }
    }

    fun resendOtpAPi(
        homeActivity: HomeActivity,
        body: ResendOtpModel,
        callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .resendOtpApi(body)
            )
        }
    }

    fun bookAPi(
        homeActivity: HomeActivity,
        body: BookModel,
        callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .bookAppointmentApi(body)
            )
        }
    }

    fun uploadImg(
        homeActivity: HomeActivity,
        imageFile: String,
        callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            val hashMap = HashMap<String, RequestBody>()
            val userImage = File(imageFile)
            val body = userImage!!.asRequestBody("image/*".toMediaTypeOrNull())
            hashMap["file\"; filename=\"" + userImage.name] = body


            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .imgUploadApi(hashMap)
            )
        }
    }

    fun createMedicalProfileAPi(
        homeActivity: HomeActivity, body: MedicalModel, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .createMedicalProfileApi(body)
            )
        }
    }

    fun loginAPi(
        homeActivity: HomeActivity, body: LoginModel, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .login(body)
            )
        }
    }

    fun forgotPasswordAPi(
        homeActivity: HomeActivity,
        body: ForgotPasswordModel,
        callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .forgotPassword(body)
            )
        }
    }

    fun forgotPasswordAPi(
        homeActivity: HomeActivity,
        body: ForgotPasswordNoModel,
        callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .forgotPassword(body)
            )
        }
    }


    fun loginAPi(
        homeActivity: HomeActivity, body: LoginPhnModel, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .login(body)
            )
        }
    }


    fun signupAPi(
        homeActivity: HomeActivity, body: SignupModel, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .signUp(body)
            )
        }
    }

    fun resetPasswordAPi(
        homeActivity: HomeActivity,
        body: ResetPasswordModel,
        callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface().resetPassword(body)
            )
        }
    }


    fun otpAPi(
        homeActivity: HomeActivity, body: OtpModel, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        homeActivity.hideLoader()
                        checkCode(mResponse, callback, homeActivity)
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .verifyOtp(body)
            )
        }
    }

    fun getNotification(
        homeActivity: HomeActivity, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface().getNotification()
            )
        }
    }


    fun getFavouriteList(
        homeActivity: HomeActivity, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface().getFavouriteList()
            )
        }
    }

    fun myWalletAmount(
        homeActivity: HomeActivity, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface().myWallet()
            )
        }
    }

    fun viewDoctorDetail(
        homeActivity: HomeActivity, id: String, callback: ResponseListener<GenericModel1>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel1>()
            mApiService.get(
                object : ApiResponse<GenericModel1> {
                    override fun onResponse(mResponse: Response<GenericModel1>) {
                        checkCode1(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel1>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .viewDoctorDetail(id)
            )
        }
    }

    fun viewLaboratory(
        homeActivity: HomeActivity, id: String, callback: ResponseListener<GenericModel>
    ) {
        if (homeActivity.isOnline(homeActivity)) {
            homeActivity.showLoader()
            val mApiService = ApiService<GenericModel>()
            mApiService.get(
                object : ApiResponse<GenericModel> {
                    override fun onResponse(mResponse: Response<GenericModel>) {
                        checkCode(mResponse, callback, homeActivity)
                        homeActivity.hideLoader()
                    }

                    override fun onError(mKey: Call<GenericModel>?, t: Throwable?, msg: String) {
                        callback.onError(msg)
                        homeActivity.hideLoader()
                    }
                },
                ApiClient.getApiInterface()
                    .viewLaboratory(id)
            )
        }
    }

    fun checkCode(
        mResponse: Response<GenericModel>,
        callback: ResponseListener<GenericModel>,
        homeActivity: HomeActivity
    ) {
        if (mResponse.code() == 200) {
            // homeActivity.showToast(mResponse.body()?.responseMessage)
            callback.onSuccess(mResponse)
        } else if (mResponse.code() == 401) {
            homeActivity.showToast(mResponse.body()?.responseMessage)
            homeActivity.logout()
        } else {
            val jObjError = JSONObject(mResponse.errorBody()?.string())
            homeActivity.showToast(jObjError.getString("responseMessage"))

            if (jObjError.has("result")) {
                val jsonObject = jObjError.getJSONObject("result")

                if (jsonObject.has("isVerified"))
                    if (jsonObject.getBoolean("isVerified")) {
                        callback.onError(true)
                    } else {
                        if (jsonObject.has("_id"))
                            SharedPrefClass().putObject(
                                homeActivity,
                                GlobalConstants.USERID,
                                jsonObject.getString("_id")
                            )
                        callback.onError(false)
                    }

            }

        }

    }

    fun checkCode1(
        mResponse: Response<GenericModel1>,
        callback: ResponseListener<GenericModel1>,
        homeActivity: HomeActivity
    ) {
        if (mResponse.code() == 200) {
            //  homeActivity.showToast(mResponse.body()?.responseMessage)
            callback.onSuccess(mResponse)
        } else {
            val jObjError = JSONObject(mResponse.errorBody()?.string())
            homeActivity.showToast(jObjError.getString("responseMessage"))

            if (jObjError.has("result")) {
                val jsonObject = jObjError.getJSONObject("result")
                if (jsonObject.has("isVerified"))
                    if (jsonObject.getBoolean("isVerified")) {
                        callback.onError(true)
                    } else {
                        if (jsonObject.has("_id"))
                            SharedPrefClass().putObject(
                                homeActivity,
                                GlobalConstants.USERID,
                                jsonObject.getString("_id")
                            )
                        callback.onError(false)
                    }

            }

        }
    }

}

interface ResponseListener<T> {
    fun onSuccess(mResponse: Response<T>)
    fun onError(msg: String)
    fun onError(msg: Boolean) {
    }
}

