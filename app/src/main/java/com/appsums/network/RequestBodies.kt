package com.appsums.network

object RequestBodies {

    data class LoginBody(
        val email:String,
        val password:String
    )

    data class AllCategoriesBody(
        val language_id:String
    )
}