package com.appsums

import android.os.Bundle
import android.view.View
import com.appsums.databinding.FragmentAddMedicalRecordBinding
import com.appsums.model.ToolBarModel
import com.appsums.view.base.BaseFragment


class AddMedicalRecordFragment :
    BaseFragment<FragmentAddMedicalRecordBinding>(FragmentAddMedicalRecordBinding::inflate) {

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.toolBarTtl = getString(R.string.ADD_MEDICAL_RECORDS)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {


        }

    }


}