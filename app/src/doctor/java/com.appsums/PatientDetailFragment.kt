package com.appsums

import android.os.Bundle
import android.view.View
import com.appsums.databinding.FragmentPatientDetailDoctorBinding
import com.appsums.model.ParamModel
import com.appsums.model.ToolBarModel
import com.appsums.view.base.BaseFragment
import com.appsums.view.base.CustomDialog
import com.appsums.view.fragment.ChatFragment
import com.appsums.view.fragment.SelectAppointmentFragment


class PatientDetailFragment(val tabPos: Int) :
    BaseFragment<FragmentPatientDetailDoctorBinding>(FragmentPatientDetailDoctorBinding::inflate) {

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.toolBarTtl = getString(R.string.PATIENT_DETAILS)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            if (tabPos == 1) {

                llBtn.visibility = View.GONE
                llBtn3.visibility = View.VISIBLE
            } else {
                llBtn.visibility = View.VISIBLE
                llBtn3.visibility = View.GONE

            }

            btnReshedule.setOnClickListener {
           //     displayItAddStack(SelectAppointmentFragment())
            }

            btnViewAppDetail.setOnClickListener {
                displayItAddStack(AppointmentViewDetailFragment())
            }
            btnCall.setOnClickListener {
                displayItAddStack(ChatFragment())
            }

            btnCancel.setOnClickListener {
                CustomDialog(homeActivity!!).openCancelAppDialog(object :
                    CustomDialog.DialogListener {
                    override fun positiveBtn() {
                        reasonDialog()
                    }

                    override fun positiveBtn(paramModel: ParamModel) {

                    }

                })
            }

        }

    }


    fun reasonDialog() {
        CustomDialog(homeActivity!!).openCancelReasonAppDialog(object :
            CustomDialog.DialogListener {
            override fun positiveBtn() {

            }

            override fun positiveBtn(paramModel: ParamModel) {

            }

        })
    }


}