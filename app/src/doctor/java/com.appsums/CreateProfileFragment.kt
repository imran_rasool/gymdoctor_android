package com.appsums

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.RadioButton
import com.appsums.databinding.FragmentCraeteProfileDoctorBinding
import com.appsums.databinding.SpinnerItemBinding
import com.appsums.model.*
import com.appsums.network.ApiCall
import com.appsums.network.ApiCallDoctor
import com.appsums.network.ResponseListener
import com.appsums.utils.CheckPermission
import com.appsums.utils.GlobalConstants
import com.appsums.utils.SharedPrefClass
import com.appsums.utils.TakePictureUtils
import com.appsums.utils.cropimage.CropImage
import com.appsums.view.adapters.ArraySpinnerAdapter
import com.appsums.view.base.BaseFragment
import retrofit2.Response
import java.io.*

class CreateProfileFragment(val from: Int, val model: UserData?=null) :
    BaseFragment<FragmentCraeteProfileDoctorBinding>(FragmentCraeteProfileDoctorBinding::inflate) {

    private val tempImage = "tempImage"
    private var file: File? = null
    private var path: String? = null
    private var url: String = ""

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            if (model != null) {
                etFn.setText(model.firstName)
                etDesc.setText(model.bio)
                etaddress.setText(model?.location?.address)
                url = model.profilePic
                setImageWithUrl(url, imgProfileAvatar)
                if (model.gender == "Female") {
                    rBMale.isChecked = false
                    rBFeMale.isChecked = true
                } else {
                    rBMale.isChecked = true
                    rBFeMale.isChecked = false
                }


            }


            imgProfileAvatar.setOnClickListener {
                checkPermission()
            }


            btnCall.setOnClickListener {
                val radioButtonID: Int = rbGrp.checkedRadioButtonId
                val radioButton: View = rbGrp.findViewById(radioButtonID)
                val idx: Int = rbGrp.indexOfChild(radioButton)
                val r: RadioButton = rbGrp.getChildAt(idx) as RadioButton

                if (isValidate()) {
                    if (model != null) {
//                        callAPi(
//                            EditProfileModel(
//                                etFn.text.toString(),
//                                r.text.toString(),
//                                etaddress.text.toString(),
//                                etDesc.text.toString(),
//                                url,
//                                model.location.coordinates[0],
//                                model.location.coordinates[1],
////                                (SharedPrefClass().getPrefValue(
////                                    requireContext(),
////                                    GlobalConstants.LATTITUDE
////                                ) ?: 28.536288450553798) as Double,
////                                (SharedPrefClass().getPrefValue(
////                                    requireContext(),
////                                    GlobalConstants.LONGITUDE
////                                ) ?: 77.38301406357039) as Double,
//                                model.height,
//                                model.weight,
//                                model.bloodGroup,
//                                model.diabetesDetails,
//                                model.allergyDeatils,
//                                model.familyNotes,
//                                model.medicalFile,
//                                model.iSDiabetes
//                                //28.536288450553798, 77.38301406357039
//                            )
//                        )
                    } else {
                        callAPi(
                            ProfileDoctorModel(
                                etFn.text.toString(),
                                r.text.toString(),
                                etaddress.text.toString(),
                                etDesc.text.toString(), url,
                                (SharedPrefClass().getPrefValue(
                                    requireContext(),
                                    GlobalConstants.LATTITUDE
                                ) ?: 28.536288450553798).toString().toDouble() ,
                                (SharedPrefClass().getPrefValue(
                                    requireContext(),
                                    GlobalConstants.LONGITUDE
                                ) ?: 77.38301406357039) .toString().toDouble()
                                //28.536288450553798, 77.38301406357039
                            )
                        )
                    }
                }
            }

        }
        setSpinner()

    }


    fun isValidate(): Boolean {
        viewDataBinding?.apply {
            if (etFn.text.isNullOrEmpty()) {
                showToast(getString(R.string.Enter_Full_Name_))
                return false
            }else if (etaddress.text.isNullOrEmpty()) {
                showToast(getString(R.string.et_Address))
                return false
            } else if (etDesc.text.isNullOrEmpty()) {
                showToast(getString(R.string.et_desc))
                return false
            } else {
                return true
            }
        }
        return true
    }

    fun callAPi(body: ProfileDoctorModel) {
        homeActivity?.let {
            ApiCallDoctor.createProfileAPi(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    if (from == 1) {
                        popBack()
                    } else {
                        displayItNoStack(SubscriptionFragment(from))

                    }
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    fun callAPi(body: EditProfileModel) {
        homeActivity?.let {
            ApiCall.editProfileAPi(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    popBack()
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    fun callAPi(body: String) {
        homeActivity?.let {
            ApiCallDoctor.uploadImg(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    url = mResponse.body()?.result?.url ?: ""

                }

                override fun onError(msg: String) {
                    Log.d("Dddd==", msg.toString())
                }
            })
        }
    }

    fun setSpinner() {

        val list = ArrayList<String>()
        list.add("Single")
        list.add("Married")
        list.add("Divorce")

        val arrayAdapter = ArraySpinnerAdapter(
            requireActivity(),
            list,
            object : ArraySpinnerAdapter.CustomSpinnerData<SpinnerItemBinding, String> {
                override fun setDataSpinner(
                    binder: SpinnerItemBinding?,
                    position: Int,
                    model: String
                ) {

                    binder?.textSpinner?.text = model

                }

            }
        )


    }

    private fun checkPermission() {
        if (CheckPermission.checkIsMarshMallowVersion()) {
            if (CheckPermission.checkCameraStoragePermission(requireContext())) {
                showImageDailoge(requireActivity(), tempImage, false)
            } else {
                CheckPermission.requestCameraStoragePermission(requireActivity())
            }
        } else {
            showImageDailoge(requireActivity(), tempImage, false)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            CheckPermission.REQUEST_CODE_CAMERA_STORAGE_PERMISSION -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showImageDailoge(requireActivity(), tempImage, false)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (resultCode == Activity.RESULT_OK) when (requestCode) {
            TakePictureUtils.TAKE_PICTURE -> TakePictureUtils.startCropImage(
                context as Activity?,
                tempImage + ".jpg",
                500,
                500
            )
            TakePictureUtils.PICK_GALLERY -> {
                try {
                    val inputStream: InputStream? =
                        homeActivity!!.getContentResolver().openInputStream(
                            data!!.data!!
                        )
                    val fileOutputStream: FileOutputStream =
                        FileOutputStream(
                            File(
                                requireContext().getExternalFilesDir("temp"),
                                tempImage + ".jpg"
                            )
                        )
                    TakePictureUtils.copyStream(inputStream, fileOutputStream)
                    fileOutputStream.close()
                    inputStream?.close()
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                TakePictureUtils.startCropImage(context as Activity?, tempImage + ".jpg", 500, 500)
            }
            TakePictureUtils.CROP_FROM_CAMERA -> {
                path = null
                if (data != null) {
                    path = data.getStringExtra(CropImage.IMAGE_PATH)
                    if (path != null) file = File(path)
                    //    viewModel.callApiFileToUrl(context, file)
                    if (path == null) {
                        return
                    }
                    callAPi(path!!)
                    setImageWithUri(file, viewDataBinding?.imgProfileAvatar)
//                    Glide.with(context!!)
//                        .load(file)
//                        .placeholder(R.drawable.dummy)
//                        .into<Target<Drawable>>(viewDataBinding)
                }
            }
        }
    }

}
