package com.appsums

import android.os.Bundle
import android.view.View
import com.appsums.databinding.FragmentSignupBinding
import com.appsums.model.GenericModel
import com.appsums.model.SignupModel
import com.appsums.model.ToolBarModel
import com.appsums.network.ApiCallDoctor
import com.appsums.network.ResponseListener
import com.appsums.utils.GlobalConstants
import com.appsums.utils.SharedPrefClass
import com.appsums.utils.UtilsFunctions
import com.appsums.view.base.BaseFragment
import retrofit2.Response


class SignUpFragment : BaseFragment<FragmentSignupBinding>(FragmentSignupBinding::inflate) {

    var isPasswordVisible = false
    var isPasswordVisible2 = false


    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            imgBackBtn.setOnClickListener {
                popBack()
            }
            btnCall.setOnClickListener {
                if (isValidate())
                    callAPi(SignupModel(UtilsFunctions.deviceType, SharedPrefClass().getPrefValue(requireContext(),
                        GlobalConstants.DEVICE_ID).toString(), etEmail.text.toString(), "+1",
                            etPhnNo.text.toString(),
                            etPassword.text.toString()
                        )
                    )
            }

            tvSignUp.setOnClickListener {
                popBack()
            }

        }

    }


    fun isValidate(): Boolean {
        viewDataBinding?.apply {
            if (etEmail.text.isNullOrEmpty()) {
                showToast(getString(R.string.valid_enter_email))
                return false
            } else if (!UtilsFunctions.isValidEmail(etEmail.text.toString())) {
                showToast(getString(R.string.valid_email))
                return false
            } else if (etPhnNo.text.isNullOrEmpty()) {
                showToast(getString(R.string.et_Mobile_Number))
                return false
            } else if (etPassword.text.isNullOrEmpty()) {
                showToast(getString(R.string.enter_password))
                return false
            } else if (etConfirmPassword.text.isNullOrEmpty()) {
                showToast(getString(R.string.enter_confirm_password))
                return false
            } else if (!etPassword.text.toString()
                    .equals(etConfirmPassword.text.toString(), ignoreCase = false)
            ) {
                showToast(getString(R.string.pass_does_not_match))
                return false
            } else {
                return true
            }
        }
        return true
    }

    fun callAPi(body: SignupModel) {
        homeActivity?.let {
            ApiCallDoctor.signupAPi(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    SharedPrefClass().putObject(
                        it,
                        GlobalConstants.ACCESS_TOKEN,
                        mResponse.body()?.userData?.token
                    )
                    SharedPrefClass().putObject(
                        it,
                        GlobalConstants.USERID,
                        mResponse.body()?.userData?._id
                    )
                    displayItAddStack(OtpFragment(0))
                }

                override fun onError(msg: String) {

                }
            })
        }
    }



}