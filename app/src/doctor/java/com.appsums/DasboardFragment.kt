package com.appsums

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.appsums.databinding.FragmentDashboardDoctorBinding
import com.appsums.databinding.ItemDasboardDoctorBinding
import com.appsums.databinding.ItemRecommendBinding
import com.appsums.databinding.ItemUpcomingAppointmentBinding
import com.appsums.model.ToolBarModel
import com.appsums.view.adapters.RecyclerCallback
import com.appsums.view.adapters.RecyclerViewGenricAdapter
import com.appsums.view.base.BaseFragment
import com.appsums.view.fragment.ChatFragment


class DasboardFragment :
    BaseFragment<FragmentDashboardDoctorBinding>(FragmentDashboardDoctorBinding::inflate) {

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.sideNav = true
        toolBarModel.notiIconShow = true
        toolBarModel.toolBarProfileShow = true
        toolBarModel.toolBarTtl = getString(R.string.DASHBOARD)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {


            recyclerView.layoutManager = GridLayoutManager(context, 2)
            recyclerView1.layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            recyclerView2.layoutManager =
                LinearLayoutManager(requireContext())

            setAdapter1(recyclerView)
            setAdapter(recyclerView1)
            setAdapter2(recyclerView2)
        }

    }

    fun setAdapter1(recyclerView: RecyclerView) {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")

        val rvAdapProgress = RecyclerViewGenricAdapter<String, ItemDasboardDoctorBinding>(
            list,
            R.layout.item_dasboard_doctor, object :
                RecyclerCallback<ItemDasboardDoctorBinding, String> {
                override fun bindData(
                    binder: ItemDasboardDoctorBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                    }
                }
            })
        recyclerView.adapter = rvAdapProgress
        recyclerView.isNestedScrollingEnabled = false

    }


    fun setAdapter(recyclerView: RecyclerView) {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")

        val rvAdapProgress = RecyclerViewGenricAdapter<String, ItemRecommendBinding>(
            list,
            R.layout.item_recommend, object :
                RecyclerCallback<ItemRecommendBinding, String> {
                override fun bindData(
                    binder: ItemRecommendBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {


                        btnCall.setOnClickListener {
                            displayItAddStack(ChatFragment())
                        }
                    }
                }
            })
        recyclerView.adapter = rvAdapProgress
        recyclerView.isNestedScrollingEnabled = false

    }


    fun setAdapter2(recyclerView: RecyclerView) {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")

        val rvAdapProgress = RecyclerViewGenricAdapter<String, ItemUpcomingAppointmentBinding>(
            list,
            R.layout.item_upcoming_appointment, object :
                RecyclerCallback<ItemUpcomingAppointmentBinding, String> {
                override fun bindData(
                    binder: ItemUpcomingAppointmentBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {
                        linearMain.setOnClickListener {
                            displayItAddStack(PatientDetailFragment(0))
                        }
                    }

                }
            })
        recyclerView.adapter = rvAdapProgress
        recyclerView.isNestedScrollingEnabled = false

    }

}