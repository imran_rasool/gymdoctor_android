package com.appsums

import android.os.Bundle
import android.view.View
import com.appsums.databinding.FragmentLoginBinding
import com.appsums.model.GenericModel
import com.appsums.model.LoginModel
import com.appsums.model.ToolBarModel
import com.appsums.network.ApiCallDoctor
import com.appsums.network.ResponseListener
import com.appsums.utils.GlobalConstants
import com.appsums.utils.SharedPrefClass
import com.appsums.utils.UtilsFunctions
import com.appsums.view.base.BaseFragment
import com.appsums.view.fragment.ClientInfoFragment
import com.appsums.view.fragment.CreateMedicalFragment
import retrofit2.Response


class LoginFragment : BaseFragment<FragmentLoginBinding>(FragmentLoginBinding::inflate) {

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            tvSignUp.setOnClickListener {
                displayItAddStack(SignUpFragment())
            }

            btnPhn.setOnClickListener {
                displayItAddStack(LoginPhnNoFragment())
            }

            btnlogin.setOnClickListener {
                if (isValidate()) {
                    callAPi(
                        LoginModel(
                            UtilsFunctions.deviceType,
                            SharedPrefClass().getPrefValue(
                                requireContext(),
                                GlobalConstants.DEVICE_ID
                            )
                                .toString(),
                            etEmail.text.toString(),
                            etPassword.text.toString(),
                            440
                        )
                    )
                }
            }

            underlineText(tvForgotPass)

            tvForgotPass.setOnClickListener {
                displayItAddStack(ForgotPassFragment())
            }


        }

    }


    fun callAPi(body: LoginModel) {
        homeActivity?.let {
            ApiCallDoctor.loginAPi(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    var model = mResponse.body()?.result
                    SharedPrefClass().putObject(
                        it,
                        GlobalConstants.ACCESS_TOKEN,
                        mResponse.body()?.result?.token
                    )
                    SharedPrefClass().putObject(
                        it,
                        GlobalConstants.EMAIL,
                        mResponse.body()?.result?.email.toString()
                    )
                    SharedPrefClass().putObject(
                        it,
                        GlobalConstants.NAME,
                        mResponse.body()?.result?.firstName.toString()
                    )
                    SharedPrefClass().putObject(
                        it,
                        GlobalConstants.PROFESSIONAL_STATUS,
                        mResponse.body()?.result?.isProfessional_profile_complete
                    )
                    SharedPrefClass().putObject(
                        it,
                        GlobalConstants.PROFILE_STATUS,
                        mResponse.body()?.result?.isPersonal_profile_complete
                    )
                    SharedPrefClass().putObject(
                        it,
                        GlobalConstants.PIC,
                        mResponse.body()?.result?.profilePic.toString()
                    )

                    SharedPrefClass().putObject(
                        it,
                        GlobalConstants.USERID,
                        mResponse.body()?.result?._id
                    )
                    SharedPrefClass().putObject(
                        it,
                        GlobalConstants.MEDICAL_STATUS,
                        true
                    )
                    homeActivity?.clearAllStack()
                    if (homeActivity?.getCurrentLocationName() == true) {
                        if (model?.isPersonal_profile_complete == false) {

                            displayItNoStack(CreateProfileFragment(0, null))
                        } else if (model?.isSubscribe == false)
                            displayItNoStack(SubscriptionFragment(0))
                        else if (model?.isProfessional_profile_complete == false)
                            displayItNoStack(ClientInfoFragment(0))
                        else
                            displayItNoStack(DasboardFragment())
                    }

                    homeActivity?.updateNavHeader()


                }

                override fun onError(msg: String) {

                }
            })
        }
    }


    fun isValidate(): Boolean {
        viewDataBinding?.apply {
            if (etEmail.text.isNullOrEmpty()) {
                showToast(getString(R.string.valid_enter_email))
                return false
            } else if (!UtilsFunctions.isValidEmail(etEmail.text.toString())) {
                showToast(getString(R.string.valid_email))
                return false
            } else if (etPassword.text.isNullOrEmpty()) {
                showToast(getString(R.string.enter_password))
                return false
            } else {
                return true
            }
        }
        return true
    }


}