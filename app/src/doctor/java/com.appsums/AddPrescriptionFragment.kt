package com.appsums

import android.os.Bundle
import android.view.View
import com.appsums.databinding.FragmentAddPrescriptionBinding
import com.appsums.model.ToolBarModel
import com.appsums.view.base.BaseFragment


class AddPrescriptionFragment :
    BaseFragment<FragmentAddPrescriptionBinding>(FragmentAddPrescriptionBinding::inflate) {

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.toolBarTtl = getString(R.string.ADD_PRESCRIPTION)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            underlineText(tvAddSign)


        }

    }


}