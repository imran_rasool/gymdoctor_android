package com.appsums

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.appsums.databinding.FragmentAppointmentBinding
import com.appsums.databinding.ItemAppointmentDoctorBinding
import com.appsums.model.ParamModel
import com.appsums.model.ToolBarModel
import com.appsums.view.adapters.RecyclerCallback
import com.appsums.view.adapters.RecyclerViewGenricAdapter
import com.appsums.view.base.BaseFragment
import com.appsums.view.base.CustomDialog
import com.google.android.material.tabs.TabLayout


class AppointmentFragment :
    BaseFragment<FragmentAppointmentBinding>(FragmentAppointmentBinding::inflate) {

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.notiIconShow = true
        toolBarModel.toolBarProfileShow = true
        toolBarModel.toolBarTtl = getString(R.string.MY_APPOINTMENT)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            recyclerView.layoutManager = LinearLayoutManager(requireContext())
            setAdapter(selectPosition)

            if (tabLayout.tabCount <= 0) {
                tabLayout.addTab(tabLayout.newTab().setText(R.string.Upcoming_Appointments))
                tabLayout.addTab(tabLayout.newTab().setText(R.string.Previous_Appointments))
            }


            tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab?) {
                    selectPosition = tab?.position ?: 0


                    setAdapter(selectPosition)


                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {

                }

                override fun onTabReselected(tab: TabLayout.Tab?) {

                }
            })
        }
    }


    fun setAdapter(selectPosition: Int) {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        val rvAdapProgress = RecyclerViewGenricAdapter<String, ItemAppointmentDoctorBinding>(
            list,
            R.layout.item_appointment_doctor, object :
                RecyclerCallback<ItemAppointmentDoctorBinding, String> {
                override fun bindData(
                    binder: ItemAppointmentDoctorBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                        if (selectPosition == 1) {
                            tvCancelled.visibility = View.VISIBLE
                            btnReshedule.visibility = View.GONE
                            btnCancelApp.visibility = View.GONE

                        } else {
                            btnReshedule.visibility = View.VISIBLE
                            btnCancelApp.visibility = View.VISIBLE
                            tvCancelled.visibility = View.GONE
                        }

                        btnCancelApp.setOnClickListener {


                            CustomDialog(homeActivity!!).openCancelAppDialog(object :
                                CustomDialog.DialogListener {
                                override fun positiveBtn() {

                                }

                                override fun positiveBtn(paramModel: ParamModel) {

                                }

                            })

                        }

                        llMain.setOnClickListener {
                            displayItAddStack(PatientDetailFragment(selectPosition))
                        }

                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }


}