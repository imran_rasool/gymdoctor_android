package com.appsums

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.appsums.databinding.FragmentPaymentDetailBinding
import com.appsums.databinding.ItemCardBinding
import com.appsums.model.ParamModel
import com.appsums.model.ToolBarModel
import com.appsums.view.adapters.RecyclerCallback
import com.appsums.view.adapters.RecyclerViewGenricAdapter
import com.appsums.view.base.BaseFragment
import com.appsums.view.base.CustomDialog
import com.appsums.view.fragment.ClientInfoFragment


class PaymentDetailsFragment(val from: Int) :
    BaseFragment<FragmentPaymentDetailBinding>(FragmentPaymentDetailBinding::inflate) {

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.toolBarTtl = getString(R.string.Payment_Details)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            recyclerView.layoutManager = LinearLayoutManager(requireContext())
            setAdapter1()
            btnCall.setOnClickListener {
                displayItAddStack(ClientInfoFragment(0))
            }
        }

    }

    fun setAdapter1() {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        val rvAdapProgress = RecyclerViewGenricAdapter(
            list,
            R.layout.item_card, object :
                RecyclerCallback<ItemCardBinding, String> {
                override fun bindData(
                    binder: ItemCardBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                        imgDelCard.setOnClickListener {
                            CustomDialog(homeActivity!!).openRemoveCardDialog(object :
                                CustomDialog.DialogListener {
                                override fun positiveBtn() {

                                }

                                override fun positiveBtn(paramModel: ParamModel) {

                                }

                            })
                        }
                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }

}