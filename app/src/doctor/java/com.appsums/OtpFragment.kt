package com.appsums

import android.os.Bundle
import android.view.View
import com.appsums.databinding.FragmentOtpBinding
import com.appsums.model.GenericModel
import com.appsums.model.OtpModel
import com.appsums.model.ResendOtpModel
import com.appsums.model.ToolBarModel
import com.appsums.network.ApiCallDoctor
import com.appsums.network.ResponseListener
import com.appsums.utils.GlobalConstants
import com.appsums.utils.SharedPrefClass
import com.appsums.view.base.BaseFragment
import retrofit2.Response


class OtpFragment(val from: Int) :
    BaseFragment<FragmentOtpBinding>(FragmentOtpBinding::inflate) {

    var otp = ""


    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        viewDataBinding?.apply {
            imgBackBtn.setOnClickListener {
                popBack()
            }

            otpView.setOtpCompletionListener {
                otp = it
            }

            tvResend.setOnClickListener {
                callAPi(
                    ResendOtpModel(
                        SharedPrefClass().getPrefValue(
                            requireContext(),
                            GlobalConstants.USERID
                        ).toString()
                    )
                )
            }

            btnCall.setOnClickListener {
                if (otp.length == 4)
                    callAPi(
                        OtpModel(
                            SharedPrefClass().getPrefValue(
                                requireContext(),
                                GlobalConstants.USERID
                            ).toString(), otp
                        )
                    ) else {
                    showToast("Please enter otp")
                }
            }

        }

    }

    fun callAPi(body: OtpModel) {
        homeActivity?.let {
            ApiCallDoctor.otpAPi(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {

                    homeActivity?.clearAllStack()
                    if (from == 1) {
                        displayItAddStack(ResetPassFragment())
                    } else {
                        if (homeActivity?.getCurrentLocationName() == true)
                            displayItNoStack(CreateProfileFragment(0, null))
                    }
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    fun callAPi(body: ResendOtpModel) {
        homeActivity?.let {
            ApiCallDoctor.resendOtpAPi(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {

                }

                override fun onError(msg: String) {

                }
            })
        }
    }

}