package com.appsums

import android.os.Bundle
import android.view.View
import com.appsums.databinding.FragmentSetttingsBinding
import com.appsums.model.ToolBarModel
import com.appsums.view.base.BaseFragment
import com.appsums.view.fragment.ChangePasswordFragment
import com.appsums.view.fragment.ClientInfoFragment
import com.appsums.view.fragment.InviteFrdFragment


class SettingsFragment : BaseFragment<FragmentSetttingsBinding>(FragmentSetttingsBinding::inflate) {

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.notiIconShow = true
        toolBarModel.toolBarProfileShow = true
        toolBarModel.toolBarTtl = getString(R.string.Settings)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            ll1.visibility=View.GONE
            ll2.visibility=View.GONE
            ll3.visibility=View.VISIBLE

            tvChangePassword.setOnClickListener {
                displayItAddStack(ChangePasswordFragment())
            }

            tvPersonalDetail.setOnClickListener {
                displayItAddStack(CreateProfileFragment(1, null))
            }

            tvProfessionalDetail.setOnClickListener {
                displayItAddStack(ClientInfoFragment(1))
            }

            tvPaymentInfo.setOnClickListener {
                displayItAddStack(PaymentDetailsFragment(0))
            }

            tvInviteFrd.setOnClickListener {
                displayItAddStack(InviteFrdFragment())
            }

        }

    }


}