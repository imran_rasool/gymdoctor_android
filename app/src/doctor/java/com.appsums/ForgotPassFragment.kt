package com.appsums

import android.os.Bundle
import android.view.View
import com.appsums.databinding.FragmentForgotPassBinding
import com.appsums.model.ToolBarModel
import com.appsums.view.base.BaseFragment


class ForgotPassFragment :
    BaseFragment<FragmentForgotPassBinding>(FragmentForgotPassBinding::inflate) {

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            imgBackBtn.setOnClickListener {
                popBack()
            }

            btnCall.setOnClickListener {
                displayItAddStack(OtpFragment(0))
            }

        }

    }


}