package com.appsums

import android.os.Bundle
import android.view.View
import com.appsums.model.ToolBarModel
import com.appsums.view.base.BaseFragment
import com.appsums.databinding.FragmentLoginBinding
import com.appsums.databinding.FragmentResetPassBinding
import com.appsums.databinding.FragmentSignupBinding
import com.appsums.view.fragment.InitialFragment


class ResetPassFragment :
    BaseFragment<FragmentResetPassBinding>(FragmentResetPassBinding::inflate) {

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            btnCall.setOnClickListener {
                displayItAddStack(InitialFragment())
            }


        }

    }


}