package com.appsums

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.appsums.AddCardFragment
import com.appsums.R
import com.appsums.databinding.FragmentApplyCouponBinding
import com.appsums.databinding.ItemCouponBinding
import com.appsums.model.GenericModel
import com.appsums.model.ParamModel
import com.appsums.model.SubscriptionModel
import com.appsums.model.ToolBarModel
import com.appsums.network.ApiCall
import com.appsums.network.ApiCallDoctor
import com.appsums.network.ResponseListener
import com.appsums.view.adapters.RecyclerCallback
import com.appsums.view.adapters.RecyclerViewGenricAdapter
import com.appsums.view.base.BaseFragment
import com.appsums.view.base.CustomDialog
import retrofit2.Response

class AppluCOuponFragment(val from: Int, val subscriptionModel: SubscriptionModel) :
    BaseFragment<FragmentApplyCouponBinding>(FragmentApplyCouponBinding::inflate) {

    private var rvAdapProgress: RecyclerViewGenricAdapter<SubscriptionModel, ItemCouponBinding>? =
        null
    val list = ArrayList<SubscriptionModel>()

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.toolBarTtl = getString(R.string.Apply_Coupon)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        selectPosition = -1
        viewDataBinding?.apply {

            recyclerView.layoutManager = LinearLayoutManager(requireContext())
            setAdapter1()
            callAPi()
            btnCall.setOnClickListener {
                if (selectPosition >= 0) {
                    CustomDialog(homeActivity!!).docOpenSubscriptionDialog(subscriptionModel,
                        object :
                            CustomDialog.DialogListener {
                            override fun positiveBtn() {
//                                if (from == 1) {
//                                    popBack()
//                                } else {
                                displayItNoStack(
                                    AddCardFragment(
                                        from,
                                        list[selectPosition]._id,
                                        subscriptionModel._id,
                                        subscriptionModel.price
                                    )
                                )
                                //  }
                            }

                            override fun positiveBtn(paramModel: ParamModel) {
                            }
                        })

                } else {
                    showToast("Please select one coupon")
                }

            }

            btnCancel.setOnClickListener {
                popBack()
            }
        }

    }

    fun callAPi() {
        homeActivity?.let {
            ApiCallDoctor.couponsListApi(it, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    list.clear()
                    mResponse.body()?.documentList?.let { it1 -> list.addAll(it1) }
                    rvAdapProgress?.notifyDataSetChanged()

                }

                override fun onError(msg: String) {

                }
            })
        }
    }


    fun setAdapter1() {
        rvAdapProgress = RecyclerViewGenricAdapter<SubscriptionModel, ItemCouponBinding>(
            list,
            R.layout.item_coupon, object :
                RecyclerCallback<ItemCouponBinding, SubscriptionModel> {
                @SuppressLint("SetTextI18n")
                override fun bindData(
                    binder: ItemCouponBinding,
                    model: SubscriptionModel,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                        tvname.text = model.code
                        tvoff.text = "$" + subscriptionModel.price
                        if (model.couponType == "PERCENTAGE") {
                            tvOffer.text =
                                "" + model.value + "% Discount on " + subscriptionModel.price
                            var perPric = (model.value * subscriptionModel.price) / 100
                            tvoff.text = "$$perPric"
                        } else {
                            tvOffer.text = "$" + model.value + " on " + subscriptionModel.price
                            tvoff.text = "$" + model.value
                        }

                        tvname.isChecked = selectPosition == position

                        tvname.setOnClickListener {
                            selectPosition = position
                            rvAdapProgress?.notifyDataSetChanged()
                        }

                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }

}