package com.appsums

import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import com.appsums.databinding.FragmentAddCardBinding
import com.appsums.model.BuySubscriptionModel
import com.appsums.model.GenericModel
import com.appsums.model.ToolBarModel
import com.appsums.network.ApiCall
import com.appsums.network.ApiCallDoctor
import com.appsums.network.ResponseListener
import com.appsums.utils.widgets.monthyearpicker.YearMonthPickerDialog
import com.appsums.view.base.BaseFragment
import com.appsums.view.fragment.ClientInfoFragment
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*


class AddCardFragment(
    val from: Int,
    val couponId: String? = null,
    val subsId: String,
    val price: Int
) :
    BaseFragment<FragmentAddCardBinding>(FragmentAddCardBinding::inflate) {

    var yearSelected = 0
    var monthSelected = 0

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.toolBarTtl = getString(R.string.Payment_Details)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            etDate.setOnClickListener {
                setMonthYear()
            }

            btnCall.setOnClickListener {
                val radioButtonID: Int = rbGrp.checkedRadioButtonId
                val radioButton: View = rbGrp.findViewById(radioButtonID)
                val idx: Int = rbGrp.indexOfChild(radioButton)
                val r: RadioButton = rbGrp.getChildAt(idx) as RadioButton
                if (isValidate())
                    callAPi(
                        BuySubscriptionModel(
                            null, subsId, r.text.toString(),
                            etNo.text.toString(),
                            monthSelected.toString(),
                            yearSelected.toString(),
                            etCvv.text.toString(), price, checkSave.isChecked
                        )
                    )
            }
        }

    }

    fun callAPi(body: BuySubscriptionModel) {
        homeActivity?.let {
            ApiCallDoctor.buySubscriptionAPi(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    if (from == 1) {
                        homeActivity?.clearAllStack()
                        displayItNoStack(DasboardFragment())
                    } else
                        displayItNoStack(ClientInfoFragment(0))
                }

                override fun onError(msg: String) {

                }
            })
        }
    }


    fun setMonthYear() {
        val calendar1 = Calendar.getInstance()
        var yearMonthPickerDialog = YearMonthPickerDialog(
            requireContext(),
            object : YearMonthPickerDialog.OnDateSetListener {
                override fun onYearMonthSet(year: Int, month: Int) {
                    val calendar = Calendar.getInstance()
                    calendar[Calendar.YEAR] = year
                    calendar[Calendar.MONTH] = month
                    yearSelected = year
                    monthSelected = month + 1
                    val dateFormat = SimpleDateFormat("MMM/yyyy")
                    viewDataBinding?.etDate?.setText(dateFormat.format(calendar.time))

                }

            },
            calendar1
        )

        yearMonthPickerDialog.show()

    }

    fun isValidate(): Boolean {
        viewDataBinding?.apply {
            if (etName.text.isNullOrEmpty()) {
                showToast(getString(R.string.et_name_on_card))
                return false
            } else if (etNo.text.isNullOrEmpty()) {
                showToast(getString(R.string.et_Card_Number))
                return false
            } else if (etDate.text.isNullOrEmpty()) {
                showToast(getString(R.string.et_card_expiry))
                return false
            } else if (etCvv.text.isNullOrEmpty()) {
                showToast(getString(R.string.et_cCVV))
                return false
            } else {
                return true
            }
        }
        return true
    }


}