package com.appsums

import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.MotionEvent
import android.view.View
import com.appsums.databinding.FragmentSignupBinding
import com.appsums.model.GenericModel
import com.appsums.model.SignupModel
import com.appsums.model.ToolBarModel
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.appsums.utils.GlobalConstants
import com.appsums.utils.SharedPrefClass
import com.appsums.utils.UtilsFunctions
import com.appsums.view.base.BaseFragment
import retrofit2.Response


class SignUpFragment : BaseFragment<FragmentSignupBinding>(FragmentSignupBinding::inflate) {

    var isPasswordVisibe = false
    var isPasswordVisibe2 = false

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            imgBackBtn.setOnClickListener {
                popBack()
            }
            btnCall.setOnClickListener {
                if (isValidate())
                    callAPi(
                        SignupModel(
                            UtilsFunctions.deviceType,
                            SharedPrefClass().getPrefValue(
                                requireContext(),
                                GlobalConstants.DEVICE_ID
                            )
                                .toString(),
                            etEmail.text.toString(),
                            "+1",
                            etPhnNo.text.toString(),
                            etPassword.text.toString()
                        )
                    )
            }

            tvSignUp.setOnClickListener {
                popBack()
            }

            etPassword.setOnTouchListener(View.OnTouchListener { v, event ->
                val DRAWABLE_LEFT = 0
                val DRAWABLE_TOP = 1
                val DRAWABLE_RIGHT = 2
                val DRAWABLE_BOTTOM = 3
                if (event.action == MotionEvent.ACTION_UP) {
                    if (event.rawX >= etPassword.getRight() - etPassword.getCompoundDrawables()
                            .get(DRAWABLE_RIGHT).getBounds().width()
                    ) {
                        isPasswordVisibe = !isPasswordVisibe

                        if (isPasswordVisibe) {
                            etPassword.transformationMethod =
                                HideReturnsTransformationMethod.getInstance();
                            etPassword.setCompoundDrawablesWithIntrinsicBounds(
                                0,
                                0,
                                R.drawable.ic_icon_visibility,
                                0
                            );
                        } else {
                            etPassword.transformationMethod =
                                PasswordTransformationMethod.getInstance();
                            etPassword.setCompoundDrawablesWithIntrinsicBounds(
                                0,
                                0,
                                R.drawable.ic_baseline_visibility_off_24,
                                0
                            );
                        }

                        return@OnTouchListener true
                    }
                }
                false
            })

            etConfirmPassword.setOnTouchListener(View.OnTouchListener { v, event ->
                val DRAWABLE_LEFT = 0
                val DRAWABLE_TOP = 1
                val DRAWABLE_RIGHT = 2
                val DRAWABLE_BOTTOM = 3
                if (event.action == MotionEvent.ACTION_UP) {
                    if (event.rawX >= etConfirmPassword.getRight() - etConfirmPassword.getCompoundDrawables()
                            .get(DRAWABLE_RIGHT).getBounds().width()
                    ) {
                        isPasswordVisibe2 = !isPasswordVisibe2

                        if (isPasswordVisibe2) {
                            etConfirmPassword.transformationMethod =
                                HideReturnsTransformationMethod.getInstance();
                            etConfirmPassword.setCompoundDrawablesWithIntrinsicBounds(
                                0,
                                0,
                                R.drawable.ic_icon_visibility,
                                0
                            );
                        } else {
                            etConfirmPassword.transformationMethod =
                                PasswordTransformationMethod.getInstance();
                            etConfirmPassword.setCompoundDrawablesWithIntrinsicBounds(
                                0,
                                0,
                                R.drawable.ic_baseline_visibility_off_24,
                                0
                            );
                        }

                        return@OnTouchListener true
                    }
                }
                false
            })

        }
    }

    fun isValidate(): Boolean {
        viewDataBinding?.apply {
            if (etEmail.text.isNullOrEmpty()) {
                showToast(getString(R.string.valid_enter_email))
                return false
            } else if (!UtilsFunctions.isValidEmail(etEmail.text.toString())) {
                showToast(getString(R.string.valid_email))
                return false
            } else if (etPhnNo.text.isNullOrEmpty()) {
                showToast(getString(R.string.et_Mobile_Number))
                return false
            } else if (etPassword.text.isNullOrEmpty()) {
                showToast(getString(R.string.enter_password))
                return false
            }  else if (etPassword.text.toString().length<7) {
                showToast(getString(R.string.valid_pass))
                return false
            } else if (etConfirmPassword.text.isNullOrEmpty()) {
                showToast(getString(R.string.enter_confirm_password))
                return false
            } else if (!etPassword.text.toString()
                    .equals(etConfirmPassword.text.toString(), ignoreCase = false)
            ) {
                showToast(getString(R.string.pass_does_not_match))
                return false
            } else {
                return true
            }
        }
        return true
    }

    fun callAPi(body: SignupModel) {
        homeActivity?.let {
            ApiCall.signupAPi(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    SharedPrefClass().putObject(
                        it,
                        GlobalConstants.ACCESS_TOKEN,
                        mResponse.body()?.userData?.token
                    )
                    SharedPrefClass().putObject(
                        it,
                        GlobalConstants.USERID,
                        mResponse.body()?.userData?._id
                    )
                    displayItAddStack(OtpFragment(0))
                }

                override fun onError(msg: String) {

                }
            })
        }
    }


}