package com.appsums

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.appsums.databinding.FragmentAppointmentViewDetailBinding
import com.appsums.databinding.ItemBillingBinding
import com.appsums.databinding.ItemMedRcordBinding
import com.appsums.databinding.ItemPerscriptionBinding
import com.appsums.model.ToolBarModel
import com.appsums.view.adapters.RecyclerCallback
import com.appsums.view.adapters.RecyclerViewGenricAdapter
import com.appsums.view.base.BaseFragment
import com.appsums.view.fragment.PrescriptionDetailFragment


class AppointmentViewDetailFragment :
    BaseFragment<FragmentAppointmentViewDetailBinding>(FragmentAppointmentViewDetailBinding::inflate) {

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.toolBarTtl = getString(R.string.VIEW_APPOINTMENT_DETAILS)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            recyclerView.layoutManager = LinearLayoutManager(requireContext())


            radioGroup.setOnCheckedChangeListener { radioGroup, i ->
                when (i) {
                    R.id.radComm1 -> {
                        setAdapter1()
                    }
                    R.id.radComm2 -> {
                        setAdapterMedRecord()
                    }
                    R.id.radComm3 -> {
                        setAdapterBilling()
                    }
                }

            }

            setAdapter1()

        }

    }

    fun setAdapter1() {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        val rvAdapProgress = RecyclerViewGenricAdapter<String, ItemPerscriptionBinding>(
            list,
            R.layout.item_perscription, object :
                RecyclerCallback<ItemPerscriptionBinding, String> {
                override fun bindData(
                    binder: ItemPerscriptionBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                        btnCall.setOnClickListener {
                            displayItAddStack(PrescriptionDetailFragment())
                        }

                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }


    fun setAdapterMedRecord() {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        val rvAdapProgress = RecyclerViewGenricAdapter<String, ItemMedRcordBinding>(
            list,
            R.layout.item_med_rcord, object :
                RecyclerCallback<ItemMedRcordBinding, String> {
                override fun bindData(
                    binder: ItemMedRcordBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }


    fun setAdapterBilling() {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        val rvAdapProgress = RecyclerViewGenricAdapter<String, ItemBillingBinding>(
            list,
            R.layout.item_billing, object :
                RecyclerCallback<ItemBillingBinding, String> {
                override fun bindData(
                    binder: ItemBillingBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }


}