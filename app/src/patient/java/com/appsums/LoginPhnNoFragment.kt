package com.appsums

import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.MotionEvent
import android.view.View
import com.appsums.databinding.FragmentLoginPhnBinding
import com.appsums.model.GenericModel
import com.appsums.model.LoginPhnModel
import com.appsums.model.ToolBarModel
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.appsums.utils.GlobalConstants
import com.appsums.utils.SharedPrefClass
import com.appsums.utils.UtilsFunctions
import com.appsums.view.base.BaseFragment
import retrofit2.Response


class LoginPhnNoFragment : BaseFragment<FragmentLoginPhnBinding>(FragmentLoginPhnBinding::inflate) {

    var isPasswordVisibe = false


    override fun getToolBar(): ToolBarModel {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            imgBackBtn.setOnClickListener {
                popBack()
            }

            tvForgotPass.setOnClickListener {
                displayItAddStack(ForgotPassFragment(1))
            }

            btnlogin.setOnClickListener {
                if (isValidate())
                    callAPi(
                        LoginPhnModel(
                            UtilsFunctions.deviceType,
                            SharedPrefClass().getPrefValue(
                                requireContext(),
                                GlobalConstants.DEVICE_ID
                            )
                                .toString(),
                            etEmail.text.toString(),
                            etPassword.text.toString(),
                            440
                        )
                    )
            }

            etPassword.setOnTouchListener(View.OnTouchListener { v, event ->
                val DRAWABLE_LEFT = 0
                val DRAWABLE_TOP = 1
                val DRAWABLE_RIGHT = 2
                val DRAWABLE_BOTTOM = 3
                if (event.action == MotionEvent.ACTION_UP) {
                    if (event.rawX >= etPassword.getRight() - etPassword.getCompoundDrawables()
                            .get(DRAWABLE_RIGHT).getBounds().width()
                    ) {
                        isPasswordVisibe = !isPasswordVisibe

                        if (isPasswordVisibe) {
                            etPassword.transformationMethod =
                                HideReturnsTransformationMethod.getInstance();
                            etPassword.setCompoundDrawablesWithIntrinsicBounds(
                                0,
                                0,
                                R.drawable.ic_icon_visibility,
                                0
                            );
                        } else {
                            etPassword.transformationMethod =
                                PasswordTransformationMethod.getInstance();
                            etPassword.setCompoundDrawablesWithIntrinsicBounds(
                                0,
                                0,
                                R.drawable.ic_baseline_visibility_off_24,
                                0
                            );
                        }

                        return@OnTouchListener true
                    }
                }
                false
            })

        }

    }


    fun callAPi(body: LoginPhnModel) {
        homeActivity?.let {
            ApiCall.loginAPi(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    SharedPrefClass().putObject(
                        it,
                        GlobalConstants.ACCESS_TOKEN,
                        mResponse.body()?.result?.token
                    )
                    SharedPrefClass().putObject(
                        it,
                        GlobalConstants.USERID,
                        mResponse.body()?.result?._id
                    )
                    displayItNoStack(DasboardFragment())
                }

                override fun onError(msg: String) {

                }

                override fun onError(msg: Boolean) {
                    super.onError(msg)
                    if (msg == false) {
                        displayItNoStack(OtpFragment(0))
                    }
                }
            })
        }
    }


    fun isValidate(): Boolean {
        viewDataBinding?.apply {
            return when {
                etEmail.text.isNullOrEmpty() -> {
                    showToast(getString(R.string.et_Mobile_Number))
                    false
                }
                etPassword.text.isNullOrEmpty() -> {
                    showToast(getString(R.string.enter_password))
                    false
                }
                else -> {
                    true
                }
            }
        }
        return true
    }


}