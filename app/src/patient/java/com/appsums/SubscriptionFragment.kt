package com.appsums.view.fragment

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.appsums.AddCardFragment
import com.appsums.R
import com.appsums.databinding.FragmentSubsBinding
import com.appsums.databinding.ItemSubscriptionBinding
import com.appsums.model.GenericModel
import com.appsums.model.ParamModel
import com.appsums.model.SubscriptionModel
import com.appsums.model.ToolBarModel
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.appsums.view.adapters.RecyclerCallback
import com.appsums.view.adapters.RecyclerViewGenricAdapter
import com.appsums.view.base.BaseFragment
import com.appsums.view.base.CustomDialog
import retrofit2.Response

class SubscriptionFragment(val from: Int) :
    BaseFragment<FragmentSubsBinding>(FragmentSubsBinding::inflate) {

    private var rvAdapProgress: RecyclerViewGenricAdapter<SubscriptionModel, ItemSubscriptionBinding>? = null
    val list = ArrayList<SubscriptionModel>()

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        if (from == 1) {
            toolBarModel.notiIconShow = true
            toolBarModel.toolBarProfileShow = true
        }
        toolBarModel.toolBarTtl = getString(R.string.My_Subscription)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        selectPosition = -1
        viewDataBinding?.apply {

            recyclerView.layoutManager = LinearLayoutManager(requireContext())
            setAdapter1()
            callAPi()

            btnCall.setOnClickListener {
                if (selectPosition >= 0) {
                    CustomDialog(homeActivity!!).openSubscriptionDialog(list[selectPosition],
                        object :
                            CustomDialog.DialogListener {
                            override fun positiveBtn() {
//                                if (from == 1) {
//                                    popBack()
//                                } else {
                                    displayItNoStack(
                                        AddCardFragment(
                                            from,
                                            null,
                                            list[selectPosition]._id,
                                            list[selectPosition].price
                                        )
                                    )
                              //  }
                            }

                            override fun positiveBtn(paramModel: ParamModel) {
                                displayItAddStack(AppluCOuponFragment(from,
                                    list[selectPosition]))

                            }
                        })

                } else {
                    showToast("Please select one subscription")
                }

            }
        }

    }

    fun callAPi() {
        homeActivity?.let {
            ApiCall.subscriptionListApi(it, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    list.clear()
                    mResponse.body()?.documentList?.let { it1 -> list.addAll(it1) }
                    rvAdapProgress?.notifyDataSetChanged()

                }

                override fun onError(msg: String) {

                }
            })
        }
    }


    fun setAdapter1() {
        rvAdapProgress = RecyclerViewGenricAdapter<SubscriptionModel, ItemSubscriptionBinding>(
            list,
            R.layout.item_subscription, object :
                RecyclerCallback<ItemSubscriptionBinding, SubscriptionModel> {
                override fun bindData(
                    binder: ItemSubscriptionBinding,
                    model: SubscriptionModel,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                        tvName.text = model.name
                        tvAudioCall.text = "Minutes of audio call: " + model.minutesOfAudioCalls
                        tvVideoCall.text = "Minutes of video call: " + model.minutesOfVideoCalls
                        tvPrice.text = "$" + model.price + "/month"

                        tvName.isChecked = selectPosition == position

                        tvName.setOnClickListener {
                            selectPosition = position
                            rvAdapProgress?.notifyDataSetChanged()
                        }

                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }

}