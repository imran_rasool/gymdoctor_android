package com.appsums

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.appsums.databinding.FragmentAppointmentBinding
import com.appsums.databinding.ItemAppointmentBinding
import com.appsums.model.AppointmentModel
import com.appsums.model.GenericModel
import com.appsums.model.ToolBarModel
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.appsums.view.adapters.RecyclerCallback
import com.appsums.view.adapters.RecyclerViewGenricAdapter
import com.appsums.view.base.BaseFragment
import com.appsums.view.fragment.AppointmentDetailFragment
import com.appsums.view.fragment.SelectTimeFragment
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import retrofit2.Response


class AppointmentFragment :
    BaseFragment<FragmentAppointmentBinding>(FragmentAppointmentBinding::inflate) {

    private var rvAdapProgress: RecyclerViewGenricAdapter<AppointmentModel, ItemAppointmentBinding>? =
        null
    var appointmentData = ArrayList<AppointmentModel>()
    var appointmentUpcomingData = ArrayList<AppointmentModel>()
    var appointmentPreviousData = ArrayList<AppointmentModel>()

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.notiIconShow = true
        toolBarModel.toolBarProfileShow = true
        toolBarModel.toolBarTtl = getString(R.string.MY_APPOINTMENT)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            recyclerView.layoutManager = LinearLayoutManager(requireContext())
            selectPosition = 0
            setAdapter()

            if (tabLayout.tabCount <= 0) {
                tabLayout.addTab(tabLayout.newTab().setText(R.string.Upcoming_Appointments))
                tabLayout.addTab(tabLayout.newTab().setText(R.string.Previous_Appointments))
            }


            tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab?) {
                    selectPosition = tab?.position ?: 0
                    if (selectPosition == 1) {
                        appointmentData.clear()
                        appointmentData.addAll(appointmentPreviousData)
                    } else {
                        appointmentData.clear()
                        appointmentData.addAll(appointmentUpcomingData)
                    }
                    rvAdapProgress?.notifyDataSetChanged()
                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {

                }

                override fun onTabReselected(tab: TabLayout.Tab?) {

                }
            })
        }
        callAPi()
    }


    fun setAdapter() {


        rvAdapProgress = RecyclerViewGenricAdapter<AppointmentModel, ItemAppointmentBinding>(
            appointmentData,
            R.layout.item_appointment, object :
                RecyclerCallback<ItemAppointmentBinding, AppointmentModel> {
                override fun bindData(binder: ItemAppointmentBinding, model1: AppointmentModel, position: Int, itemView: View) {
                    binder.apply {

                        if (selectPosition == 1) {
                            tvCancelled.visibility = View.VISIBLE
                            btnBookApp.visibility = View.VISIBLE
                            btnReshedule.visibility = View.GONE
                            btnCancelApp.visibility = View.GONE

                        } else {
                            btnReshedule.visibility = View.VISIBLE
                            btnCancelApp.visibility = View.VISIBLE
                            tvCancelled.visibility = View.GONE
                            btnBookApp.visibility = View.GONE
                        }

                        var model = model1.serviceProviderId
                        when {
                            model.experience_in_months == null -> {
                                tvExp.text =
                                    "" + model.experience_in_years + "yrs " + "of Experience"
                            }
                            model.experience_in_years == null -> {
                                tvExp.text =
                                    "" + model.experience_in_months + "mo " + "of Experience"
                            }
                            else -> {
                                tvExp.text =
                                    "" + model.experience_in_months + "mo " + model.experience_in_years + "yrs " + "of Experience"
                            }
                        }

                        setImageWithUrl(model.profilePic, img)
                        tvName.text = model.fullName
                        ratingStars.rating = model.rating.toFloat()
                        if (!model.secondry_specialty.isNullOrEmpty())
                            tvType.text = model.secondry_specialty[0].secondry_specialty
                        tvFees.text = "$" + model1.amount + " Consultation Fee"



                        lllTxt.setOnClickListener {
                            displayItAddStack(
                                AppointmentDetailFragment(
                                    selectPosition,
                                    appointmentData[position]._id
                                )
                            )
                        }

                        btnBookApp.setOnClickListener {
                            displayItAddStack(SelectTimeFragment())
                        }

                    }
                }
            })
        viewDataBinding?.apply {
            recyclerView.adapter = rvAdapProgress
        }
    }


    fun callAPi() {
        homeActivity?.let {
            ApiCall.upcomingAppointmentsList(it, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    val model = mResponse.body()
                    Log.d("myTag", "AppointmentFragment: ${Gson().toJson(model)}")
                    appointmentData.clear()
                    appointmentData.addAll(model?.appointmentData!!)
                    appointmentUpcomingData.clear()
                    appointmentUpcomingData.addAll(model?.appointmentData!!)
                    rvAdapProgress?.notifyDataSetChanged()

                }

                override fun onError(msg: String) {

                }
            })
        }
        callAPi1()
    }

    fun callAPi1() {
        homeActivity?.let {
            ApiCall.previousAppointmentsList(it, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    val model = mResponse.body()
                    appointmentPreviousData.clear()
                    appointmentPreviousData.addAll(model?.appointmentData!!)
                }

                override fun onError(msg: String) {

                }
            })
        }
    }


}