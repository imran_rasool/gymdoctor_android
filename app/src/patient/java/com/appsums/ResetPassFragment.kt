package com.appsums

import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.MotionEvent
import android.view.View
import com.appsums.databinding.FragmentResetPassBinding
import com.appsums.model.GenericModel
import com.appsums.model.ResetPasswordModel
import com.appsums.model.ToolBarModel
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.appsums.utils.GlobalConstants
import com.appsums.utils.SharedPrefClass
import com.appsums.view.base.BaseFragment
import com.appsums.view.fragment.InitialFragment
import retrofit2.Response


class ResetPassFragment :
    BaseFragment<FragmentResetPassBinding>(FragmentResetPassBinding::inflate) {

    var isPasswordVisibe = false
    var isPasswordVisibe2 = false

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            btnCall.setOnClickListener {
                if (isValidate())
                    callAPi(
                        ResetPasswordModel(
                            SharedPrefClass().getPrefValue(
                                requireContext(),
                                GlobalConstants.USERID
                            ).toString(), etPassword.text.toString()
                        )
                    )
            }
            etPassword.setOnTouchListener(View.OnTouchListener { v, event ->
                val DRAWABLE_LEFT = 0
                val DRAWABLE_TOP = 1
                val DRAWABLE_RIGHT = 2
                val DRAWABLE_BOTTOM = 3
                if (event.action == MotionEvent.ACTION_UP) {
                    if (event.rawX >= etPassword.getRight() - etPassword.getCompoundDrawables()
                            .get(DRAWABLE_RIGHT).getBounds().width()
                    ) {
                        isPasswordVisibe = !isPasswordVisibe

                        if (isPasswordVisibe) {
                            etPassword.transformationMethod =
                                HideReturnsTransformationMethod.getInstance();
                            etPassword.setCompoundDrawablesWithIntrinsicBounds(
                                0,
                                0,
                                R.drawable.ic_icon_visibility,
                                0
                            );
                        } else {
                            etPassword.transformationMethod =
                                PasswordTransformationMethod.getInstance();
                            etPassword.setCompoundDrawablesWithIntrinsicBounds(
                                0,
                                0,
                                R.drawable.ic_baseline_visibility_off_24,
                                0
                            );
                        }

                        return@OnTouchListener true
                    }
                }
                false
            })


            etConfirmPassword.setOnTouchListener(View.OnTouchListener { v, event ->
                val DRAWABLE_LEFT = 0
                val DRAWABLE_TOP = 1
                val DRAWABLE_RIGHT = 2
                val DRAWABLE_BOTTOM = 3
                if (event.action == MotionEvent.ACTION_UP) {
                    if (event.rawX >= etConfirmPassword.getRight() - etConfirmPassword.getCompoundDrawables()
                            .get(DRAWABLE_RIGHT).getBounds().width()
                    ) {
                        isPasswordVisibe2 = !isPasswordVisibe2

                        if (isPasswordVisibe2) {
                            etConfirmPassword.transformationMethod =
                                HideReturnsTransformationMethod.getInstance();
                            etConfirmPassword.setCompoundDrawablesWithIntrinsicBounds(
                                0,
                                0,
                                R.drawable.ic_icon_visibility,
                                0
                            );
                        } else {
                            etConfirmPassword.transformationMethod =
                                PasswordTransformationMethod.getInstance();
                            etConfirmPassword.setCompoundDrawablesWithIntrinsicBounds(
                                0,
                                0,
                                R.drawable.ic_baseline_visibility_off_24,
                                0
                            );
                        }

                        return@OnTouchListener true
                    }
                }
                false
            })


        }

    }


    fun callAPi(body: ResetPasswordModel) {
        homeActivity?.let {
            ApiCall.resetPasswordAPi(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    homeActivity?.clearAllStack()
                    displayItAddStack(InitialFragment())
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    fun isValidate(): Boolean {
        viewDataBinding?.apply {
            if (etPassword.text.isNullOrEmpty()) {
                showToast(getString(R.string.enter_password))
                return false
            } else if (etConfirmPassword.text.isNullOrEmpty()) {
                showToast(getString(R.string.enter_confirm_password))
                return false
            } else if (!etPassword.text.toString()
                    .equals(etConfirmPassword.text.toString(), ignoreCase = false)
            ) {
                showToast(getString(R.string.pass_does_not_match))
                return false
            } else {
                return true
            }
        }
        return true
    }

}