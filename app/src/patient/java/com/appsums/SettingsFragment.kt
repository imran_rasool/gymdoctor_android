package com.appsums

import android.os.Bundle
import android.view.View
import com.appsums.databinding.FragmentSetttingsBinding
import com.appsums.model.ToolBarModel
import com.appsums.model.UserData
import com.appsums.utils.GlobalConstants
import com.appsums.utils.SharedPrefClass
import com.appsums.view.base.BaseFragment
import com.appsums.view.fragment.*
import com.google.gson.Gson


class SettingsFragment : BaseFragment<FragmentSetttingsBinding>(FragmentSetttingsBinding::inflate) {

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.notiIconShow = true
        toolBarModel.toolBarProfileShow = true
        toolBarModel.toolBarTtl = getString(R.string.Settings)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            tvEditProfile.setOnClickListener {
                val model: String? =
                    SharedPrefClass().getPrefValue(requireContext(), GlobalConstants.USER_PROFILE)
                        .toString()
                val model1: UserData? = Gson().fromJson(model, UserData::class.java)
                displayItAddStack(CreateProfileFragment(1, model1))
            }

            tvEditMedicalProfile.setOnClickListener {
                val model: String? =
                    SharedPrefClass().getPrefValue(requireContext(), GlobalConstants.USER_PROFILE)
                        .toString()
                val model1: UserData? = Gson().fromJson(model, UserData::class.java)

                displayItAddStack(CreateMedicalFragment(1, model1))
            }
            tvChangePassword.setOnClickListener {
                displayItAddStack(ChangePasswordFragment())
            }

            tvMedicalInfo.setOnClickListener {
                displayItAddStack(PaymentDetailsFragment())
            }

            tvPrivacyPolicy.setOnClickListener {
                displayItAddStack(WebViewFragment(getString(R.string.Privacy_Policy)))
            }

            tvTermCondition.setOnClickListener {
                displayItAddStack(WebViewFragment(getString(R.string.Terms_Conditions)))
            }

            tvInviteFrd.setOnClickListener {
                displayItAddStack(InviteFrdFragment())
            }

        }

    }


}