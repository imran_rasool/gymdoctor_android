package com.appsums

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.appsums.databinding.*
import com.appsums.model.*
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.appsums.utils.GlobalConstants
import com.appsums.utils.SharedPrefClass
import com.appsums.view.adapters.RecyclerCallback
import com.appsums.view.adapters.RecyclerViewGenricAdapter
import com.appsums.view.base.BaseFragment
import com.appsums.view.fragment.DeatilFragment
import com.appsums.view.fragment.PhysicanFragment
import com.google.gson.Gson
import retrofit2.Response

private const val TAG = "DasboardFragment"

class DasboardFragment : BaseFragment<FragmentDashboardBinding>(FragmentDashboardBinding::inflate) {

    private var rvAdapProgressLib: RecyclerViewGenricAdapter<HomeDataDocsModel, ItemLibrotiryBinding>? =
        null
    private var rvAdapProgressType: RecyclerViewGenricAdapter<SubscriptionModel, ItemDasboardBinding>? =
        null
    private var rvAdapProgress: RecyclerViewGenricAdapter<HomeDataModel, ItemHomeBinding>? = null
    val list = ArrayList<HomeDataModel>()
    val liboratoryList = ArrayList<HomeDataDocsModel>()
    val serviceTypelist = ArrayList<SubscriptionModel>()


    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.sideNav = true
        toolBarModel.notiIconShow = true
        toolBarModel.toolBarProfileShow = true
        toolBarModel.toolBarTtl = getString(R.string.DASHBOARD)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setLocationName()
        homeActivity?.updateNavHeader()

        viewDataBinding?.apply {
            recyclerView.layoutManager = GridLayoutManager(context, 2)
            recyclerView1.layoutManager = LinearLayoutManager(requireContext())
            serviceTypelist.clear()
            serviceTypelist.add(SubscriptionModel())
            setAdapter1(recyclerView)
            setAdapter(recyclerView1)
            //setAdapter(recyclerView2)
            // setAdapter(recyclerView3)
            setAdapter2(recyclerView4)

            callAPi()
            callAPi(LatLngModel(lat, long))
            callAPi2(LatLngModel(lat1, long1))

        }

    }

    fun setLocationName() {
        viewDataBinding?.tvLocName?.text =
            SharedPrefClass().getPrefValue(requireContext(), GlobalConstants.LOCNAME).toString()
    }


    fun setAdapter(recyclerView1: RecyclerView) {
        rvAdapProgress = RecyclerViewGenricAdapter<HomeDataModel, ItemHomeBinding>(
            list,
            R.layout.item_home, object :
                RecyclerCallback<ItemHomeBinding, HomeDataModel> {
                override fun bindData(
                    binder: ItemHomeBinding,
                    model: HomeDataModel,
                    position: Int,
                    itemView: View
                ) {
                    binder.apply {
                        val name = model.docs[0].serviceProviderType[0].primary_specialty
                        tvName.text = "Latest $name in your area"
                        recyclerView.layoutManager =
                            LinearLayoutManager(
                                requireContext(),
                                LinearLayoutManager.HORIZONTAL,
                                false
                            )
                        setAdapterItem(recyclerView, model.docs)

                    }
                }
            })
        recyclerView1.adapter = rvAdapProgress
        recyclerView1.isNestedScrollingEnabled = false

    }

    fun setAdapter1(recyclerView: RecyclerView) {
        rvAdapProgressType = RecyclerViewGenricAdapter<SubscriptionModel, ItemDasboardBinding>(
            serviceTypelist,
            R.layout.item_dasboard, object :
                RecyclerCallback<ItemDasboardBinding, SubscriptionModel> {
                override fun bindData(
                    binder: ItemDasboardBinding,
                    model: SubscriptionModel,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                        if (position == serviceTypelist.size - 1) {
                            tvDesc.text = "Get your test reports online"
                        } else {
                            setImageWithUrl(model.image, img)
                            tvDesc.text = "Top " + model.primary_specialty + " Near By Your Region"
                        }

                        linearMain.setOnClickListener {
                            if (position == serviceTypelist.size - 1) {
                                displayItAddStack(DeatilFragment())
                            } else {
                                displayItAddStack(PhysicanFragment(model._id))
                            }

                        }
                    }
                }
            })
        recyclerView.adapter = rvAdapProgressType
        recyclerView.isNestedScrollingEnabled = false

    }


    fun setAdapterItem(recyclerView: RecyclerView, docs: ArrayList<HomeDataDocsModel>) {

        val rvAdapProgress = RecyclerViewGenricAdapter<HomeDataDocsModel, ItemAreaBinding>(
            docs,
            R.layout.item_area, object :
                RecyclerCallback<ItemAreaBinding, HomeDataDocsModel> {
                @SuppressLint("SetTextI18n")
                override fun bindData(
                    binder: ItemAreaBinding,
                    model: HomeDataDocsModel,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                        setImageWithUrl(model.businessLogo, img)

                        tvName.text = model.fullName
                        ratingStars.rating = model.rating.toFloat()
                        when {
                            false -> {
                                tvExp.text =
                                    "" + model.experience_in_years + "yrs " + "Exp"
                            }
                            false -> {
                                tvExp.text =
                                    "" + model.experience_in_months + "mo " + "Exp"
                            }
                            else -> {
                                tvExp.text =
                                    "" + model.experience_in_years + "yrs " + model.experience_in_months + "mo " + "Exp"
                            }
                        }

                        tvFees.text = "$" + model.serviceCharge + " Consultation Fee"
                        tvType.text = model.serviceProviderType[0].primary_specialty

                        llItem.setOnClickListener {
                            displayItAddStack(ViewDetailFragment(model._id))
                        }

                    }
                }
            })
        recyclerView.adapter = rvAdapProgress
        recyclerView.isNestedScrollingEnabled = false

    }

    fun setAdapter2(recyclerView: RecyclerView) {
        rvAdapProgressLib = RecyclerViewGenricAdapter<HomeDataDocsModel, ItemLibrotiryBinding>(
            liboratoryList,
            R.layout.item_librotiry, object :
                RecyclerCallback<ItemLibrotiryBinding, HomeDataDocsModel> {
                @SuppressLint("SetTextI18n")
                override fun bindData(
                    binder: ItemLibrotiryBinding,
                    model: HomeDataDocsModel,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {
                        setImageWithUrl(model.image, img)
                        //tvExpYMonth.text = "" + model.experience_in_years + " Years" + model.experience_in_months + " Months of Experience"
                        tvName.text = model.name
                        //ratingStars.rating = model.rating.toFloat()
                        //tvFeeConsultant.text = "$" + model.serviceCharge + " Consultation Fee"

                        llItemMain.setOnClickListener {
                            displayItAddStack(ViewDetailLabFragment(model._id))
                        }
                    }
                }
            })
        recyclerView.adapter = rvAdapProgressLib
        recyclerView.isNestedScrollingEnabled = false

    }

    fun callAPi(body: LatLngModel) {
        homeActivity?.let {
            ApiCall.homeAPi(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    var model = mResponse.body()
                    list.clear()
                    list.addAll(model?.homeData!!)
                    rvAdapProgress?.notifyDataSetChanged()
                }

                override fun onError(msg: String) {

                }
            })
        }

    }

    fun callAPi() {
        homeActivity?.let {
            ApiCall.serviceProviderTypeListAPi(it, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    val model = mResponse.body()
                    serviceTypelist.clear()
                    serviceTypelist.addAll(model?.documentList!!)
                    serviceTypelist.add(SubscriptionModel())
                    rvAdapProgressType?.notifyDataSetChanged()
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    fun callAPi2(body: LatLngModel) {
        homeActivity?.let {
            ApiCall.laboratoryHomeDataAPi(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    var model = mResponse.body()
                    Log.d(TAG, "callAPi2: ${Gson().toJson(model)}")
                    liboratoryList.clear()
                    liboratoryList.addAll(model?.laboratory?.docs ?: liboratoryList)
                    rvAdapProgressLib?.notifyDataSetChanged()
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

}