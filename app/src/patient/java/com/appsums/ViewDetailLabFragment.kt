package com.appsums

import android.os.Bundle
import android.util.Log
import android.view.View
import com.appsums.databinding.FragmentViewDetailLabBinding
import com.appsums.databinding.ItemViewLabDetailBinding
import com.appsums.model.GenericModel
import com.appsums.model.ToolBarModel
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.appsums.view.adapters.RecyclerCallback
import com.appsums.view.adapters.RecyclerViewGenricAdapter
import com.appsums.view.base.BaseFragment
import com.google.gson.Gson
import retrofit2.Response

private const val TAG = "ViewDetailLabFragment"

class ViewDetailLabFragment(val _id: String) :
    BaseFragment<FragmentViewDetailLabBinding>(FragmentViewDetailLabBinding::inflate) {

    val list = ArrayList<String>()

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.toolBarTtl = getString(R.string.VIEW_DETAIL)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {
            setAdapter1()
        }

        callAPi(_id)
    }

    fun callAPi(id: String) {
        homeActivity?.let {
            ApiCall.viewLaboratory(it, id, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    Log.d(TAG, "onSuccess: ${Gson().toJson(mResponse.body())}")
                    val model = mResponse.body()?.laboratoryDetails
                    viewDataBinding?.apply {
                        setImageWithUrl(model?.image, imgProfile)
                    }
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    fun setAdapter1() {
        val list = ArrayList<String>()
        list.add("All")
        list.add("Field")
        list.add("Racket")
        list.add("Winter")
        list.add("Water")
        list.add("Motor")
        list.add("Martial")
        list.add("Other")

        val rvAdapProgress = RecyclerViewGenricAdapter<String, ItemViewLabDetailBinding>(
            list,
            R.layout.item_view_lab_detail, object :
                RecyclerCallback<ItemViewLabDetailBinding, String> {
                override fun bindData(
                    binder: ItemViewLabDetailBinding,
                    model: String,
                    position: Int,
                    itemView: View
                ) {

                    binder.apply {

                        llItem.setOnClickListener {
                            // displayItAddStack(InvitationCheckingFragment())
                        }

                    }
                }
            })
        viewDataBinding?.apply {
            recyclerViewLab.adapter = rvAdapProgress
        }
    }


}