package com.appsums

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import com.appsums.databinding.FragmentLoginBinding
import com.appsums.model.GenericModel
import com.appsums.model.LoginModel
import com.appsums.model.ToolBarModel
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.appsums.utils.GlobalConstants
import com.appsums.utils.SharedPrefClass
import com.appsums.utils.UtilsFunctions
import com.appsums.view.base.BaseFragment
import com.appsums.view.fragment.CreateMedicalFragment
import com.appsums.view.fragment.SubscriptionFragment
import retrofit2.Response


class LoginFragment : BaseFragment<FragmentLoginBinding>(FragmentLoginBinding::inflate) {

    var isPasswordVisibe = false

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        return toolBarModel
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {

            tvSignUp.setOnClickListener {
                displayItAddStack(SignUpFragment())
            }

            btnPhn.setOnClickListener {
                displayItAddStack(LoginPhnNoFragment())
            }

            btnlogin.setOnClickListener {
                if (isValidate()) {
                    if (homeActivity?.getCurrentLocationName() == true)
                        callAPi(
                            LoginModel(
                                UtilsFunctions.deviceType,
                                SharedPrefClass().getPrefValue(
                                    requireContext(),
                                    GlobalConstants.DEVICE_ID
                                )
                                    .toString(),
                                etEmail.text.toString(),
                                etPassword.text.toString(),
                                440
                            )
                        )
                }

            }

            underlineText(tvForgotPass)

            tvForgotPass.setOnClickListener {
                displayItAddStack(ForgotPassFragment(0))
            }


            etPassword.setOnTouchListener(OnTouchListener { v, event ->
                val DRAWABLE_LEFT = 0
                val DRAWABLE_TOP = 1
                val DRAWABLE_RIGHT = 2
                val DRAWABLE_BOTTOM = 3
                if (event.action == MotionEvent.ACTION_UP) {
                    if (event.rawX >= etPassword.getRight() - etPassword.getCompoundDrawables()
                            .get(DRAWABLE_RIGHT).getBounds().width()
                    ) {
                        isPasswordVisibe = !isPasswordVisibe

                        if (isPasswordVisibe) {
                            etPassword.transformationMethod =
                                HideReturnsTransformationMethod.getInstance();
                            etPassword.setCompoundDrawablesWithIntrinsicBounds(
                                0,
                                0,
                                R.drawable.ic_icon_visibility,
                                0
                            );
                        } else {
                            etPassword.transformationMethod =
                                PasswordTransformationMethod.getInstance();
                            etPassword.setCompoundDrawablesWithIntrinsicBounds(
                                0,
                                0,
                                R.drawable.ic_baseline_visibility_off_24,
                                0
                            );
                        }

                        return@OnTouchListener true
                    }
                }
                false
            })
        }
    }

    ///
    // "responseMessage":"Otp has been sent,Please verify your account now","result":{"isVerified":false,"_id":"61d417178caefc5c66246f33"}}
    fun callAPi(body: LoginModel) {
        homeActivity?.let {
            ApiCall.loginAPi(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    val model = mResponse.body()?.result
                    if (mResponse.body()?.result?.isVerified == true) {
                        SharedPrefClass().putObject(
                            it,
                            GlobalConstants.EMAIL,
                            mResponse.body()?.result?.email.toString()
                        )

                        SharedPrefClass().putObject(
                            it,
                            GlobalConstants.PIC,
                            mResponse.body()?.result?.profilePic.toString()
                        )
                        SharedPrefClass().putObject(
                            it,
                            GlobalConstants.ACCESS_TOKEN,
                            mResponse.body()?.result?.token
                        )
                        SharedPrefClass().putObject(
                            it,
                            GlobalConstants.PROFILE_STATUS,
                            mResponse.body()?.result?.isPersonal_profile_complete
                        )

                        SharedPrefClass().putObject(
                            it,
                            GlobalConstants.MEDICAL_STATUS,
                            mResponse.body()?.result?.isMedical_profile_complete
                        )
                        SharedPrefClass().putObject(
                            it,
                            GlobalConstants.IS_SUBSCRIBED,
                            mResponse.body()?.result?.isSubscribe
                        )
                    }

                    SharedPrefClass().putObject(
                        it,
                        GlobalConstants.IS_VERIFIED,
                        mResponse.body()?.result?.isVerified
                    )
                    SharedPrefClass().putObject(
                        it,
                        GlobalConstants.USERID,
                        mResponse.body()?.result?._id
                    )

                    homeActivity?.clearAllStack()
                    if (homeActivity?.getCurrentLocationName() == true) {
                        when {
                            model?.isVerified == false -> {
                                displayItNoStack(OtpFragment(0))
                            }
                            model?.isPersonal_profile_complete == false -> {
                                displayItNoStack(CreateProfileFragment(0, null))

                            }
                            model?.isSubscribe == false -> displayItNoStack(SubscriptionFragment(0))
                            model?.isMedical_profile_complete == false -> displayItNoStack(
                                CreateMedicalFragment(1, null)
                            )
                            else -> {
                                displayItNoStack(DasboardFragment())
                            }
                        }
                    }
                    homeActivity?.updateNavHeader()
                }

                override fun onError(msg: String) {

                }
            })
        }
    }


    fun isValidate(): Boolean {
        viewDataBinding?.apply {
            if (etEmail.text.isNullOrEmpty()) {
                showToast(getString(R.string.valid_enter_email))
                return false
            } else if (!UtilsFunctions.isValidEmail(etEmail.text.toString())) {
                showToast(getString(R.string.valid_email))
                return false
            } else if (etPassword.text.isNullOrEmpty()) {
                showToast(getString(R.string.enter_password))
                return false
            } else {
                return true
            }
        }
        return true
    }


}