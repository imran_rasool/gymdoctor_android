package com.appsums

import android.os.Bundle
import android.view.View
import com.appsums.databinding.FragmentPatientDetailBinding
import com.appsums.model.ToolBarModel
import com.appsums.view.base.BaseFragment


class PatientDetailFragment(val tabPos: Int) : BaseFragment<FragmentPatientDetailBinding>(FragmentPatientDetailBinding::inflate) {

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.toolBarTtl = getString(R.string.PATIENT_DETAILS)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {


            btnCall.setOnClickListener {
                displayItAddStack(PaymentDetailsFragment())
            }
        }

    }


}