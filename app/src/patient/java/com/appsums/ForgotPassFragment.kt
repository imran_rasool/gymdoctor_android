package com.appsums

import android.os.Bundle
import android.view.View
import com.appsums.databinding.FragmentForgotPassBinding
import com.appsums.model.ForgotPasswordModel
import com.appsums.model.ForgotPasswordNoModel
import com.appsums.model.GenericModel
import com.appsums.model.ToolBarModel
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.appsums.utils.GlobalConstants
import com.appsums.utils.SharedPrefClass
import com.appsums.utils.UtilsFunctions
import com.appsums.view.base.BaseFragment
import retrofit2.Response


class ForgotPassFragment(var from: Int) :
    BaseFragment<FragmentForgotPassBinding>(FragmentForgotPassBinding::inflate) {

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = false
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding?.apply {
            if (from == 1) {
                tvInfo.setText(R.string.forgot_pass_info_no)
                etEmail.setHint(R.string.et_Mobile_Number)
            }else{
                etEmail.setHint(R.string.valid_enter_email)
                tvInfo.setText(R.string.forgot_pass_info)
            }


            imgBackBtn.setOnClickListener {
                popBack()
            }

            btnCall.setOnClickListener {
                if (isValidate()){
                    if (from == 1) {
                        callAPi(ForgotPasswordNoModel(etEmail.text.toString()))
                    }else{
                        callAPi(ForgotPasswordModel(etEmail.text.toString()))
                    }
                }

            }

        }

    }


    fun isValidate(): Boolean {
        viewDataBinding?.apply {
            if (from == 1) {
                if (etEmail.text.isNullOrEmpty()) {
                    showToast(getString(R.string.et_Mobile_Number))
                    return false
                } else {
                    return true
                }
            } else {
                if (etEmail.text.isNullOrEmpty()) {
                    showToast(getString(R.string.valid_enter_email))
                    return false
                } else if (!UtilsFunctions.isValidEmail(etEmail.text.toString())) {
                    showToast(getString(R.string.valid_email))
                    return false
                } else {
                    return true
                }
            }

        }
        return true
    }


    fun callAPi(body: ForgotPasswordModel) {
        homeActivity?.let {
            ApiCall.forgotPasswordAPi(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    SharedPrefClass().putObject(
                        it,
                        GlobalConstants.USERID,
                        mResponse.body()?.result?._id
                    )
                    displayItAddStack(OtpFragment(1))
                }

                override fun onError(msg: String) {

                }
            })
        }
    }
    fun callAPi(body: ForgotPasswordNoModel) {
        homeActivity?.let {
            ApiCall.forgotPasswordAPi(it, body, object : ResponseListener<GenericModel> {
                override fun onSuccess(mResponse: Response<GenericModel>) {
                    SharedPrefClass().putObject(
                        it,
                        GlobalConstants.USERID,
                        mResponse.body()?.result?._id
                    )
                    displayItAddStack(OtpFragment(1))
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

}