package com.appsums

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import com.appsums.R
import com.appsums.databinding.FragmentViewDetailBinding
import com.appsums.model.*
import com.appsums.network.ApiCall
import com.appsums.network.ResponseListener
import com.appsums.view.base.BaseFragment
import com.google.gson.Gson
import retrofit2.Response

private const val TAG = "ViewDetailFragment"

class ViewDetailFragment(val _id: String) :
    BaseFragment<FragmentViewDetailBinding>(FragmentViewDetailBinding::inflate) {
    val listSP = ArrayList<ViewDoctorDetail>()

    override fun getToolBar(): ToolBarModel? {
        val toolBarModel = ToolBarModel()
        toolBarModel.toolBarShow = true
        toolBarModel.backBtn = true
        toolBarModel.toolBarTtl = getString(R.string.VIEW_DETAIL)
        return toolBarModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        viewDataBinding?.apply {

            imgFavouriteUnFavourite.setOnClickListener {
                callAPi(FavouriteModal(listSP[0]._id ?: "", !(listSP[0].isFavourite ?: true)))
            }

            // callAPi3(_id)
        }

    }

    fun callAPi(body: FavouriteModal) {
        homeActivity?.let {
            ApiCall.favAPi(it, body, object :
                ResponseListener<GenericModel1> {
                override fun onSuccess(mResponse: Response<GenericModel1>) {
                    listSP[0].isFavourite = !listSP[0].isFavourite
                    callBookMark()
                }

                override fun onError(msg: String) {

                }
            })
        }
    }

    fun callAPi3(id: String) {
        homeActivity?.let {
            ApiCall.viewDoctorDetail(it, id, object :
                ResponseListener<GenericModel1> {
                @SuppressLint("SetTextI18n")
                override fun onSuccess(mResponse: Response<GenericModel1>) {
                    val model = mResponse.body()
                    Log.d(TAG, "onSuccess: ${Gson().toJson(mResponse)}")
                    listSP.clear()
                    listSP.addAll(model?.result!!)

                    /*viewDataBinding.apply {

                        setImageWithUrl(listSP[0].profilePic, imgProfileSP)
                        tvConsultantFee.text = "Consultation Fee  $" + listSP[0].serviceCharge
                        ratingStars.rating = listSP[0].rating.toFloat()
                        tvExperience.text =
                            "" + listSP[0].experience_in_years + " Years and " + listSP[0].experience_in_months + " Months Of Experience"

                    }*/


                }

                override fun onError(msg: String) {

                }
            })
        }

    }


    fun callBookMark() {
        viewDataBinding?.apply {
            if (listSP[0].isFavourite) {
                setImgTint(imgFavouriteUnFavourite, R.color.colorRed)
            } else {
                setImgTint(imgFavouriteUnFavourite, R.color.colorGray4)
            }
        }
    }

}